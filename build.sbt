import xsbti.compile

name := "sygus-repair"

version := "1.0"

scalaVersion := "2.12.6"

compileOrder := CompileOrder.JavaThenScala
//javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

//scalacOptions += "-target:jvm-1.8"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

//libraryDependencies += "com.typesafe.akka" % "akka-actor" % "2.0.2"

libraryDependencies ++= Seq("org.scalaz" %% "scalaz-core" % "7.2.22",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.0",
  "io.argonaut" %% "argonaut" % "6.2.1")

// for debugging sbt problems
logLevel := Level.Debug

//scalacOptions += "-deprecation"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

//packMain := Map("hello" -> "driver.Main")

exportJars := true

mainClass in assembly  := Some("engines.RepairDriver")