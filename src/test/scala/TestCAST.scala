import engines.coreast._

/**
  * Created by dxble on 10/4/16.
  */

object TestCAST{
  class TestEval extends CASTVisitorBase{
    // Exception expected
    //override def preVisit(node: CAST): (VisitorReturnType, Boolean) = {if(node.isInstanceOf[And])(EmptyType(), true) else (EmptyType(), false)}

    override def preVisit(node: CAST): (VisitorReturnType, Boolean) = {(EmptyType(), true)}

    override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

    override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = ???

    override def visitIntVar(intVar: IntVar): VisitorReturnType = ???
  }
  def main(args: Array[String]) {
    println(And(BoolConst("true"), BoolConst("false")).accept(new TestEval))
    println(And(BoolConst("true"), BoolConst("true")).accept(new TestEval))
  }
}