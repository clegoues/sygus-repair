import engines.features.RankingFeature
import extsolvers.common._
import engines.coreast._
import extsolvers.metasearch.MetaSearchSolver
import synthlib2parser.{Synthlib2Compiler}
import org.apache.log4j.PropertyConfigurator

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 7/13/16.
  */
object TestConEval {
  val log4jProp = "/home/dxble/workspace/repairtools/angelix/src/af2sygus/src/main/resources/Log4j.properties"
  val fileName1 = "/home/dxble/workspace/synthesis/mytest/if-condition"
  val fileName2 =  "/home/dxble/workspace/synthesis/mytest/distance.sl"

  def main(args: Array[String]) {
    PropertyConfigurator.configure(log4jProp)
    val parsedResult = Synthlib2Compiler(scala.io.Source.fromFile(fileName1).mkString, true)
    println("***Parsed result: ")
    println(parsedResult)
    parsedResult match {
      case Right(res) => {
        val (theProg, parser) = res
        import scala.util.Random
        val rand = new Random(0)
        val solver = new MetaSearchSolver(parser.theSymbolTable, rand, new ArrayBuffer[RankingFeature](), null)
        theProg.accept(solver)
        val (constraints,origExprs) = (solver.allConstraints, solver.originalExprs)
        println("***Constraints: ")
        println(constraints)
        val subs = new DefaultSubsCollector(solver)
        constraints(0).accept(subs)

        //Map from function signature to its body
        val subst = new mutable.HashMap[FunctionSignature, Solution]()
        val args = new ArrayBuffer[InternalTerm]()
        args.append(new IntVar("x"))
        args.append(new IntVar("y"))
        val func = new FunctionSignature("f", args)
        val subofFunc = new GTE(new IntVar("x"),new IntVar("y"))
        subst += (func -> new OriginalSolution(func,subofFunc,-1, subofFunc))

        println("***Test concrete evaluation with substitution: "+subst)
        val conEval = new ConcreteEvaluation(subs,subst)
        println(constraints(0).accept(conEval))
        println(constraints(1).accept(conEval))
        println(solver.solutions)

        println("***Test collecting sub-expressions:")
        val expCol = new ExpressionsCollector
        println(origExprs.head._2.accept(expCol))
        println(constraints(0).accept(expCol))

        println("***Evaluating original expressions:")
        println(origExprs)
        origExprs.foreach(f =>{
          f match {
            case (funSig, body) => println(body.accept(conEval))
          }
        })

        println("****Test transformer: ")
        def matchedFunc(e1: CAST, e2: CAST): Boolean ={
          return func.equals(e1)
        }

        val trans = new ASTTransformer(subofFunc, matchedFunc)
        val x= constraints(0).accept(trans)
        println(x)
      }
    }
  }
}
