import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 6/21/16.
  */
sealed trait BaseClass
class L1 extends BaseClass{
  def apply(typeStr: String) = typeStr
  def unapply(arg: String): Option[String] = {
    arg match {
      case "Int" => Some("Int")
    }
  }
}
case class L11() extends L1
//object L1 extends BaseClass{
//  def unapply(arg: L1): Option[String] = Some("L1 here")
//}
class TestObjectField{
  val tb = new mutable.HashMap[String, String]()
  def getTB() = tb
}

object Twice {
  def apply(x: Int): Int = x //* 2 //add *2 to make it matched
  def unapply(z: Int): Option[Int] = if (z%2 == 0) Some(z/2) else None
}

object TestScala {
  def main(args: Array[String]) {
    //testRegular()
    //patMatchCaseClass(L11())
    //testUnapply()
    //testObjectField()
    //testlogic()
    //testCrossP()
    val x = -1
    println(x.toString)
  }

  def testlogic(): Unit ={
    //(and (or x y) z)
    //(or x (and y z))
    val c = (true || true) && false
    val d = true || true && false
    println(c)
    println(d)
  }

  def testCrossP() ={
    val inputList: List[List[Int]] = List(List(1, 2), List(3, 4, 5), List(1, 9))
    val zss: List[List[Int]] = List(List())
    def fun(xs: List[Int], zss: List[List[Int]]): List[List[Int]] = {
      for {
        x <- xs
        zs <- zss
      } yield {
        x :: zs
      }
    }
    val crossProd: List[List[Int]] = inputList.foldRight(zss)(fun _)
    println(crossProd)
  }
  def testObjectField() ={
    val tb = new mutable.HashMap[String, String]()
    tb += ("X1" -> "Y1")
    tb += ("X2" -> "Y2")
    println("OutTB = " + tb)
    val test = new TestObjectField
    tb.foldLeft(test.getTB()){
      (res, tbi) =>{
        res += tbi
        res
      }
    }
    println("TBinTest = "+test.getTB())
  }
  def testUnapply(): Unit ={
    val x = Twice(21)
    x match { case Twice(n) => Console.println(n) } // prints 21
  }

  def patMatchCaseClass(e: BaseClass): Unit ={
    //val e = new L1()
    e match {
      case e1:L1 => println("L1 matched")
      case e2: L11 => println("L11 matched")
    }
  }

  def testmap(): Unit ={
    val map = new scala.collection.immutable.HashMap[String, ArrayBuffer[String]]
    val arr = new ArrayBuffer[String]()
    arr.append("1")
    val map1 = map + ("x" -> arr)
    val newMap = map1.get("x") match {
      case None => {
        val arr = new ArrayBuffer[String]()
        arr.append("1")
        map1 + ("x" -> arr)
      }
      case Some(existingArr) => {
        existingArr.append("2")
        //map.updated("x", existingArr)
        map1
      }
    }
    println(newMap)
  }

  //|\+|-|\*|&|||!|~|<|>|=|/|%|\?|.|$|^
  def testRegular(): Unit ={
    val pattern = "([a-z]|[A-Z]|_|\\+|-|\\*|&|\\|\\!|\\~|\\<|\\>|\\=|\\/|\\%|\\?|\\.|\\$|\\^)([a-z]|[A-Z]|_|\\+|-|\\*|&|\\|\\!|\\~|\\<|\\>|\\=|\\/|\\%|\\?|\\.|\\$|\\^|[0-9])*".r //
    println((pattern findAllIn "<= findDix456+-*&|! x - y").mkString(","))
  }
}
