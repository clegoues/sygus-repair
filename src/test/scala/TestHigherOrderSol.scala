import engines.coreast._

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, HashMap}
import extsolvers.common.{MutantSolution, Solution}

/**
  * Created by dxble on 10/13/16.
  */
object TestHigherOrderSol {
  type SynthesizedSolution = mutable.HashMap[Int, Solution]
  val SIZE = 3

  def test2() = {
    def helper(list: List[Int], tailOfWhole: Boolean): List[(Int, Int)] = {
      println("whole:"+list)
      list match {
        case hd::tl => {
          val sols = tl match {
            case htl::tl2 => {
              val res = (hd,htl)
              tl2 match {
                case Nil => List[(Int, Int)](res)
                case _ => res :: helper(hd::tl2, false)
              }
            }
            case Nil => List[(Int,Int)]()
          }
          println("tail:"+tl)
          val tailSol = if(tailOfWhole) helper(tl, true) else List[(Int, Int)]()
          sols ++ tailSol
        }
        case Nil => List[(Int, Int)]()
      }
    }

    val data = List[Int](1,2,3,4,5)
    /*val res = data match {
      case hd::tl => helper(data) ++ helper(tl)
      case Nil => List[(Int,Int)]()
    }*/
    //println(res)
    println(helper(data, true))
  }

  def test3() = {

  }
  def test() = {
    def helper(list: List[(SynthesizedSolution, mutable.HashSet[Int])], tailOfWhole: Boolean): List[SynthesizedSolution] = {
      list match {
        case hd::tl => {
          // println("whole"+hd::tl)
          val sols = tl match {
            case htl::tl2 => {
              // println("tail"+htl::tl2)
              // If the unition of the sets of passed constraints has size equal with allConstraints, we combine the mutant
              val hSol = if(hd._2.union(htl._2).size == SIZE){
                //Get the higher order mutant here
                val sol1 = hd._1
                val sol2 = htl._1
                // now turn the Solution in SynthesizedSolution to higher order
                val higherOrderSol = sol1.foldLeft(new SynthesizedSolution){
                  (res, s) => {
                    val combinedSolution = sol2.getOrElse(s._1, null).createHigherOrder(s._2)
                    res += s._1 -> combinedSolution
                    res
                  }
                }
                List[SynthesizedSolution](higherOrderSol)
              }else //Otherwise we do not combine them
                List[SynthesizedSolution]()

              tl2 match {
                case Nil => {hSol}
                case _ => hSol ++ helper(hd::tl2, false)
              }
            }
            case Nil => {List[SynthesizedSolution]()}
          }
          val tailSols = if(tailOfWhole) helper(tl, true) else List[SynthesizedSolution]()
          sols ++ tailSols
        }
        case Nil => {List[SynthesizedSolution]()}
      }
    }
    val testData1 = new SynthesizedSolution
    testData1 += 1 -> new MutantSolution(new FunctionSignature("f1", new ArrayBuffer[InternalTerm]()), And(BoolConst("true"), BoolConst("false")), 0.1, BoolConst("false"), null)

    val s1 = mutable.HashSet[Int](1,2)

    val testData2 = new SynthesizedSolution
    testData2 += 1 -> new MutantSolution(new FunctionSignature("f1", new ArrayBuffer[InternalTerm]()), And(BoolVar("x"), BoolConst("false")), 0.1, BoolConst("false"), null)

    val s2 = mutable.HashSet[Int](3)

    val testData3 = new SynthesizedSolution
    testData3 += 1 -> new MutantSolution(new FunctionSignature("f1", new ArrayBuffer[InternalTerm]()), And(BoolVar("y"), BoolConst("false")), 0.1, BoolConst("false"), null)

    val s3 = mutable.HashSet[Int](1)

    val testData4 = new SynthesizedSolution
    testData4 += 1 -> new MutantSolution(new FunctionSignature("f1", new ArrayBuffer[InternalTerm]()), And(BoolVar("z"), BoolConst("false")), 0.1, BoolConst("false"), null)

    val s4 = mutable.HashSet[Int](2,3)

    println(helper(List[(SynthesizedSolution, mutable.HashSet[Int])]((testData1, s1), (testData2, s2), (testData3, s3), (testData4, s4)), true))
    //println(helper(List[(SynthesizedSolution, mutable.HashSet[Int])]((testData1, s1),(testData3, s3), (testData4, s4))))

  }

  def main(args: Array[String]) {
    test()
    //test2()
  }
}
