import synthlib2parser._

/**
  * Created by dxble on 7/8/16.
  */

object TestParser {
  def main(args: Array[String]) {
    //val x = Synthlib2Compiler("(set-logic LIA)\n(define-fun findx1 ((x1 Int) (y1 Int)) Int (- x y))\n(synth-fun findIdx ((y1 Int) (y2 Int)) Int \n((Start Int (0 1 y1 y2 (+ Start Start) (- Start Start)))))")
    //println(x)
    //val x = Synthlib2Compiler(scala.io.Source.fromFile("/home/dxble/workspace/synthesis/mytest/distance.sl").mkString, true)
    //println(x)
    test1()
    //testParsingAssert()
  }

  def test1(): Unit ={
    val x = Synthlib2Compiler(scala.io.Source.fromFile("/home/dxble/workspace/repairtools/angelix/src/af2sygus/src/test/resources/testparser/origexpr").mkString, true)
    println(x)
  }

  def testParsingAssert(): Unit ={
    val x = Synthlib2Compiler(scala.io.Source.fromFile("/home/dxble/workspace/repairtools/angelix/src/af2sygus/src/test/resources/testparser/assert").mkString, true)
    println(x)
  }
}
