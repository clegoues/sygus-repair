package engines.visitor

import engines.coreast.{FunctionSignature, InternalTerm, _}
import synthlib2parser.scopemanager.SymbolTableScope
import synthlib2parser.{BoolSortExp, LiteralTerm, _}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 7/21/16.
  */
class OriginalExprVisitor extends ASTVisitorBase("OriginalExprVisitor"){
  var indivConstraint: CAST = null

  private def argList2InternalTermList(argList: ArgList): ArrayBuffer[InternalTerm] ={
    argList.args.foldLeft(new ArrayBuffer[InternalTerm]){
      (res, asPair) =>{
        asPair.sort match {
          case BoolSortExp() => {
            res.append(new BoolVar(asPair.name))
          }
          case IntSortExp() => {
            res.append(new IntVar(asPair.name))
          }
        }
        res
      }
    }
  }


  /*override def visitAssertCmd(cmd: AssertCmd) ={
    //val args = argList2InternalTermList(cmd.args)
    //val funDesign = new FunctionSignature(cmd.name, args)
    cmd.term match {
      case e:FunTerm => {
        val origExprInternalAST = visit(e)
        //originalExprs += (funDesign -> origExprInternalAST)
        indivConstraint = null// reset this variable to null
      }
      case e:SymbolTerm =>{
        val eSort = cmd.scope.lookup(e.symbolStr).getSort()
        eSort match {
          case BoolSortExp() => originalExprs += (funDesign -> new BoolVar(e.symbolStr, e.pos))
          case IntSortExp() => originalExprs += (funDesign ->new IntVar(e.symbolStr, e.pos))
        }
      }
      case e:LiteralTerm =>{
        e.litTerm.sort match {
          case BoolSortExp() => originalExprs += (funDesign ->new BoolConst(e.litTerm.litStr, e.pos))
          case IntSortExp() => originalExprs += (funDesign ->new IntConst(e.litTerm.litStr, e.pos))
        }
      }
    }
  }*/

  /*def visit(e: FunTerm, scope: SymbolTableScope = null): CAST = {
    val grammarDesign: ArrayBuffer[CAST] = new ArrayBuffer [CAST]()
    var i: Int = 0
    while (i < e.args.size) {
      val expr: Term = e.args(i)
      //Recursively call visit if expr is a FunTerm
      if (expr.isInstanceOf[FunTerm]) grammarDesign.append(visit(expr.asInstanceOf[FunTerm], scope))
      else if (expr.isInstanceOf[SymbolTerm]) {
        var exprSort: SortExp = null
        if(scope == null)
          exprSort = expr.getTermSort(theSymbolTable)
        else
          exprSort = scope.lookup(expr.asInstanceOf[SymbolTerm].symbolStr).getSort()
        if (exprSort.isInstanceOf[BoolSortExp])grammarDesign.append(new BoolVar(expr.asInstanceOf[SymbolTerm].symbolStr, expr.pos))
        else grammarDesign.append(new IntVar(expr.asInstanceOf[SymbolTerm].symbolStr, expr.pos))
      }
      else if (expr.isInstanceOf[LiteralTerm]) {
        val exprSort: SortExp = expr.asInstanceOf[LiteralTerm].litTerm.sort
        //val exprSort = expr.getTermSort(theSymbolTable)
        if (exprSort.isInstanceOf[BoolSortExp]) grammarDesign.append(new BoolConst(expr.asInstanceOf[LiteralTerm].litTerm.litStr, expr.pos))
        else grammarDesign.append(new IntConst(expr.asInstanceOf[LiteralTerm].litTerm.litStr, expr.pos))
      }
      else println(expr)
      i += 1
    }
    if (e.funName == "and") {
      indivConstraint = new And(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == "or") {
      indivConstraint = new Or(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == "not") {
      indivConstraint = new Not(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == ">") {
      indivConstraint = new Greater(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "<") {
      indivConstraint = new LessThan(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == ">=") {
      indivConstraint = new GTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "<=") {
      indivConstraint = new LTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "=") {
      indivConstraint = new Equal(grammarDesign(0), grammarDesign(1), e.pos)
    }
    else if (e.funName == "+") {
      indivConstraint = new Plus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "-") {
      indivConstraint = new Minus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "/") {
      indivConstraint = new Div(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "*") {
      indivConstraint = new Mult(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "=>"){
      indivConstraint = new Implies(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else{// function name defined by user
      //println(e)
      indivConstraint = new FunctionSignature(e.funName, grammarDesign.foldLeft(new ArrayBuffer[InternalTerm]){ (res, arg) => {
        res.append(arg.asInstanceOf[InternalTerm])
        res
      }}, e.pos)
    }
    return indivConstraint
  }*/
}
