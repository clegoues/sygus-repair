package engines

import java.io.FileWriter
import java.util

import config.ConfigurationPropertiesX

/**
  * Created by dxble on 2/15/17.
  */
object FixedDefects {
  val param = "/home/dxble/workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3/t14/last-angelic-forest.json /home/dxble/workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3/t14/extracted patch /home/dxble/workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3/t14/config.json Meta /home/dxble/workspace/synthesis/sygus-comp14/solvers/CVC4-0205-4/cvc4 /home/dxble/workspace/sysgus-repair/sygusResult/parseResult.py /home/dxble/workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3/t14/addconfig.prop"
  ///home/dxble/workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3/t1
  val defect = "t1"
  def main(args: Array[String]): Unit = {
    val p = param.replace("t14",defect)
    engines.RepairDriver.main(p.split(" "))
    //autorunOneFeature()
  }

  def autorunOneFeature(): Unit = {
    val featureSet = List(2,5,7,3,6,8)
    for (i <- 1 to 20){
      val p = param.replace("t14", "t"+i.toString)
      val config = p.split(" ").last
      ConfigurationPropertiesX.load(config)
      val rep1 = ConfigurationPropertiesX.getProperty("actualVarTypeFile").replace("Desktop/initRes", "workspace/FSE2017-S3-SyntaxSemanticRepairData-master/prioritizeS3")
      ConfigurationPropertiesX.properties.setProperty("actualVarTypeFile", rep1)
      ConfigurationPropertiesX.properties.setProperty("feature.syntax", "false")
      ConfigurationPropertiesX.properties.setProperty("feature.semantic", "false")
      featureSet.foreach(f => {
        //println("Running t"+i.toString +" feature "+f)
        ConfigurationPropertiesX.properties.setProperty("feature.only", f.toString)
        val newConfig = config+f
        ConfigurationPropertiesX.properties.store(new FileWriter(newConfig), "")
        //ConfigurationPropertiesX.clearConfig()
        val p1 = p.replace("patch", "patch_t"+i.toString+"_"+f.toString).replace(config, newConfig)
        val cmd = "java -jar /home/dxble/workspace/sysgus-repair/out/artifacts/sysgus_repair_jar/sysgus-repair.jar " + p1
        println(cmd)
        //import scala.sys.process._
        //val res = cmd.!!
        //println(res)
        /*val executor = new ProcessExecutor
        val list = new util.ArrayList[String]()
        cmd.split(" ").foreach(s => list.add(s))
        println("Executing Jar ...")
        val start = System.currentTimeMillis()
        val res = executor.execute(list, 120)
        val end = System.currentTimeMillis()
        println("Running time of jar: "+ (end - start)/1000.0)
        println("Finished!")
        println("OUTPUT======")
        val iter = res.iterator()
        while (iter.hasNext){
          println(iter.next())
        }
        println("Debug")*/
      })
    }
  }
}
