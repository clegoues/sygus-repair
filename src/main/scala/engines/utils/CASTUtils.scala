package engines.utils

import extsolvers.common.VarsCollector
import engines.coreast.{BoolVar, Equal, IntVar, LTE, _}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by dxble on 7/22/16.
  */
object CASTUtils {

  def transformASTs(idASTMap: ListBuffer[(Int, CAST)], suspiciousIds: List[String],
                    additionalVars: Map[String, (List[String], List[String])]
                   ): mutable.HashMap[String, (CAST, mutable.HashSet[String], mutable.HashSet[String])] = {
    //Return stmtid -> (GrammarDesign ast, collection of vars that have unchanged value, collection of vars that have changed value)
    idASTMap.foldLeft(new mutable.HashMap[String, (CAST, mutable.HashSet[String], mutable.HashSet[String])]()){
      (res, idAST) =>{
        val (id, ast) = idAST
        val stmtLine = suspiciousIds(id)
        additionalVars.get(stmtLine) match {
          case None => {
            val varsChangedValue = new mutable.HashSet[String]()
            // We only care to transform ast if its size is big enough, otherwise small size does not matter to use optimization by the transformation of ast
            val transformedAST = visitAST2Change(ast, List[String](), ast, new mutable.HashSet[String], varsChangedValue) /*if(ast.size() > 3) visitAST2Change(ast, List[String](), ast, new mutable.HashSet[String], varsChangedValue) else {
              ast match {
                case _: BoolOperatorsCAST => BoolExpr2Change(ast.asInstanceOf[BoolOperatorsCAST])
                case _: ArithOperatorsCAST => IntExpr2Change(ast.asInstanceOf[ArithOperatorsCAST])
              }
            }*/
            res.get(stmtLine) match {
              case None => {
                res += (stmtLine -> (transformedAST, new mutable.HashSet[String], varsChangedValue))
              }
              case Some(ast) => throw new RuntimeException("Unexpected2!")
            }
          } //throw new RuntimeException("Unexpected!")
          case Some (pair) => {
            val (_, varsUnchangedValues) = pair
            val varsChangedValue = new mutable.HashSet[String]()
            val varsUnchangedValue = varsUnchangedValues.foldLeft(new mutable.HashSet[String]){(res, v) => {res.add(v); res}} //new mutable.HashSet[String]() // Bachle: new updates
            val transformedAST = visitAST2Change(ast, varsUnchangedValues, ast, varsUnchangedValue, varsChangedValue) /*if(ast.size() > 3) visitAST2Change(ast, varsUnchangedValues, ast, varsUnchangedValue, varsChangedValue) else {
              ast match {
                case _: BoolOperatorsCAST => BoolExpr2Change(ast.asInstanceOf[BoolOperatorsCAST])
                case _: ArithOperatorsCAST => IntExpr2Change(ast.asInstanceOf[ArithOperatorsCAST])
              }
            }*/
            res.get(stmtLine) match {
              case None => {
                res += (stmtLine -> (transformedAST, varsUnchangedValue, varsChangedValue))
              }
              case Some(ast) => throw new RuntimeException("Unexpected2!")
            }

          }
        }
        res
      }
    }
  }


  def visitAST2Change(ast: CAST, varUnchangedValues: List[String], parent: CAST, collectVarsUnchangedValue: mutable.HashSet[String],
                      collectVarschangedValue: mutable.HashSet[String]): CAST ={
    ast match {
      case e: And =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[BoolExprNoChange] && res2.isInstanceOf[BoolExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(And(res1.asInstanceOf[BoolOperatorsCAST], res2.asInstanceOf[BoolOperatorsCAST]))
      }
      case e: Or =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[BoolExprNoChange] && res2.isInstanceOf[BoolExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(Or(res1.asInstanceOf[BoolOperatorsCAST], res2.asInstanceOf[BoolOperatorsCAST]))
      }
      case e: Not =>{
        val res = visitAST2Change(e.opr,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res.isInstanceOf[BoolExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(Not(res.asInstanceOf[BoolOperatorsCAST]))
      }
      case e: Greater =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(Greater(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: GTE =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(GTE(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: LTE =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue,collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(LTE(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: LessThan =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(LessThan(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: Equal =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        /*
        (res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange]) ||
          (res1.isInstanceOf[BoolExprNoChange] && res2.isInstanceOf[BoolExprNoChange]) ||
          (res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[BoolExprNoChange]) ||
          (res1.isInstanceOf[BoolExprNoChange] && res2.isInstanceOf[IntExprNoChange])
         */
        if(res1.isInstanceOf[ExpNoChange] && res2.isInstanceOf[ExpNoChange] && !res1.equals(res2))
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(Equal(res1, res2))
      }
      case e: BoolVar =>{
        /**
          * Similar to IntVar below
          */
        if(varUnchangedValues.contains(e.name) && !parentContain2ChangeVars(parent, varUnchangedValues)){
          collectVarsUnchangedValue.add(e.name)
          return new BoolExprNoChange(e)
        }else {
          collectVarschangedValue.add(e.name)
          return BoolExpr2Change(e)
        }
      }
      case e: IntVar =>{
        /**
          * If this var is an unchangedValue var and its is relative to another unchanged Value var
          * => it is a no change int exp
          * Otherwise, it is subject to change
          **/
        if(varUnchangedValues.contains(e.name) && !parentContain2ChangeVars(parent, varUnchangedValues)){
          collectVarsUnchangedValue.add(e.name)
          return new IntExprNoChange(e)
        }else{
          collectVarschangedValue.add(e.name)
          return IntExpr2Change(e)
        }
      }
      case e: IntConst =>{
        if(parentContainUnchangedVars(parent, varUnchangedValues))
          return new IntExprNoChange(e)
        else
          return new IntExpr2Change(e)
      }
      case e: BoolConst =>{
        if(parentContainUnchangedVars(parent, varUnchangedValues))
          return new BoolExprNoChange(e)
        else
          return new BoolExpr2Change(e)
      }
      case e: Plus =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new IntExprNoChange(e)
        else
          return new IntExpr2Change(Plus(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: Minus =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new IntExprNoChange(e)
        else
          return new IntExpr2Change(Minus(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: Div =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new IntExprNoChange(e)
        else
          return new IntExpr2Change(Div(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
      case e: Mult =>{
        val res1 = visitAST2Change(e.opr1,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        val res2 = visitAST2Change(e.opr2,varUnchangedValues, e, collectVarsUnchangedValue, collectVarschangedValue)
        if(res1.isInstanceOf[IntExprNoChange] && res2.isInstanceOf[IntExprNoChange])
          return new IntExprNoChange(e)
        else
          return new IntExpr2Change(Mult(res1.asInstanceOf[ArithOperatorsCAST], res2.asInstanceOf[ArithOperatorsCAST]))
      }
    }
  }

  def parentContainUnchangedVars(parent: CAST, unchangedVars: List[String]): Boolean ={
    if(unchangedVars == null)
      return false

    parent match {
      case e: And => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Or => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Not => return parentContainUnchangedVars(e.opr, unchangedVars)
      case e: GTE => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: LTE => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: LessThan => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Greater => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Equal => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: BoolVar => return unchangedVars.contains(e.name)
      case e: IntVar => return unchangedVars.contains(e.name)
      case e: Plus => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Minus => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Div => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case e: Mult => return parentContainUnchangedVars(e.opr1, unchangedVars) || parentContainUnchangedVars(e.opr2,unchangedVars)
      case _ => false
    }
  }

  def parentContain2ChangeVars(parent: CAST, unchangedVars: List[String]): Boolean ={
    if(unchangedVars == null)
      return true
    parent match {
      case e: And => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Or => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Not => return parentContain2ChangeVars(e.opr, unchangedVars)
      case e: GTE => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: LTE => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: LessThan => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Greater => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Equal => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: BoolVar => return !unchangedVars.contains(e.name)
      case e: IntVar => return !unchangedVars.contains(e.name)
      case e: Plus => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Minus => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Div => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case e: Mult => return parentContain2ChangeVars(e.opr1, unchangedVars) || parentContain2ChangeVars(e.opr2,unchangedVars)
      case _ => false
    }
  }

  def hasNoChangeExpr(ast: CAST): Boolean ={
    ast match {
      case BoolExprNoChange(_)| IntExprNoChange(_) => return true
      case BoolExpr2Change(e) => hasNoChangeExpr(e)
      case IntExpr2Change(e) => hasNoChangeExpr(e)
      case And(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case Not(opr) => hasNoChangeExpr(opr)
      case Or(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case Plus(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case Minus(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case Div(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case Mult(opr1, opr2) => hasNoChangeExpr(opr1) || hasNoChangeExpr(opr2)
      case _ => false
    }
  }

  def getChangedPart(ast: CAST): CAST = {
    ast match {
      //case e:BoolExprNoChange => e.toString
      //case e:IntExprNoChange => e.toString
      case e: BoolExprNoChange => {
        null
      }
      case e: IntExprNoChange => {
        null
      }
      case e: BoolExpr2Change => {
        e.op match {
          case op: BinOp => {
            if (op.getLeft().isInstanceOf[Exp2Change] && op.getRight().isInstanceOf[Exp2Change])
              op
            else if (op.getLeft().isInstanceOf[Exp2Change]) {
              op.getLeft()
            } else {
              op.getRight()
            }
          }
          case op: UnOp => {
            if (!hasNoChangeExpr(op))
              op
            else
              op.getOperand()
          }
          case bv: BoolVar => {
            bv
          }
          case bc: BoolConst => {
            bc
          }
        }
      }
      case e: IntExpr2Change => {
        e.op match {
          case op: BinOp => {

            if (op.getLeft().isInstanceOf[Exp2Change] && op.getRight().isInstanceOf[Exp2Change])
              op
            else if (op.getLeft().isInstanceOf[Exp2Change]) {
              op.getLeft()
            } else {
              op.getRight()
            }
          }
          case iv: IntVar => {
            iv
          }
          case ic: IntConst => ic
        }
      }
    }
  }

  def transformFixExpr(ast: CAST, fixExpr: String): String = {
    ast match {
      //case e:BoolExprNoChange => e.toString
      //case e:IntExprNoChange => e.toString
      case e: BoolExprNoChange => {
        e.op.toString
      }
      case e: IntExprNoChange => {
        e.op.toString
      }
      case e: BoolExpr2Change => {
        e.op match {
          case op: And => {

            if (op.opr1.isInstanceOf[BoolExpr2Change] && op.opr2.isInstanceOf[BoolExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " && " + res2 + ")"
            }
          }
          case op: Or => {

            if (op.opr1.isInstanceOf[BoolExpr2Change] && op.opr2.isInstanceOf[BoolExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " || " + res2 + ")"
            }
          }
          case op: Not => {
            if (!hasNoChangeExpr(op))
              fixExpr
            else
              "!(" + transformFixExpr(op, fixExpr) + ")"
          }
          case op: Greater => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " > " + res2 + ")"
            }
          }
          case op: LessThan => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " < " + res2 + ")"
            }
          }
          case op: GTE => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " >= " + res2 + ")"
            }
          }
          case op: LTE => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " || " + res2 + ")"
            }
          }
          case op: Equal => {

            if ((op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change]) ||
              (op.opr1.isInstanceOf[BoolExpr2Change] && op.opr2.isInstanceOf[BoolExpr2Change]) ||
              (op.opr1.isInstanceOf[BoolExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change]) ||
              (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change]))
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " == " + res2 + ")"
            }
          }
          case bv: BoolVar => {
            fixExpr
          }
        }
      }

      case e: IntExpr2Change => {
        e.op match {
          case op: Plus => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " + " + res2 + ")"
            }
          }
          case op: Minus => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " - " + res2 + ")"
            }
          }
          case op: Div => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " / " + res2 + ")"
            }
          }
          case op: Mult => {

            if (op.opr1.isInstanceOf[IntExpr2Change] && op.opr2.isInstanceOf[IntExpr2Change])
              fixExpr
            else {
              val res1 = transformFixExpr(op.opr1, fixExpr)
              val res2 = transformFixExpr(op.opr2, fixExpr)
              "(" + res1 + " * " + res2 + ")"
            }
          }
          case iv: IntVar => {
            fixExpr
          }
        }
      }
    }
  }

  def transformFixExprWrapper(astw: CAST, fixExprw: String): String ={
    if(!hasNoChangeExpr(astw))
      return fixExprw
    else
      transformFixExpr(astw, fixExprw)
  }

  def collectVars(ast: CAST): mutable.HashSet[String] = {
    val varsCollector = new VarsCollector
    ast.accept(varsCollector)
    varsCollector.varNames
  }
}
