package engines.coreast

//import z3.scala.Z3AST
import za.ac.sun.cs.green.expr.Expression

/**
  * Created by dxble on 7/15/16.
  */
case class DivisionByZeroException(message: String) extends Exception(message)

trait VisitorReturnType{
  def eq(v2: VisitorReturnType): Boolean
}
trait DataType extends VisitorReturnType
trait ArithType extends DataType{
  def greater(v2: ArithType): Boolean
  def lessThan(v2: ArithType): Boolean
  def lte(v2: ArithType): Boolean
  def gte(v2: ArithType): Boolean
  def plus(v2: ArithType): ArithType
  def minus(v2: ArithType): ArithType
  def mult(v2: ArithType): ArithType
  def div(v2: ArithType): ArithType
}
case class IntType(key: CAST, value: Int) extends ArithType{
  override def greater(v2: ArithType): Boolean = this.asInstanceOf[IntType].value > v2.asInstanceOf[IntType].value

  override def lte(v2: ArithType): Boolean = this.asInstanceOf[IntType].value <= v2.asInstanceOf[IntType].value

  override def eq(v2: VisitorReturnType): Boolean = {
    if(v2.isInstanceOf[IntType])
      this.asInstanceOf[IntType].value == v2.asInstanceOf[IntType].value
    else if(v2.isInstanceOf[BoolType])
      if(v2.asInstanceOf[BoolType].isFalse())
        this.asInstanceOf[IntType].value == 0
      else
        this.asInstanceOf[IntType].value == 1
    else
      throw new RuntimeException("Why comparing int type with this: "+v2)
  }

  override def lessThan(v2: ArithType): Boolean = this.asInstanceOf[IntType].value < v2.asInstanceOf[IntType].value

  override def gte(v2: ArithType): Boolean = this.asInstanceOf[IntType].value >= v2.asInstanceOf[IntType].value

  override def plus(v2: ArithType): ArithType = new IntType(null, this.value + v2.asInstanceOf[IntType].value)

  override def div(v2: ArithType): ArithType = {
    if(v2.asInstanceOf[IntType].value == 0)
      throw DivisionByZeroException("division by zero exception!")

    new IntType(null, this.value / v2.asInstanceOf[IntType].value)
  }

  override def mult(v2: ArithType): ArithType = new IntType(null, this.value * v2.asInstanceOf[IntType].value)

  override def minus(v2: ArithType): ArithType = new IntType(null, this.value - v2.asInstanceOf[IntType].value)
}

case class BoolType(key: CAST, value: Boolean) extends DataType{
  def isTrue(): Boolean = value == true

  def eq(v2: BoolType): Boolean = this.value == v2.value

  def isFalse(): Boolean = value == false

  def and(v2: BoolType): Boolean = this.value && v2.value

  def or(v2: BoolType): Boolean = this.value || v2.value

  def not(): Boolean = !this.value

  def implies(v2: BoolType) = this.not() || v2.value

  override def eq(v2: VisitorReturnType): Boolean = this.asInstanceOf[BoolType].value == v2.asInstanceOf[BoolType].value

}
case class ExpList(exps: List[CAST]) extends VisitorReturnType{

  //TODO: We haven't needed this eq for ExpList yet. Consider a proper implementation of this later.
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)

  def concat(that: ExpList): ExpList = {
    return new ExpList(this.exps ::: that.exps)
  }
}
case class EmptyType() extends VisitorReturnType with DataType{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)
}
abstract class Scope extends VisitorReturnType{
  def cross(that: Scope): Scope

  implicit class Crossable[X](xs: List[X]) {
    def crossP[Y](ys: List[Y]) = for { x <- xs; y <- ys } yield (x, y)
  }
}
case class ScopeName(scope: String/*, expansions: List[String] = null*/) extends Scope{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)

  override def cross(that: Scope): Scope = {
    if(that.isInstanceOf[ScopeName])
      ScopeCrossProduct(List[(ScopeName,ScopeName)]((this, that.asInstanceOf[ScopeName])))
    else{//ScopeCrossProduct type
      throw new RuntimeException("Do not handle cross product of scope name and other types!")
    }
  }
}
case class ScopeList(scopesList: List[ScopeName]) extends Scope{

  override def cross(that: Scope): Scope = {
    if(that.isInstanceOf[ScopeList]){
      val crossed = scopesList crossP that.asInstanceOf[ScopeList].scopesList
      return ScopeCrossProduct(crossed)
    } else {//ScopeCrossProduct type
      throw new RuntimeException("Do not handle cross product of scope list and other types!")
    }
  }

  override def hashCode(): Int ={
    scopesList.foldLeft(0){
      (res, scope) => res + scope.hashCode()
    }
  }

  override def equals(that: Any): Boolean ={
    if(that.isInstanceOf[ScopeList]){
      val castThat = that.asInstanceOf[ScopeList]
      val thatScope = castThat.scopesList.toSet
      val thisScope = scopesList.toSet
      thatScope.find(s => !thisScope.contains(s)) match {
        case None => return true
        case Some(_) => return false
      }
    }else{
      return false
    }
  }
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)
}
case class ScopeCrossProduct(scopes: List[(ScopeName, ScopeName)]) extends Scope{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)

  override def cross(that: Scope): Scope = {
      throw new RuntimeException("Do not handle cross product of scope cross product!")
  }
}
/*case class Z3ASTReturnType(ast: Z3AST) extends VisitorReturnType{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)
  def getAST(): Z3AST = ast
}*/
case class GreenPCReturnType(exp: Expression) extends VisitorReturnType{
  override def eq(v2: VisitorReturnType): Boolean = {
    if(v2.isInstanceOf[GreenPCReturnType]){
      v2.asInstanceOf[GreenPCReturnType].exp.equals(exp)
    }else false
  }
}
