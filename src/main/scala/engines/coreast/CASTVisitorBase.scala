package engines.coreast

/**
  * Created by dxble on 7/15/16.
  */
/**
  * The default implementation of this visitor is intended for concrete evaluation and expression collection (@see DataType)
  * If user wants to implement different behaviours, user will then need to reimplement
  * individual visit methods of this visitor
  */

abstract class CASTVisitorBase{
  def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType

  def preVisit(node: CAST): (VisitorReturnType, Boolean) = {(EmptyType(), true)}
  def postVisit(node: CAST): Unit = {}

  def visitGreater(greater: Greater): VisitorReturnType = {
    new BoolType(greater, greater.opr1.accept(this).asInstanceOf[ArithType].greater(greater.opr2.accept(this).asInstanceOf[ArithType]))
  }

  def visitLessThan(lessThan: LessThan): VisitorReturnType ={
    new BoolType(lessThan, lessThan.opr1.accept(this).asInstanceOf[ArithType].lessThan(lessThan.opr2.accept(this).asInstanceOf[ArithType]))
  }

  def visitLTE(lTE: LTE): VisitorReturnType ={
    new BoolType(lTE, lTE.opr1.accept(this).asInstanceOf[ArithType].lte(lTE.opr2.accept(this).asInstanceOf[ArithType]))
  }

  def visitGTE(gTE: GTE): VisitorReturnType ={
    new BoolType(gTE, gTE.opr1.accept(this).asInstanceOf[ArithType].gte(gTE.opr2.accept(this).asInstanceOf[ArithType]))
  }

  @throws(classOf[DivisionByZeroException])
  def visitEqual(equal: Equal): VisitorReturnType={
    new BoolType(equal, equal.opr1.accept(this).eq(equal.opr2.accept(this)))
  }
  def visitAnd(and: And): VisitorReturnType={
    new BoolType(and, and.opr1.accept(this).asInstanceOf[BoolType].and(and.opr2.accept(this).asInstanceOf[BoolType]))
  }

  def visitOr(or: Or): VisitorReturnType={
    new BoolType(or, or.opr1.accept(this).asInstanceOf[BoolType].or(or.opr2.accept(this).asInstanceOf[BoolType]))
  }

  def visitNot(not: Not): VisitorReturnType={
    new BoolType(not, not.opr.accept(this).asInstanceOf[BoolType].not())
  }

  def visitImplies(implies: Implies): VisitorReturnType={
    new BoolType(implies, implies.opr1.accept(this).asInstanceOf[BoolType].implies(implies.opr2.accept(this).asInstanceOf[BoolType]))
  }

  def visitBoolVar(boolVar: BoolVar): VisitorReturnType

  def visitBoolConst(boolConst: BoolConst): VisitorReturnType={
    new BoolType(boolConst, boolConst.value.toBoolean)
  }

  def visitPlus(plus: Plus): VisitorReturnType={
    val res = plus.opr1.accept(this).asInstanceOf[ArithType].plus(plus.opr2.accept(this).asInstanceOf[ArithType])
    res match {
      case IntType(key, value) => new IntType(plus, value)
      case _ => throw new RuntimeException("Unexpected DataType")
    }
  }

  def visitMinus(minus: Minus): VisitorReturnType={
    val res = minus.opr1.accept(this).asInstanceOf[ArithType].minus(minus.opr2.accept(this).asInstanceOf[ArithType])
    res match {
      case IntType(key, value) => new IntType(minus, value)
      case _ => throw new RuntimeException("Unexpected DataType")
    }
  }

  @throws(classOf[DivisionByZeroException])
  def visitDiv(div: Div): VisitorReturnType={
    val res = div.opr1.accept(this).asInstanceOf[ArithType].div(div.opr2.accept(this).asInstanceOf[ArithType])
    res match {
      case IntType(key, value) => new IntType(div, value)
      case _ => throw new RuntimeException("Unexpected DataType")
    }
  }

  def visitMult(mult: Mult): VisitorReturnType={
    val res = mult.opr1.accept(this).asInstanceOf[ArithType].mult(mult.opr2.accept(this).asInstanceOf[ArithType])
    res match {
      case IntType(key, value) => new IntType(mult, value)
      case _ => throw new RuntimeException("Unexpected DataType")
    }
  }

  def visitIntConst(intConst: IntConst): VisitorReturnType={
    new IntType(intConst, intConst.value.toInt)
  }

  def visitIntVar(intVar: IntVar): VisitorReturnType
}
