package engines

import scala.util.Random


/**
  * Created by dxble on 6/27/16.
  */
object Strategy {

  def randomlyDropConstraints(sygusInput: String, config: SynthesisConfig, rand: Random): String ={
    val constraints = sygusInput.split("\n").filter(p => p.contains("constraint") &&
                                !p.contains("NOT_WANTED") && !p.startsWith(";("))
    val restored = sygusInput.replace(";(","(")
    val toRemove = constraints(rand.nextInt(constraints.size))
    return restored.replace(toRemove,";"+toRemove)
  }
}
