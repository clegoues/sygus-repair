package engines.features

import java.util.{Properties, Random}
import java.util.regex.Pattern

import com.github.gumtreediff.actions.ActionGenerator
import com.github.gumtreediff.matchers.Matchers
import engines.features.jdtgum.MyJdtTreeGenerator
import engines.utils.CASTUtils
import extsolvers.common.modelcounting.{Conversions, ToGreenPCConverter}
import extsolvers.common.Solution
import engines.coreast.{FunctionSignature, _}
import extsolvers.metasearch.ranking.{CosineSimilarity, Exp2VectorConverter}
import org.apache.log4j.Logger
import za.ac.sun.cs.green.Green
import za.ac.sun.cs.green.expr.{Expression, IntConstant, IntVariable, Operation}
import za.ac.sun.cs.green.util.Configuration
import za.ac.sun.cs.green.Instance
import org.apfloat.Apint

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 9/30/16.
  */
abstract class RankingFeature {
  protected var threshold: Double = 0.5
  protected var startingSketch: CAST = null
  protected var weight: Double = 1.0
  protected lazy val logger = Logger.getLogger(this.getClass)

  def setWeight(w: Double) = weight = w
  def getWeight(): Double = weight
  def setStartingSketch(sketch: CAST) = startingSketch = sketch
  def getStartingSketch() = startingSketch
  def setThreshold(t: Double) = threshold = t
  def getThreshold(): Double = threshold
  def satisfyFeature(sol: Solution): Boolean
  def featureUniqueCode(): Int
}
case class EnoughVarsFeature(asts: mutable.HashMap[String, (CAST, mutable.HashSet[String], mutable.HashSet[String])]) extends RankingFeature{
  lazy val startingSketchVarNames = CASTUtils.collectVars(startingSketch)

  override def satisfyFeature(solution: Solution): Boolean = {
    solution.transformMutant(startingSketch)
    val varNames = CASTUtils.collectVars(solution.getTransformedCAST())
    if(varNames.size <= 2)// Bachle: new updates: if the number of vars is small, we dont care.
      return true

    val stmtID = solution.getFuncSignature().funName.replace("f_","")
    asts.get(stmtID) match {
      case None => false
      case Some(v) => {
        // if variable names from solution contains (is super set of) all variables that have changed value
        // if starting sketch is too small, we dont care, just return true
        if(startingSketch.size()>=3 && v._3.size>0) {
          //v._3.subsetOf(varNames)
          // Just now changed to the bellow
          startingSketchVarNames.subsetOf(varNames)
        }
        else true
      }
    }
  }

  override def featureUniqueCode(): Int = 1
}
case class CosineSimilarityFeature() extends RankingFeature{

  def this(thresh: Double){
    this()
    setThreshold(thresh)
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    val varCount = new CollectMajorVars
    startingSketch.accept(varCount)
    if(varCount.variableCount.size == 0)
      return true

    val vectorSol = new Exp2VectorConverter
    sol.getSolution().accept(vectorSol)

    val vectorOrig = new Exp2VectorConverter
    sol.getOrigExp().accept(vectorOrig)
    val simScore = CosineSimilarity.cosineSimilarity(vectorOrig.vector, vectorSol.vector)
    setWeight(10.0)
    sol.addFeatureScores(this.featureUniqueCode(), simScore * weight)
    simScore >= threshold
  }

  override def featureUniqueCode(): Int = 2
}
case class NoDupOnSameOperator() extends RankingFeature{
  class checkNoDupVisitor() extends CASTVisitorBase{
    var dup = false

    override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

    private def visitBin(binOp: BinOp): VisitorReturnType = {
      if(binOp.getLeft().mangledString().contains(binOp.getRight().mangledString()) || binOp.getRight().mangledString().contains(binOp.getLeft().mangledString()))
        dup = true
      if(!dup) {
        binOp.getLeft().accept(this)
        binOp.getRight().accept(this)
      }
      EmptyType()
    }

    override def visitAnd(and: And): VisitorReturnType = {
      visitBin(and)
    }

    override def visitOr(or: Or): VisitorReturnType = {
      visitBin(or)
    }

    override def visitNot(not: Not): VisitorReturnType = {
      not.getOperand().accept(this)
    }

    override def visitGreater(greater: Greater): VisitorReturnType = {
      visitBin(greater)
    }

    override def visitGTE(gTE: GTE): VisitorReturnType = {
      visitBin(gTE)
    }

    override def visitEqual(eq: Equal): VisitorReturnType = {
      visitBin(eq)
    }

    override def visitLessThan(lessThan: LessThan): VisitorReturnType ={
      visitBin(lessThan)
    }

    override def visitLTE(lTE: LTE): VisitorReturnType = {
      visitBin(lTE)
    }

    override def visitPlus(plus: Plus): VisitorReturnType = {
      visitBin(plus)
    }

    override def visitMinus(minus: Minus): VisitorReturnType = {
      visitBin(minus)
    }

    override def visitDiv(div: Div): VisitorReturnType = {
      visitBin(div)
    }

    override def visitMult(mult: Mult): VisitorReturnType = {
      visitBin(mult)
    }

    override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = BoolType(null, true)

    override def visitIntVar(intVar: IntVar): VisitorReturnType = IntType(null, 1)
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    sol.transformMutant(startingSketch)
    val noDupVisitor = new checkNoDupVisitor
    sol.getTransformedCAST().accept(noDupVisitor)
    !noDupVisitor.dup
  }

  override def featureUniqueCode(): Int = 3
}
case class CollectMajorVars() extends CASTVisitorBase{
  val variableCount: mutable.HashMap[String, Int] = new mutable.HashMap[String, Int]()

  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    val count = variableCount.getOrElse(boolVar.name, 0)
    variableCount.update(boolVar.name, count+1)
    BoolType(null, true)
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    val count = variableCount.getOrElse(intVar.name, 0)
    variableCount.update(intVar.name, count+1)
    IntType(null, 1)
  }

  def majorVars(): scala.collection.Set[String] = {
    // max by count, return variable names with biggest count
    if(variableCount.size>0)
      variableCount.groupBy(_._2).maxBy(_._1)._2.keySet
    else
      new mutable.HashSet[String]()
  }
}
case class MajorVarNotChange() extends RankingFeature{
  lazy val stSketchMajorVarCollector = new CollectMajorVars
  lazy val startingSketchMajorVars: scala.collection.Set[String] = {
    startingSketch.accept(stSketchMajorVarCollector)
    stSketchMajorVarCollector.majorVars()
  }

  override def featureUniqueCode(): Int = 4

  override def satisfyFeature(sol: Solution): Boolean = {
    sol.transformMutant(startingSketch)
    val majorVarCount = new CollectMajorVars
    sol.getTransformedCAST().accept(majorVarCount)
    // check if major vars (vars occurring most frequently) in starting sketch
    // is a subset of major vars in transformed solution. Size of variables collected > 1, we do subset check.
    // Otherwise just return true because original expression can be small, which involves only one variable
    if(stSketchMajorVarCollector.variableCount.size > 1 && startingSketch.size() >= 3)
      startingSketchMajorVars.subsetOf(majorVarCount.majorVars())
    else true
  }
}

case class GumtreeDiffFeature() extends RankingFeature{

  def this(thresh: Double){
    this()
    threshold = thresh
  }

  private def diff(orig: String, sol: String) ={
    // Note that we need to create stub code for JDT to parse appropriately
    val stubOrig = "public class X{ public void replace(){return "+orig+";}}"
    val stubSol = "public class X{ public void replace(){return "+sol+";}}"
    val stg = new MyJdtTreeGenerator()
    val dtg = new MyJdtTreeGenerator()
    val stc= stg.generateFromString(stubOrig)
    val dtc= dtg.generateFromString(stubSol)
    val m=Matchers.getInstance().getMatcher(stc.getRoot(), dtc.getRoot())
    m.`match`()
    val g = new ActionGenerator(stc.getRoot(), dtc.getRoot(), m.getMappings())
    g.generate()
    val actions = g.getActions()
    actions.size()
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    val varCount = new CollectMajorVars
    startingSketch.accept(varCount)
    if(varCount.variableCount.size == 0)
      return true

    sol.transformMutant(startingSketch)
    val numActions = diff(startingSketch.mangledString(), sol.getTransformedCAST().mangledString())
    if(numActions > 0){
      setWeight(0.1)
      sol.addFeatureScores(featureUniqueCode(), (1.5f/numActions) * weight)
    }
    numActions <= threshold //&& numActions > 0
  }

  override def featureUniqueCode(): Int = 5
}
case class ModelCounterCloserFeature(lattePath: String, intL: Int, intU: Int) extends RankingFeature{
  var stSketchBoundingExp: Expression = null
  var stSketchBoundedVars: mutable.HashMap[String, Boolean] = null
  var stSketchNotGreen: Expression = null
  var stSketchGreen: Expression = null

  private def stSketch2Green() = {
    if(stSketchGreen == null) {
      val converter = new ToGreenPCConverter(intL, intU, new mutable.HashMap[String, Boolean]())
      stSketchGreen = startingSketch.accept(converter).asInstanceOf[GreenPCReturnType].exp
      stSketchBoundingExp = converter.boundingExpression
      stSketchBoundedVars = converter.alreadyBoundedVars
      val stSketchNot = Conversions.toNNF(Not(startingSketch.asInstanceOf[BoolOperatorsCAST]))
      stSketchNotGreen = stSketchNot.accept(new ToGreenPCConverter(intL, intU, stSketchBoundedVars)).asInstanceOf[GreenPCReturnType].exp
    }
  }

  private def configure() = {
    val props: Properties = new Properties
    props.setProperty("green.services", "count")
    val solver: Green = new Green
    //props.setProperty("green.store", "za.ac.sun.cs.green.store.redis.RedisStore");
    //props.setProperty("green.service.count", "latte");
    //props.setProperty("green.service.count.latte","za.ac.sun.cs.green.service.latte.CountLattEService");
    props.setProperty("green.service.count", "(bounder (canonize latte))")
    props.setProperty("green.service.count.latte", "za.ac.sun.cs.green.service.latte.CountLattEService")
    props.setProperty("green.service.count.bounder", "za.ac.sun.cs.green.service.bounder.BounderService")
    props.setProperty("green.service.count.canonize", "za.ac.sun.cs.green.service.canonizer.SATCanonizerService")

    props.setProperty("green.latte.path", lattePath)
    val config: Configuration = new Configuration(solver, props)
    config.configure
    solver
  }

  // Note: ast is required to be in DNF form
  def modelCount(solver: Green, ast: CAST): Apint = {
    ast match {
      case Or(a,b) => {
        (Array[CAST](a, b, And(a,b)))
        val counta = modelCount(solver, a)
        val countb = modelCount(solver, b)
        val countab = modelCount(solver, And(a,b))
        counta.add(countb).subtract(countab)
      }

      case _ => {
        val converter = new ToGreenPCConverter(intL, intU, new mutable.HashMap[String, Boolean]())
        val greenPC = ast.accept(converter).asInstanceOf[GreenPCReturnType].exp
        val greenPCBounded = new Operation(Operation.Operator.AND, greenPC, converter.boundingExpression)
        val greenInstance = new Instance(solver, null, greenPCBounded)
        val count = greenInstance.request("count").asInstanceOf[Apint]
        logger.debug("$$$ "+greenInstance.getFullExpression+" with count ="+count)
        count
      }
    }
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    // For now we only model count boolean expression.
    if(!startingSketch.isInstanceOf[BoolOperatorsCAST])
      return true

    // If the original expression contains no variables, e.g., not (= 1 0)
    val varCount = new CollectMajorVars
    startingSketch.accept(varCount)
    if(varCount.variableCount.size == 0)
      return true

    sol.transformMutant(startingSketch)
    val pc1 = And(startingSketch.asInstanceOf[BoolOperatorsCAST], Not(sol.getTransformedCAST().asInstanceOf[BoolOperatorsCAST]))
    val pc2 = And(Not(startingSketch.asInstanceOf[BoolOperatorsCAST]), sol.getTransformedCAST().asInstanceOf[BoolOperatorsCAST])
    val pcAll = Or(pc1, pc2)
    logger.debug("Original formula: "+pcAll)
    val nnf = Conversions.toNNF(pcAll).asInstanceOf[BoolOperatorsCAST]
    logger.debug("NNF formula="+nnf)
    val dnf = Conversions.toDNF(nnf)
    logger.debug("DNF formula="+dnf)
    val result = modelCount(configure(), dnf)
    logger.debug("Final count="+result)
    // If final count is zero, it means the two are equivalent. However, check the TODO bellow.
    //TODO: this is only correct if no overflow. E.g., a * b >=0 and (a>=0 && b>=0) are not equivalent
    if(result.intValue() > 0){
      //setWeight(Math.pow(10,10))
      setWeight(0.1)
      sol.addFeatureScores(featureUniqueCode(), (1.0f/result.intValue()) * weight)
      true
    }else{
      false
    }
  }

  def satisfyFeatureOld2(sol: Solution): Boolean = {
    // For now we only model count boolean expression.
    if(!startingSketch.isInstanceOf[BoolOperatorsCAST])
      return true

    val solver = configure()
    stSketch2Green()
    sol.transformMutant(startingSketch)
    val converter = new ToGreenPCConverter(intL, intU, stSketchBoundedVars)
    val notSolutionExp = Conversions.toNNF(Not(sol.getTransformedCAST().asInstanceOf[BoolOperatorsCAST]))
    val notSolutionExpGreen = notSolutionExp.accept(new ToGreenPCConverter(intL, intU, stSketchBoundedVars)).asInstanceOf[GreenPCReturnType].exp
    val solutionExpGreen = sol.getTransformedCAST().accept(converter).asInstanceOf[GreenPCReturnType].exp
    val solutionBoundingExp = converter.boundingExpression
    if(solutionBoundingExp == null && stSketchBoundedVars == null){
      throw new RuntimeException("Variables should be bounded!")
    }

    val pc1 = new Operation(Operation.Operator.AND, stSketchGreen, notSolutionExpGreen)
    val pc2 = new Operation(Operation.Operator.AND, solutionExpGreen, stSketchNotGreen)
    val pc3 = new Operation(Operation.Operator.AND, pc1, pc2)

    val orPCs = new Operation(Operation.Operator.OR, pc2, pc1)
    val bounding = {
      if(solutionBoundingExp != null && stSketchBoundedVars != null)
        new Operation(Operation.Operator.AND, stSketchBoundingExp, solutionBoundingExp)
      else if(stSketchBoundedVars != null)
        stSketchBoundingExp
      else
        solutionBoundingExp
    }
    /**
      * to count (pc1 or pc2) and bounds
      * we count pc1 and bounds -> result1
      * then count pc2 and bounds -> result2
      * then count pc1 and pc2 and bounds -> result3
      * final count is result1 + result2 - result3
      */
    val greenPC1 = new Instance(solver, null, new Operation(Operation.Operator.AND, pc1, bounding))
    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$" + greenPC1.getExpression)
    val result1 = greenPC1.request("count").asInstanceOf[Apint]
    val greenPC2 = new Instance(solver, null, new Operation(Operation.Operator.AND, pc2, bounding))
    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$" + greenPC2.getExpression)
    val result2 = greenPC2.request("count").asInstanceOf[Apint]
    val greenPC3 = new Instance(solver, null, new Operation(Operation.Operator.AND, pc3, bounding))
    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$" + greenPC3.getExpression)
    val result3 = greenPC1.request("count").asInstanceOf[Apint]
    val result = result1.add(result2).subtract(result3)
    System.out.println("RESULT " + result)

    // If final count is zero, it means the two are equivalent. However, check the TODO bellow.
    //TODO: this is only correct if no overflow. E.g., a * b >=0 and (a>=0 && b>=0) are not equivalent
    if(result.intValue() > 0){
      //setWeight(Math.pow(10,10))
      //setWeight(0.0)
      sol.addFeatureScores(featureUniqueCode(), (1.0f/result.intValue()) * weight)
      true
    }else{
      false
    }

  }

   def satisfyFeatureOld(sol: Solution): Boolean = {
    // For now we only model count boolean expression.
    if(!startingSketch.isInstanceOf[BoolOperatorsCAST])
      return true
    //test04()


    val solver = configure()
    stSketch2Green()
    sol.transformMutant(startingSketch)
    val converter = new ToGreenPCConverter(intL, intU, stSketchBoundedVars)
    val notSolutionExp = Conversions.toNNF(Not(sol.getTransformedCAST().asInstanceOf[BoolOperatorsCAST]))
    val notSolutionExpGreen = notSolutionExp.accept(new ToGreenPCConverter(intL, intU, stSketchBoundedVars)).asInstanceOf[GreenPCReturnType].exp
    val solutionExpGreen = sol.getTransformedCAST().accept(converter).asInstanceOf[GreenPCReturnType].exp
    val solutionBoundingExp = converter.boundingExpression
    if(solutionBoundingExp == null && stSketchBoundedVars == null){
      throw new RuntimeException("Variables should be bounded!")
    }

    val pc1 = new Operation(Operation.Operator.AND, stSketchGreen, notSolutionExpGreen)
    val pc2 = new Operation(Operation.Operator.AND, solutionExpGreen, stSketchNotGreen)
    val orPCs = new Operation(Operation.Operator.OR, pc2, pc1)
    val bounding = {
      if(solutionBoundingExp != null && stSketchBoundedVars != null)
        new Operation(Operation.Operator.AND, stSketchBoundingExp, solutionBoundingExp)
      else if(stSketchBoundedVars != null)
        stSketchBoundingExp
      else
        solutionBoundingExp
    }
    val finalCond = new Operation(Operation.Operator.AND, orPCs, bounding)
    val greenPC = new Instance(solver, null, finalCond)
    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$" + greenPC.getExpression)
    val result = greenPC.request("count").asInstanceOf[Apint]
    System.out.println("RESULT " + result)
    if(result.intValue() > 0){
      //setWeight(Math.pow(10,10))
      sol.addFeatureScores(featureUniqueCode(), (result.intValue()) * weight)
      true
    }else{
      false
    }

  }

  override def featureUniqueCode(): Int = 6
}
case class VarLocationChange() extends RankingFeature{

  lazy val stSketchVarLoc = {
    val varLocVisitor = new VarLocVisitor
    startingSketch.accept(varLocVisitor)
    varLocVisitor.varLocs
  }

  def this(thresh: Double){
    this()
    threshold = thresh
  }

  class VarLocVisitor extends CASTVisitorBase{
    val varLocs = new ArrayBuffer[String]()

    override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

    override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
      varLocs.append(boolVar.name)
      BoolType(null, true)
    }

    override def visitIntVar(intVar: IntVar): VisitorReturnType = {
      varLocs.append(intVar.name)
      IntType(null, 1)
    }

    override def visitBoolConst(boolConst: BoolConst): VisitorReturnType = {
      varLocs.append(boolConst.value)
      BoolType(null, true)
    }

    override def visitIntConst(intConst: IntConst): VisitorReturnType = {
      varLocs.append(intConst.value)
      IntType(null, 1)
    }
  }

  private def editDistance(map1: ArrayBuffer[String], map2: ArrayBuffer[String]): Double = {
    if(map1.size == 0 && map2.size == 0)
      return 0.0

    while (map1.size < map2.size){
      map1.append("")
    }
    while (map2.size < map1.size){
      map2.append("")
    }
    val distance = map1.zip(map2).foldLeft(0){(res, t) =>{
      if(t._1.compareTo(t._2) == 0)
        res
      else res+1
    }}
    // note that this is less than or equal to 1
    distance.toDouble/map1.size.toDouble
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    // If the original expression contains no variables, e.g., not (= 1 0)
    val varCount = new CollectMajorVars
    startingSketch.accept(varCount)
    if(varCount.variableCount.size == 0)
      return true

    // In case starting sketch size is too small, we dont care, just return true, new updates changed <= <
    if(startingSketch.size() < 3)
      return true

    sol.transformMutant(startingSketch)
    val varLocVisitor = new VarLocVisitor
    sol.getTransformedCAST().accept(varLocVisitor)
    val solVarLoc = varLocVisitor.varLocs
    val editDist = editDistance(stSketchVarLoc, solVarLoc)
    sol.addFeatureScores(featureUniqueCode(), (1.0f - editDist) * weight)
    if(editDist <= threshold)
      true
    else false
  }

  override def featureUniqueCode(): Int = 7
}
case class VarLocationChangeOLD() extends RankingFeature{

  lazy val stSketchVarLoc = {
    val varLocVisitor = new VarLocVisitor
    startingSketch.accept(varLocVisitor)
    varLocVisitor.varLocs
  }

  def this(thresh: Double){
    this()
    threshold = thresh
  }

  class VarLocVisitor extends CASTVisitorBase{
    val varLocs = new ArrayBuffer[String]()

    override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

    override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
      varLocs.append(boolVar.name)
      BoolType(null, true)
    }

    override def visitIntVar(intVar: IntVar): VisitorReturnType = {
      varLocs.append(intVar.name)
      IntType(null, 1)
    }
  }

  private def editDistance(map1: ArrayBuffer[String], map2: ArrayBuffer[String]): Double = {
    if(map1.size == 0 && map2.size == 0)
      return 0.0

    while (map1.size < map2.size){
      map1.append("")
    }
    while (map2.size < map1.size){
      map2.append("")
    }
    val distance = map1.zip(map2).foldLeft(0){(res, t) =>{
      if(t._1.compareTo(t._2) == 0)
        res
      else res+1
    }}
    // note that this is less than or equal to 1
    distance.toDouble/map1.size.toDouble
  }

  override def satisfyFeature(sol: Solution): Boolean = {
    // If the original expression contains no variables, e.g., not (= 1 0)
    val varCount = new CollectMajorVars
    startingSketch.accept(varCount)
    if(varCount.variableCount.size == 0)
      return true

    // In case starting sketch size is too small, we dont care, just return true, new updates changed <= <
    if(startingSketch.size() < 3)
      return true

    sol.transformMutant(startingSketch)
    val varLocVisitor = new VarLocVisitor
    sol.getTransformedCAST().accept(varLocVisitor)
    val solVarLoc = varLocVisitor.varLocs
    val editDist = editDistance(stSketchVarLoc, solVarLoc)
    sol.addFeatureScores(featureUniqueCode(), (1.0f - editDist) * weight)
    if(editDist <= threshold)
      true
    else false
  }

  override def featureUniqueCode(): Int = 7
}
case class AngelicValuesBalanceCoverage(coverage: Double) extends RankingFeature{
  override def satisfyFeature(sol: Solution): Boolean = {
    if(coverage >= 0.5) {
      sol.addFeatureScores(featureUniqueCode(), coverage * weight)
      return true
    }
    else false
  }

  override def featureUniqueCode(): Int = 8
}