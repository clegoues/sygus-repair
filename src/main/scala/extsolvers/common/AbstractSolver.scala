package extsolvers.common

import extsolvers.common.FromSearcher.FromSearcher
import engines.coreast._
import synthlib2parser._
import synthlib2parser.scopemanager.{SymbolTable, SymbolTableCommon, SymbolTableScope}
import org.apache.log4j.Logger

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, HashMap}

/**
  * Created by dxble on 7/12/16.
  */
abstract class AbstractSolver(theSymbolTable: SymbolTable) extends ASTVisitorBase("AbstractSolver"){
  var indivConstraint: CAST = null
  val allConstraints: ArrayBuffer[CAST] = new ArrayBuffer[CAST]()
  val originalExprs: HashMap[FunctionSignature,CAST] = new HashMap[FunctionSignature,CAST]()
  val synthFuns : HashMap[FunctionSignature,SynthFunCmd] = new HashMap[FunctionSignature,SynthFunCmd]()

  //Map from function signature to its body which satisfies the contraints
  val solutions: HashMap[FunctionSignature, mutable.HashSet[Solution]] = new HashMap[FunctionSignature, mutable.HashSet[Solution]]

  //each constraint (index) -> substitutions. Note that the index of substitutions corresponds to index in allConstraints.
  //E.g., substitution(0) is the substitutions in allConstraints(0)
  //val substitutions: mutable.HashMap[Int, DefaultSubsCollector] = new mutable.HashMap[Int,DefaultSubsCollector]()

  //
  val constraintsOfFuns: mutable.HashMap [FunctionSignature, ArrayBuffer[CAST]] = new mutable.HashMap[FunctionSignature, ArrayBuffer[CAST]]()
  val funsAngelicValues: mutable.HashMap [FunctionSignature, mutable.HashSet[InternalTerm]] = new mutable.HashMap[FunctionSignature, mutable.HashSet[InternalTerm]]()
  val logger = Logger.getLogger(getClass)

  private def argList2InternalTermList(argList: ArgList): ArrayBuffer[InternalTerm] ={
      argList.args.foldLeft(new ArrayBuffer[InternalTerm]){
      (res, asPair) =>{
        asPair.sort match {
          case BoolSortExp() => {
            res.append(new BoolVar(asPair.name))
          }
          case IntSortExp() => {
            res.append(new IntVar(asPair.name))
          }
        }
        res
      }
    }
  }

  override def visitOriginalExpCmd(cmd: OriginalExpCmd) ={
    val args = argList2InternalTermList(cmd.args)
    val funDesign = new FunctionSignature(cmd.name, args)
    cmd.constraint match {
      case e:FunTerm => {
        val origExprInternalAST = visit(e, scope = cmd.scope)
        originalExprs += (funDesign -> origExprInternalAST)
        indivConstraint = null// reset this variable to null
      }
      case e:SymbolTerm =>{
        val eSort = cmd.scope.lookup(e.symbolStr).getSort()
        eSort match {
          case BoolSortExp() => originalExprs += (funDesign -> new BoolVar(e.symbolStr, e.pos))
          case IntSortExp() => originalExprs += (funDesign ->new IntVar(e.symbolStr, e.pos))
        }
      }
      case e:LiteralTerm =>{
        e.litTerm.sort match {
          case BoolSortExp() => originalExprs += (funDesign ->new BoolConst(e.litTerm.litStr, e.pos))
          case IntSortExp() => originalExprs += (funDesign ->new IntConst(e.litTerm.litStr, e.pos))
        }
      }
    }
  }

  override def visitSynthFunCmd(cmd: SynthFunCmd): Unit ={
    //Bachle: Semantic check if the function defined in the grammar is supported by theory
    try {
      cmd.grammarRules.foreach(r => {
        r.expansions.foreach(exp => {
          if (exp.isInstanceOf[FunGTerm]) {
            val expFunGTerm = exp.asInstanceOf[FunGTerm]
            val definedTheoryFun = theSymbolTable.lookupFun(expFunGTerm.funName,
              expFunGTerm.args.foldLeft(List[SortExp]()) { (res, expi) => {
                if (expi.isInstanceOf[SymbolGTerm]) {
                  val symGterm = expi.asInstanceOf[SymbolGTerm]
                  //This is in case of recursive type definition
                  if (symGterm.symbol == r.symbol)
                    res :+ r.sort
                  else {
                    //Bachle: resolve in current scope first, then go to global scope
                    //Current scope
                    val resolvedSort = cmd.scope.lookup(SymbolTableCommon.mangleSortName(symGterm.symbol)).getSort()
                    if (resolvedSort != null)
                      res :+ resolvedSort
                    else//Global scope
                      res :+ symGterm.getTermSort(theSymbolTable)
                  }
                } else {//FunGTerm or LiteralGTerm
                  res :+ expi.getTermSort(theSymbolTable)
                }
              }
              })
            if (definedTheoryFun == null)
              throw new RuntimeException("Error: not defined theory fun: " + expFunGTerm + " at Line: " + expFunGTerm.pos.line + " Column: " + expFunGTerm.pos.column)
          }
        })
      })
    }catch {
      case e: Exception => logger.info("*****Semantics Check Failed: "+ e.getMessage + "\n"+ e.getStackTraceString)
    }finally {
      val args = argList2InternalTermList(cmd.argList)
      synthFuns += (new FunctionSignature(cmd.name, args) -> cmd)
    }
  }

  private def isBoolOperator(op: String): Boolean = {
    //op == "=" || op == ">" || op == ">=" || op == "<" || op == "<=" are int operators
    if (op == "and" || op == "or" || op == "not" || op == "=>") return true
    else return false
  }

  /**
    * This is a recursive function used for collecting all constraint cmds, converting these contraints
    * to internal core AST
    *
    * @param e
    * @param scope
    * @return core ast expression that represent the constraint
    */
  def visit(e: FunTerm, scope: SymbolTableScope = null): CAST = {
    val grammarDesign: ArrayBuffer[CAST] = new ArrayBuffer [CAST]()
    var i: Int = 0
    while (i < e.args.size) {
      val expr: Term = e.args(i)
      //Recursively call visit if expr is a FunTerm
      if (expr.isInstanceOf[FunTerm]) grammarDesign.append(visit(expr.asInstanceOf[FunTerm], scope))
      else if (expr.isInstanceOf[SymbolTerm]) {
        var exprSort: SortExp = null
        if(scope == null)
          exprSort = expr.getTermSort(theSymbolTable)
        else {
          try {
            exprSort = scope.lookup(expr.asInstanceOf[SymbolTerm].symbolStr).getSort()
          }catch {
            case exc:Exception =>{
              println("debug:"+expr)
              exprSort = expr.getTermSort(theSymbolTable)
            }
          }
        }
        if (exprSort.isInstanceOf[BoolSortExp])grammarDesign.append(new BoolVar(expr.asInstanceOf[SymbolTerm].symbolStr, expr.pos))
        else grammarDesign.append(new IntVar(expr.asInstanceOf[SymbolTerm].symbolStr, expr.pos))
      }
      else if (expr.isInstanceOf[LiteralTerm]) {
        val exprSort: SortExp = expr.asInstanceOf[LiteralTerm].litTerm.sort
        //val exprSort = expr.getTermSort(theSymbolTable)
        if (exprSort.isInstanceOf[BoolSortExp]) grammarDesign.append(new BoolConst(expr.asInstanceOf[LiteralTerm].litTerm.litStr, expr.pos))
        else {
          if(!isBoolOperator(e.funName))
            grammarDesign.append(new IntConst(expr.asInstanceOf[LiteralTerm].litTerm.litStr, expr.pos))
          else{
            val strIntValue = expr.asInstanceOf[LiteralTerm].litTerm.litStr
            val strBoolValue = if(strIntValue == "0") "false" else "true"
            grammarDesign.append(new BoolConst(strBoolValue, expr.pos))
          }
        }
      }
      else println(expr)
      i += 1
    }
    if (e.funName == "and") {
      indivConstraint = new And(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == "or") {
      indivConstraint = new Or(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == "not") {
      indivConstraint = new Not(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else if (e.funName == ">") {
      indivConstraint = new Greater(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "<") {
      indivConstraint = new LessThan(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == ">=") {
      indivConstraint = new GTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "<=") {
      indivConstraint = new LTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "=") {
      indivConstraint = new Equal(grammarDesign(0), grammarDesign(1), e.pos)
    }
    else if (e.funName == "+") {
      indivConstraint = new Plus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "-") {
      indivConstraint = new Minus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "/") {
      indivConstraint = new Div(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "*") {
      indivConstraint = new Mult(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST], e.pos)
    }
    else if (e.funName == "=>"){
      indivConstraint = new Implies(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST], e.pos)
    }
    else{// function name defined by user
      //println(e)
      indivConstraint = new FunctionSignature(e.funName, grammarDesign.foldLeft(new ArrayBuffer[InternalTerm]){ (res, arg) => {
        res.append(arg.asInstanceOf[InternalTerm])
        res
      }}, e.pos)
    }
    return indivConstraint
  }

  override def visitConstraintCmd(cmd: ConstraintCmd) = {
    if(!cmd.term.isInstanceOf[FunTerm]){
      throw new RuntimeException("AbstractSolver: constraints should be FunTerm.")
    }else{
      val aConstraint = cmd.term.asInstanceOf[FunTerm]
      if(!isBoolOperator(aConstraint.funName))
        if(!aConstraint.getTermSort(theSymbolTable).isInstanceOf[BoolSortExp]){
          throw new RuntimeException("AbstractSolver: constraints should be FunTerm with Boolean type")
        }

      val constraintInternalAST = visit(aConstraint)
      allConstraints.append(constraintInternalAST)
      indivConstraint = null
    }

  }

  /**
    * Once we reached this function, all contraints and original expressions have already been collected
    * Note that check-synth is the last command in sygus input. This acts as an activation function.
    * Depending on the search behavior of each synthesis technique/solver, this function will be implemented
    * differently.
    *
    * @param cmd
    */

  override def visitCheckSynthCmd(cmd: CheckSynthCmd): Unit ={
    //Main function here
    val sols = synthFuns.foldLeft(mutable.HashSet[Solution]()){
      (res, fun) =>{
        var passedAll = true
        allConstraints.foreach{ constr => {
          val funSignature = fun._1
          val conStrCollector = new ConstraintsOfFuncCollector(funSignature)
          constr.accept(conStrCollector)

          // Collect all angelic (expected) values for each function
          val angelicVs = this.funsAngelicValues.get(funSignature).getOrElse(new mutable.HashSet[InternalTerm]())
          conStrCollector.angelicValues.foreach(a => angelicVs.add(a))
          this.funsAngelicValues.update(funSignature, angelicVs)

          // Collect constraints of the function. Collected constraints are of the form: (=> X Y)
          // Since we can't be sure on Or constraint, we only collect constraints in And form or alone
          if(!constr.isInstanceOf[Or]) {

            if(conStrCollector.constraintsOfFunc.size > 0) {
              //substitutions += (index -> subsCollector)
              this.constraintsOfFuns.get(funSignature) match {
                case None => {
                  this.constraintsOfFuns += (funSignature -> conStrCollector.constraintsOfFunc)
                }
                case Some(c) => {
                  c.appendAll(conStrCollector.constraintsOfFunc)
                  this.constraintsOfFuns.update(funSignature, c)
                }
              }

              val anyFalse = conStrCollector.constraintsOfFunc.find(cstr => {
                //Each cstr is of the form: (=> X Y)
                //Collect all substitutions of variables in each constraint cstr
                val subsCollector = new DefaultSubsCollector(this)
                cstr.accept(subsCollector)

                //Concrete evaluation of original expressions on its constraints.
                val originalExprWrapper = originalExprs.foldLeft(new mutable.HashMap[FunctionSignature, Solution]()) {
                  (res, funExp) => {
                    res += funExp._1 -> new OriginalSolution(funExp._1, funExp._2, -1, funExp._2)
                  }
                }
                val concreteEvaluation = new ConcreteEvaluation(subsCollector, originalExprWrapper)
                cstr.accept(concreteEvaluation).asInstanceOf[BoolType].value == false
              })
              //If original expression already passes its constraints, that original expression constitute a solution
              anyFalse match {
                case None => {}
                case Some(e) => {
                  passedAll = false
                }
              }
            }else{
              // We do not find any constraint in a AND of this function. So we set it passedAll to false
              // to indicate that we have not evaluated it, and to indicate that we need to do synthesis of this func
              passedAll = false
            }
          }else{// Or constraint
            passedAll = false
          }
        }}
        if(passedAll) {
          val score = this.constraintsOfFuns.get(fun._1).getOrElse(new ArrayBuffer[CAST]()).size
          val origExpr = originalExprs.get(fun._1).getOrElse(null)
          res.add(new OriginalSolution(fun._1,origExpr,score, origExpr))
        }
        res
      }
    }

    sols.groupBy(_.getFuncSignature()).foreach(sol => {
      this.solutions += sol
    })
  }

  protected def solutionIsOriginal(functionDesign: FunctionSignature): Boolean ={
    val sols = this.solutions.get(functionDesign).getOrElse(null)
    if(sols == null)
      return false
    else {
      val isOrig = sols.find(_.isInstanceOf[OriginalSolution]).getOrElse(null)
      if(isOrig == null)
        return false
      else return true
    }
  }

  protected def partialEvalCandidateSol(solution: Solution): Solution ={
    val solMap = new mutable.HashMap[FunctionSignature, Solution]()
    solMap += (solution.getFuncSignature() -> solution)
    val andConstraints = this.constraintsOfFuns.get(solution.getFuncSignature()).getOrElse(null)
    val score = andConstraints.foldLeft(0)((res, cstr) => {
      // Each cstr is of the form: (=> X Y)
      // Collect all substitutions of variables in each constraint cstr
      val subsCollector = new DefaultSubsCollector(this)
      cstr.accept(subsCollector)

      //Concrete evaluation of original expressions on its constraints.
      val concreteEvaluation = new ConcreteEvaluation(subsCollector, solMap)
      if(cstr.accept(concreteEvaluation).asInstanceOf[BoolType].value){
        res + 1
      }else res
    })
    if(score == andConstraints.size)
      return new MutantSolution(solution.getFuncSignature(), solution.getSolution(), score, solution.getOrigExp(), solution.getFromSearcher())
    else return null
  }

  protected def crossPSolutions(sol: mutable.HashMap[FunctionSignature, mutable.HashSet[Solution]]): List[List[Solution]] ={
    def fun(xs: List[Solution], zss: List[List[Solution]]): List[List[Solution]] = {
      for {
        x <- xs
        zs <- zss
      } yield {
        x :: zs
      }
    }
    val inputList = sol.par.foldLeft(List[List[Solution]]()){
      (res, sol) => {
        res :+ sol._2.toList
      }
    }
    if(sol.size == 1)
      return sol.head._2.par.foldLeft(List(List[Solution]()))((res, s) => {
        res :+ List[Solution](s)
      })

    val crossProd: List[List[Solution]] = inputList.par.foldRight(List(List[Solution]()))(fun _)
    return crossProd
  }

  protected def crossedSol2Map(crossedSolutions: List[List[Solution]]): List[mutable.HashMap[FunctionSignature, Solution]] ={
    crossedSolutions.par.foldLeft(List[mutable.HashMap[FunctionSignature, Solution]]()){
      (res, solP) => {
        val x = solP.foldLeft(new mutable.HashMap[FunctionSignature, Solution])((res1, s) => {
          res1 += s.getFuncSignature() -> s.myClone().asInstanceOf[Solution]
        })
        res :+ x
      }
    }
  }

  /*def constraintsAndOrigExp2InternalAST(): (ArrayBuffer[GrammarDesign], scala.collection.mutable.HashMap[FunctionDesign,GrammarDesign]) = {
    if(allConstraints == null && originalExprs == null) {
      allConstraints = new ArrayBuffer[GrammarDesign]()
      originalExprs = new scala.collection.mutable.HashMap[FunctionDesign,GrammarDesign]()
      prog.accept(this)
    }
    return (allConstraints, originalExprs)
  }*/
}
