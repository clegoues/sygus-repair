package extsolvers.common

import engines.coreast._

/**
  * Created by dxble on 7/16/16.
  */
class ASTTransformer(toRep: CAST, matched: (CAST, CAST) => Boolean) extends CASTVisitorBase{
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    if(matched(functionDesign, toRep)){
      return toRep
    }else{
      //TODO: to consider visit args, funName later
      return functionDesign
    }
  }

  override def visitGreater(greater: Greater): VisitorReturnType = {
    if(matched(greater,toRep)){
      return toRep
    }else{
      val left = greater.opr1.accept(this)
      val right = greater.opr2.accept(this)
      return new Greater(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], greater.position)
    }
  }

  override def visitLessThan(lessThan: LessThan): VisitorReturnType ={
    if(matched(lessThan,toRep)){
      return toRep
    }else{
      val left = lessThan.opr1.accept(this)
      val right = lessThan.opr2.accept(this)
      return new LessThan(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], lessThan.position)
    }
  }

  override def visitLTE(lTE: LTE): VisitorReturnType ={
    if(matched(lTE,toRep)){
      return toRep
    }else{
      val left = lTE.opr1.accept(this)
      val right = lTE.opr2.accept(this)
      return new LTE(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], lTE.position)
    }
  }

  override def visitGTE(gTE: GTE): VisitorReturnType ={
    if(matched(gTE,toRep)){
      return toRep
    }else{
      val left = gTE.opr1.accept(this)
      val right = gTE.opr2.accept(this)
      return new GTE(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], gTE.position)
    }
  }

  override def visitEqual(equal: Equal): VisitorReturnType={
    if(matched(equal,toRep)){
      return toRep
    }else{
      val left = equal.opr1.accept(this)
      val right = equal.opr2.accept(this)
      return new Equal (left.asInstanceOf[CAST], right.asInstanceOf[CAST], equal.position)
    }
  }

  override def visitAnd(and: And): VisitorReturnType={
    if(matched(and,toRep)){
      return toRep
    }else{
      val left = and.opr1.accept(this)
      val right = and.opr2.accept(this)
      return new And(left.asInstanceOf[BoolOperatorsCAST], right.asInstanceOf[BoolOperatorsCAST], and.position)
    }
  }

  override def visitOr(or: Or): VisitorReturnType={
    if(matched(or,toRep)){
      return toRep
    }else{
      val left = or.opr1.accept(this)
      val right = or.opr2.accept(this)
      return new Or(left.asInstanceOf[BoolOperatorsCAST], right.asInstanceOf[BoolOperatorsCAST], or.position)
    }
  }

  override def visitNot(not: Not): VisitorReturnType={
    if(matched(not,toRep)){
      return toRep
    }else{
      val oprand = not.opr.accept(this)
      return new Not(oprand.asInstanceOf[BoolOperatorsCAST], not.position)
    }
  }

  override def visitImplies(implies: Implies): VisitorReturnType={
    if(matched(implies,toRep)){
      return toRep
    }else{
      val left = implies.opr1.accept(this)
      val right = implies.opr2.accept(this)
      return new Implies(left.asInstanceOf[BoolOperatorsCAST], right.asInstanceOf[BoolOperatorsCAST], implies.position)
    }
  }

  override def visitPlus(plus: Plus): VisitorReturnType={
    if(matched(plus,toRep)){
      return toRep
    }else{
      val left = plus.opr1.accept(this)
      val right = plus.opr2.accept(this)
      return new Plus(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], plus.position)
    }
  }

  override def visitMinus(minus: Minus): VisitorReturnType={
    if(matched(minus,toRep)){
      return toRep
    }else{
      val left = minus.opr1.accept(this)
      val right = minus.opr2.accept(this)
      return new Minus(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], minus.position)
    }
  }

  override def visitDiv(div: Div): VisitorReturnType={
    if(matched(div,toRep)){
      return toRep
    }else{
      val left = div.opr1.accept(this)
      val right = div.opr2.accept(this)
      return new Div(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], div.position)
    }
  }

  override def visitMult(mult: Mult): VisitorReturnType={
    if(matched(mult,toRep)){
      return toRep
    }else{
      val left = mult.opr1.accept(this)
      val right = mult.opr2.accept(this)
      return new Mult(left.asInstanceOf[ArithOperatorsCAST], right.asInstanceOf[ArithOperatorsCAST], mult.position)
    }
  }


  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    if(matched(boolVar,toRep)){
      return toRep
    }else{
      return boolVar
    }
  }

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType = {
    if(matched(boolConst,toRep)){
      return toRep
    }else{
      return boolConst
    }
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    if(matched(intVar,toRep)){
      return toRep
    }else{
      return intVar
    }
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType = {
    if(matched(intConst,toRep)){
      return toRep
    }else{
      return intConst
    }
  }
}
