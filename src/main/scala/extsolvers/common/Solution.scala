package extsolvers.common

import extsolvers.common.FromSearcher.FromSearcher
import engines.coreast._
import scala.collection.mutable

/**
  * Created by dxble on 7/14/16.
  */
object FromSearcher extends Enumeration{
  type FromSearcher = Value
  val FROM_MUTATION_SEARCHER,
  FROM_ORIGINAL_EXPR,
  FROM_ENUMERATION_SEARCHER = Value
}

abstract class Solution extends CAST{
  var transformed = false
  var transformedCAST: CAST = null
  val featureScores: mutable.HashMap[Int, Double] = new mutable.HashMap[Int, Double]()
  protected var sumFScore = 0.0

  def getFuncSignature(): FunctionSignature
  def getSolution(): CAST
  def getScore(): Double
  def setTransformed() = transformed = true

  def setScore(score: Double): Unit

  def getFromSearcher(): FromSearcher

  def sumFeatureScores(): Double = sumFScore + featureScores.foldLeft(0.0)(_+_._2)

  def setFScore(s: Double) = sumFScore = s

  def addFeatureScores(featureCode: Int, featureScore: Double) = {
    if(featureScores.contains(featureCode))
      throw new RuntimeException("Feature already exist in this mutant, cannot add: "+featureCode+" score = "+featureScore)

    featureScores += featureCode -> featureScore
  }

  def transformMutant(sketch: CAST): CAST ={
    if(transformed)
      return transformedCAST

    def matchedPos(e1: CAST, e2:CAST): Boolean ={
      return e1.position.line == e2.position.line && e1.position.column == e2.position.column
    }
    val rewriter = new ASTTransformer(getSolution(), matchedPos)
    transformedCAST = sketch.accept(rewriter).asInstanceOf[CAST]
    transformed = true
    transformedCAST
  }

  def getTransformedCAST() = {
    if(!transformed)
      throw new RuntimeException("Have not yet transformed the CAST from starting sketch and CAST solution!")

    transformedCAST
  }

  /*override def toString(): String ={
    getFuncSignature() + getSolution().toString + getScore() + getFromSearcher()
  }*/

  def createHigherOrder(that: Solution): HigherOrderSolution = {
    new HigherOrderSolution(getFuncSignature(), List[CAST](this.getSolution(), that.getSolution()), this.sumFeatureScores()+that.sumFeatureScores(), List[CAST](this.getOrigExp(), that.getOrigExp()), null)
  }

  def getOrigExp(): CAST
  override def equals(that: Any): Boolean ={
    if(!that.isInstanceOf[Solution])
      return false
    else{
      val sameSol = this.getSolution().equals(that.asInstanceOf[Solution].getSolution())
      if(this.getOrigExp() != null && that.asInstanceOf[Solution].getOrigExp() != null)
        return sameSol && this.getOrigExp().equals(that.asInstanceOf[Solution].getOrigExp())
      else return sameSol
    }
  }

  override def hashCode(): Int = {
    getSolution().hashCode() //+ getOrigExp().hashCode()
  }

  override def smtString(): String = getOrigExp().smtString()
}
case class OriginalSolution(funtionSig: FunctionSignature, solution: CAST, var score: Double, origExp: CAST, comeFrom: FromSearcher = FromSearcher.FROM_ORIGINAL_EXPR) extends Solution{
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = solution.accept(visitorBase)

  override def size(): Int = solution.size()

  override def getFuncSignature(): FunctionSignature = funtionSig

  override def getSolution(): CAST = solution

  override def topLevelString(): String = solution.topLevelString()

  override def getScore(): Double = score

  override def setScore(score: Double): Unit = this.score = score

  override def getOrigExp(): CAST = origExp

  override def getFromSearcher(): FromSearcher = comeFrom

  override def myClone(): CAST = {
    val cloneOrig = OriginalSolution(funtionSig.myClone().asInstanceOf[FunctionSignature], solution.myClone(), score, origExp.myClone(),comeFrom)
    cloneOrig.transformed = this.transformed
    cloneOrig.transformedCAST = if(this.transformedCAST != null) this.transformedCAST.myClone() else null
    this.featureScores.foreach(f => cloneOrig.featureScores += f)
    cloneOrig
  }

  /*override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[OriginalSolution])
      return false
    else{
      val castedThat = that.asInstanceOf[OriginalSolution]
      if(castedThat.getSolution().toString.compareTo(this.getSolution().toString) != 0 ||
        castedThat.getOrigExp().toString.compareTo(this.getOrigExp().toString) != 0)
        return false
      else return true
    }
  }*/
}
case class MutantSolution(functionSig: FunctionSignature, solution: CAST, var score: Double, origExp: CAST, comeFrom: FromSearcher) extends Solution{
  @throws(classOf[DivisionByZeroException])
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = solution.accept(visitorBase)

  override def size(): Int = solution.size()

  override def getFuncSignature(): FunctionSignature = functionSig

  override def getSolution(): CAST = solution

  override def topLevelString(): String = solution.topLevelString()

  override def getScore(): Double = score

  override def setScore(score: Double): Unit = this.score = score

  override def getOrigExp(): CAST = origExp

  override def getFromSearcher(): FromSearcher = comeFrom

  override def myClone(): CAST = {
    val cloneMut = MutantSolution(functionSig.myClone().asInstanceOf[FunctionSignature], solution.myClone(), score, origExp.myClone(),comeFrom)
    cloneMut.transformed = this.transformed
    cloneMut.transformedCAST = if(this.transformedCAST != null) this.transformedCAST.myClone() else null
    this.featureScores.foreach(f => cloneMut.featureScores += f)
    cloneMut
  }

  /*override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[MutantSolution])
      return false
    else{
      val castedThat = that.asInstanceOf[MutantSolution]
      if(castedThat.getSolution().toString.compareTo(this.getSolution().toString) != 0 ||
        castedThat.getOrigExp().toString.compareTo(this.getOrigExp().toString) != 0)
        return false
      else return true
    }
  }*/
}
//TODO: this is not used yet. But might be useful later
case class HigherOrderSolution(funSig: FunctionSignature, solutionList: List[CAST], var score: Double, origExps: List[CAST], comeFrom: FromSearcher) extends Solution{
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    solutionList.foreach(sol => sol.accept(visitorBase))
    EmptyType()
  }

  override def size(): Int = solutionList.size

  override def getFuncSignature(): FunctionSignature = funSig

  override def getSolution(): CAST = null

  override def topLevelString(): String = "Calling top level String from solution list"

  override def getScore(): Double = score

  override def setScore(score: Double): Unit = this.score = score

  override def getOrigExp(): CAST = null

  override def getFromSearcher(): FromSearcher = comeFrom

  override def myClone(): CAST = {
    val clonedSolList = solutionList.foldLeft(List[CAST]()){(res, sol) => res :+ sol.myClone()}
    val clonedOrigs = origExps.foldLeft(List[CAST]()){(res, orig) => res :+ orig.myClone()}
    new HigherOrderSolution(funSig, clonedSolList, score, clonedOrigs, comeFrom)
  }

  override def transformMutant(sketch: CAST): CAST ={
    def matchedPos(e1: CAST, e2:CAST): Boolean ={
      return e1.position.line == e2.position.line && e1.position.column == e2.position.column
    }
    var rewrittenAST = sketch
    solutionList.foreach(s => {
      val rewriter = new ASTTransformer(s, matchedPos)
      rewrittenAST = rewrittenAST.accept(rewriter).asInstanceOf[CAST]
    })
    transformed = true
    transformedCAST = rewrittenAST
    transformedCAST
  }

  def toMutantSolution(startingSketch: CAST): MutantSolution = {
    transformMutant(startingSketch)
    val res = new MutantSolution(getFuncSignature(), getTransformedCAST(), -1.0, null, null)
    // Sum feature score now be the score of this higher order mutant, which is the sum of feature scores of
    // its constituents. See createHigherOrder in abstract class Solution.
    res.setFScore(score)
    res
  }
}
