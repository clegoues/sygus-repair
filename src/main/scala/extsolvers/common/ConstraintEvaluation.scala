package extsolvers.common

import engines.coreast._
import scala.collection._
/**
  * Created by dxble on 7/16/16.
  */
class ConstraintEvaluation(funcSubs: mutable.HashMap[FunctionSignature, Solution], solver: AbstractSolver) extends ConcreteEvaluation(null, funcSubs){
  // Satisfied angelic values
  val funsAngelicValuesSat: mutable.HashMap [FunctionSignature, mutable.HashSet[InternalTerm]] = new mutable.HashMap[FunctionSignature, mutable.HashSet[InternalTerm]]()

  override def visitImplies(implies: Implies): VisitorReturnType ={
    //Each cstr is of the form: (=> X Y)
    //Collect all substitutions of variables in each constraint cstr
    val subsCollector = new DefaultSubsCollector(solver)
    implies.accept(subsCollector)

    //Concrete evaluation of solution expressions on implies constraints.
    val concreteEvaluation = new ConcreteEvaluation(subsCollector, funcSubs)
    val res = implies.accept(concreteEvaluation)

    if(res.asInstanceOf[BoolType].isTrue()) {// The implication constraint is satisfied
      val angelicValCollector = new CollectAngelicValues
      implies.opr2.accept(angelicValCollector)
      angelicValCollector.funsAngelicValues.foreach(f => {
        val angelicValues = funsAngelicValuesSat.getOrElse(f._1, new mutable.HashSet[InternalTerm]())
        f._2.foreach(a => angelicValues.add(a))
        funsAngelicValuesSat.update(f._1, angelicValues)
      })
    }
    res
  }
}
