package extsolvers.common

import engines.coreast._

import scala.collection.mutable

/**
  * Created by dxble on 7/13/16.
  */

/**
  * Default collector for substitutions.
  * We traverse constraints to get concrete values for variables in the constraints.
  * These concrete values are the substitutions for the variables during concrete evaluation.
  * This collector should be accepted by constraints of the form (=> X Y) only.
  */

class DefaultSubsCollector(solver: AbstractSolver) extends CASTVisitorBase{
  val substitutions = new mutable.HashMap[InternalTerm, VisitorReturnType]()
  val variableNameMap = new mutable.HashMap[InternalTerm, InternalTerm]()

  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    val cmd = solver.synthFuns.get(functionDesign).getOrElse(throw new RuntimeException("Function has not been defined/declared! "+functionDesign))

    /** Build variableNameMap
      * Iterate over synth-fun arguments and arguments of function in the constraint to make sure
      * that the arg names are the same. If not same, create a map from synth-fun arg name to arg name
      * of the function in the constraint
      */
    cmd.argList.args.foldLeft(0)((index,arg) =>{
      val thatArg = functionDesign.args(index)
      thatArg match {
        case BoolVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (BoolVar(arg.name) -> thatArg)
        case IntVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (IntVar(arg.name) -> thatArg)
      }
      index + 1
    })
    EmptyType()//Return whatever type, which is not null
  }

  //Return whatever
  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = EmptyType()

  //Return whatever
  override def visitIntVar(intVar: IntVar): VisitorReturnType = EmptyType()

  //Visit LHS to collect substitutions for variables
  //Visit RHS to collect functions
  override def visitImplies(implies: Implies): VisitorReturnType ={
    implies.opr1.accept(this)
    implies.opr2.accept(this)
  }

  /**
    *
    * @param equal should be in the form of (= variable value), e.g., (= x 10), otherwise this would fail
    * @return
    */
  override def visitEqual(equal: Equal): VisitorReturnType ={
    var constData: VisitorReturnType = null
    if(equal.opr2.isInstanceOf[IntConst])
      constData= visitIntConst(equal.opr2.asInstanceOf[IntConst])
    else if(equal.opr2.isInstanceOf[BoolConst])
      constData= visitBoolConst(equal.opr2.asInstanceOf[BoolConst])
    else throw new RuntimeException("Not expected when collecting substitutions in RHS of: "+equal)
    substitutions += (equal.opr1.asInstanceOf[InternalTerm] -> constData)
    super.visitEqual(equal)
  }

  def findSubstitution(variable: InternalTerm) ={
    // Note: variableNameMap is e.g., int1 -> int1_12_...
    val varInConstraint = variableNameMap.get(variable).getOrElse(null)
    if(varInConstraint != null){
      substitutions.get(varInConstraint)
    }else{
      substitutions.get(variable)
    }
  }
}
