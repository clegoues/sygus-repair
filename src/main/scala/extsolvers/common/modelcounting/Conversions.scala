package extsolvers.common.modelcounting

import engines.coreast._

/**
  * Created by dxble on 10/4/16.
  */
object Conversions {
  def main(args: Array[String]) {
    println(toNNF(Not(Not(Not(BoolVar("x"))))))
    println(toNNF(Not(And(Not(Not(BoolVar("x"))), BoolVar("y")))))
    println(toNNF(Not(Not(And(Not(BoolVar("x")), BoolVar("y"))))))
    println(toNNF(Not(Not(Not(Not(BoolVar("x")))))))
    println(toNNF(Not(GTE(Plus(IntVar("a"), IntVar("b")), IntConst("1")))))
    println(toNNF(Not(Not(Not(BoolConst("true"))))))
    println(toNNF((Not(Not(BoolConst("true"))))))
    println("AST: ")
    val ast = Not(And(Or(Not(BoolVar("x")), And(BoolVar("y"), BoolVar("z"))), BoolVar("w")))
    println(ast)
    println(toNNF(ast))
    println(toDNF(toNNF(ast).asInstanceOf[BoolOperatorsCAST]))

    println("AST: ")
    val ast2 = Or(And(And(And(BoolVar("a"), BoolVar("b")), BoolVar("c")), Not(And(And(BoolVar("x"), BoolVar("y")), BoolVar("z")))), And(Not(And(And(BoolVar("a"), BoolVar("b")), BoolVar("c"))), And(And(BoolVar("x"), BoolVar("y")), BoolVar("z"))))
    println(ast2)
    println(toNNF(ast2))
    val dnf = toDNF(toNNF(ast2).asInstanceOf[BoolOperatorsCAST])
    println(dnf)

    println(verifyDNF(dnf))
  }

  // Negation Normal Form using double negation and de morgan to push NOT to leaves
  def toNNF(ast: CAST): CAST = {
    ast match {
      case Not(And(y,z)) => Or(toNNF(Not(y)).asInstanceOf[BoolOperatorsCAST],toNNF(Not(z)).asInstanceOf[BoolOperatorsCAST])
      case Not(Or(y,z)) => And(toNNF(Not(y)).asInstanceOf[BoolOperatorsCAST],toNNF(Not(z)).asInstanceOf[BoolOperatorsCAST])
      case Not(Not(y)) => toNNF(y)
      case Not(GTE(y,z)) => toNNF(LessThan(toNNF(y).asInstanceOf[ArithOperatorsCAST],toNNF(z).asInstanceOf[ArithOperatorsCAST]))
      case Not(Greater(y,z)) => toNNF(LTE(toNNF(y).asInstanceOf[ArithOperatorsCAST],toNNF(z).asInstanceOf[ArithOperatorsCAST]))
      case Not(LTE(y,z)) => toNNF(Greater(toNNF(y).asInstanceOf[ArithOperatorsCAST],toNNF(z).asInstanceOf[ArithOperatorsCAST]))
      case Not(LessThan(y,z)) => toNNF(GTE(toNNF(y).asInstanceOf[ArithOperatorsCAST],toNNF(z).asInstanceOf[ArithOperatorsCAST]))
      case Not(BoolConst(y)) => BoolConst((!y.toBoolean).toString)
      case And(y,z) => And(toNNF(y).asInstanceOf[BoolOperatorsCAST],toNNF(z).asInstanceOf[BoolOperatorsCAST])
      case Or(y,z) => Or(toNNF(y).asInstanceOf[BoolOperatorsCAST],toNNF(z).asInstanceOf[BoolOperatorsCAST])
      case _ => ast
    }
  }


  private def hasOr(node: CAST): Boolean = {
    node match {
      case e:Or => return true
      case And(a, b) => hasOr(a) || hasOr(b)
      case _ => return false
    }
    return false
  }

  private def verifyDNF(node: BoolOperatorsCAST): Boolean = {
    node match {
      case And(a,b) => if(hasOr(node)) return false else return true
      case Or(a,b) => verifyDNF(a) && verifyDNF(b)
      case _ => return true
    }
  }
  /**
    * Example: (a || b) && c => (a && c) || (b && c)
    * a || b || c
    * (a || b) || (c && d)
    * (x >= 1 || x <= 4) || (x >= 2 && x <= 3)          x = [1...5]
    * x 1 5, x 1 4, x 1 4 => 5
    * x 2 3 => 2
    * x 1 4 2 3 => 2
    * Total count: 2
    * ((a || b) || c) && d => ((a || b) && d) || (c && d) => (a && d) || (b && d) || (c && d)
    * ((a || b) && c) && d => ((a && c) || (b && c)) && d => (a && c && d) || (b && c && d)
    * a && b && ((c && d) || (e && f))
    */

  // Note: it is required that ast is in NNF form
  def toDNF_old(ast: BoolOperatorsCAST): BoolOperatorsCAST = {
    ast match {
      case And(Or(a,b), c) => Or(toDNF(And(a, c)), toDNF(And(b, c)))
      case And(c, Or(a,b)) => Or(toDNF(And(a,c)), toDNF(And(b, c)))
      case And(a,b) => And(toDNF(a), toDNF(b))
      case Or(a,b) => Or(toDNF(a), toDNF(b))
      case _ => ast
    }
  }

  // Note: it is required that ast is in NNF form
  def toDNF(ast: BoolOperatorsCAST): BoolOperatorsCAST = {
    def mk_left(x: BoolOperatorsCAST, y: BoolOperatorsCAST): BoolOperatorsCAST = {
      y match {
        case Or(a,b) => Or(mk_left(x,a), mk_left(x,b))
        case _ => And(x,y)
      }
    }
    def mk_right(x: BoolOperatorsCAST, y: BoolOperatorsCAST): BoolOperatorsCAST = {
      x match {
        case Or(a,b) => Or(mk_right(a, y), mk_right(b, y))
        case _ => mk_left(x, y)
      }
    }
    def mk(t: BoolOperatorsCAST): BoolOperatorsCAST = {
      t match {
        case Or(x,y) => Or(mk(x), mk(y))
        case And(x,y) => mk_right(mk(x), mk(y))
        case _ => t
      }
    }

    mk(ast)
  }

}
