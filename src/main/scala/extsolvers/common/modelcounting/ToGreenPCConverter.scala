package extsolvers.common.modelcounting

import engines.coreast.{GreenPCReturnType, _}
import za.ac.sun.cs.green.expr._

import scala.collection.mutable

/**
  * Created by dxble on 10/1/16.
  */
class ToGreenPCConverter(intLowerBound: Int, intUpperBound: Int, not2BoundVars: mutable.HashMap[String, Boolean]) extends CASTVisitorBase{
  var boundingExpression: Expression = null
  val alreadyBoundedVars: mutable.HashMap[String, Boolean] = new mutable.HashMap[String, Boolean]()

  override def visitFunctionSignature(functionDesign: FunctionSignature): GreenPCReturnType ={
    null
  }

  override def visitGreater(greater: Greater): GreenPCReturnType = {
    val left = greater.opr1.accept(this)
    val right = greater.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.GT, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitLessThan(lessThan: LessThan): GreenPCReturnType ={
    val left = lessThan.opr1.accept(this)
    val right = lessThan.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.LT, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitLTE(lTE: LTE): GreenPCReturnType ={
    val left = lTE.opr1.accept(this)
    val right = lTE.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.LE, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitGTE(gTE: GTE): GreenPCReturnType ={
    val left = gTE.opr1.accept(this)
    val right = gTE.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.GE, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitEqual(equal: Equal): GreenPCReturnType={
    val left = equal.opr1.accept(this)
    val right = equal.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.EQ, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitAnd(and: And): GreenPCReturnType={
    val left = and.opr1.accept(this)
    val right = and.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.AND, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitOr(or: Or): GreenPCReturnType={
    val left = or.opr1.accept(this)
    val right = or.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.OR, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitNot(not: Not): GreenPCReturnType={
    val op = not.opr.accept(this)
    if(op.asInstanceOf[GreenPCReturnType].exp.isInstanceOf[IntVariable]){
      // This is the trick to convert boolVar of IntVariable type
      val sub = new Operation(Operation.Operator.SUB, new IntConstant(1), op.asInstanceOf[GreenPCReturnType].exp)
      GreenPCReturnType(new Operation(Operation.Operator.EQ, new IntConstant(1), sub))
    }
    else GreenPCReturnType(new Operation(Operation.Operator.NOT, op.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitImplies(implies: Implies): GreenPCReturnType={
    val left = implies.opr1.accept(this)
    val right = implies.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.IMPLIES, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitBoolVar(boolVar: BoolVar): GreenPCReturnType ={
    val bvar = new IntVariable(boolVar.name, 0, 1)
    if(!alreadyBoundedVars.contains(boolVar.name) && !not2BoundVars.contains(boolVar.name)) {
      val bBoundL = new Operation(Operation.Operator.GE, bvar, new IntConstant(0))
      val bBoundU = new Operation(Operation.Operator.LE, bvar, new IntConstant(1))
      val boundExp = new Operation(Operation.Operator.AND, bBoundL, bBoundU)
      if (boundingExpression == null)
        boundingExpression = boundExp
      else {
        boundingExpression = new Operation(Operation.Operator.AND, boundingExpression, boundExp)
      }
      alreadyBoundedVars.put(boolVar.name, true)
    }
    GreenPCReturnType(new Operation(Operation.Operator.EQ, bvar, new IntConstant(1)))
  }

  override def visitBoolConst(boolConst: BoolConst): GreenPCReturnType={
    val value = boolConst.value.toBoolean
    if(value){
      GreenPCReturnType(new Operation(Operation.Operator.EQ, new IntConstant(1), new IntConstant(1)))
    }else{
      GreenPCReturnType(new Operation(Operation.Operator.EQ, new IntConstant(1),new IntConstant(0)))
    }
  }

  override def visitPlus(plus: Plus): GreenPCReturnType={
    val left = plus.opr1.accept(this)
    val right = plus.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.ADD, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitMinus(minus: Minus): GreenPCReturnType={
    val left = minus.opr1.accept(this)
    val right = minus.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.SUB, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitDiv(div: Div): GreenPCReturnType={
    val left = div.opr1.accept(this)
    val right = div.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.DIV, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitMult(mult: Mult): GreenPCReturnType={
    val left = mult.opr1.accept(this)
    val right = mult.opr2.accept(this)
    GreenPCReturnType(new Operation(Operation.Operator.MUL, left.asInstanceOf[GreenPCReturnType].exp, right.asInstanceOf[GreenPCReturnType].exp))
  }

  override def visitIntConst(intConst: IntConst): GreenPCReturnType={
    GreenPCReturnType(new IntConstant(intConst.value.toInt))
  }

  override def visitIntVar(intVar: IntVar): GreenPCReturnType = {
    val ivar = new IntVariable(intVar.name, intLowerBound, intUpperBound)
    if(!alreadyBoundedVars.contains(intVar.name) && !not2BoundVars.contains(intVar.name)) {
      val bBoundL = new Operation(Operation.Operator.GE, ivar, new IntConstant(intLowerBound))
      val bBoundU = new Operation(Operation.Operator.LE, ivar, new IntConstant(intUpperBound))
      val boundExp = new Operation(Operation.Operator.AND, bBoundL, bBoundU)
      if (boundingExpression == null)
        boundingExpression = boundExp
      else {
        boundingExpression = new Operation(Operation.Operator.AND, boundingExpression, boundExp)
      }
      alreadyBoundedVars.put(intVar.name, true)
    }
    GreenPCReturnType(ivar)
  }
}
