package extsolvers.common

import engines.coreast._

import scala.collection._
/**
  * Created by dxble on 7/14/16.
  */
class ConstraintsOfFuncCollector(functionDesign: FunctionSignature) extends CASTVisitorBase{
  val constraintsOfFunc = new mutable.ArrayBuffer[CAST]()
  val angelicValues = new mutable.HashSet[InternalTerm]()
  var matched = false

  override def visitFunctionSignature(func: FunctionSignature): VisitorReturnType = {
    if(functionDesign.equals(func))
      matched = true

    return EmptyType()
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = EmptyType()

  override def visitIntVar(intVar: IntVar): VisitorReturnType = EmptyType()

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType={
    if(matched){
      angelicValues.add(boolConst)
    }
    return EmptyType()
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType={
    if(matched){
      angelicValues.add(intConst)
    }
    return EmptyType()
  }

  override def visitImplies(implies: Implies): VisitorReturnType ={
    val temp = implies.opr2.accept(this)
    if(matched) {
      constraintsOfFunc.append(implies)
      matched = false
    }
    return temp
  }
}
