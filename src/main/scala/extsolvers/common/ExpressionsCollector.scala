package extsolvers.common

import engines.coreast.{IntConst, IntVar, _}

/**
  * Created by dxble on 7/14/16.
  */
class ExpressionsCollector extends CASTVisitorBase{
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType ={
    functionDesign.args.foldLeft(new ExpList(List[CAST]()))((res, arg) => res.concat(arg.accept(this).asInstanceOf[ExpList])).concat(new ExpList(List[CAST](functionDesign)))
  }

  override def visitGreater(greater: Greater): VisitorReturnType = {
    greater.opr1.accept(this).asInstanceOf[ExpList].concat(greater.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](greater)))
  }
  override def visitLessThan(lessThan: LessThan): VisitorReturnType ={
    lessThan.opr1.accept(this).asInstanceOf[ExpList].concat(lessThan.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](lessThan)))

  }
  override def visitLTE(lTE: LTE): VisitorReturnType ={
    lTE.opr1.accept(this).asInstanceOf[ExpList].concat(lTE.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](lTE)))
  }

  override def visitGTE(gTE: GTE): VisitorReturnType ={
    gTE.opr1.accept(this).asInstanceOf[ExpList].concat(gTE.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](gTE)))
  }
  override def visitEqual(equal: Equal): VisitorReturnType={
    equal.opr1.accept(this).asInstanceOf[ExpList].concat(equal.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](equal)))
  }
  override def visitAnd(and: And): VisitorReturnType={
    and.opr1.accept(this).asInstanceOf[ExpList].concat(and.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](and)))
  }
  override def visitOr(or: Or): VisitorReturnType={
    or.opr1.accept(this).asInstanceOf[ExpList].concat(or.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](or)))
  }
  override def visitNot(not: Not): VisitorReturnType={
    not.opr.accept(this).asInstanceOf[ExpList].concat(new ExpList(List[CAST](not)))
  }
  override def visitImplies(implies: Implies): VisitorReturnType={
    implies.opr1.accept(this).asInstanceOf[ExpList].concat(implies.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](implies)))
  }


  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType ={
    new ExpList(List[CAST](boolVar))
  }

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType={
    new ExpList(List[CAST](boolConst))
  }

  override def visitPlus(plus: Plus): VisitorReturnType={
    plus.opr1.accept(this).asInstanceOf[ExpList].concat(plus.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](plus)))
  }

  override def visitMinus(minus: Minus): VisitorReturnType={
    minus.opr1.accept(this).asInstanceOf[ExpList].concat(minus.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](minus)))
  }

  override def visitDiv(div: Div): VisitorReturnType={
    div.opr1.accept(this).asInstanceOf[ExpList].concat(div.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](div)))
  }

  override def visitMult(mult: Mult): VisitorReturnType={
    mult.opr1.accept(this).asInstanceOf[ExpList].concat(mult.opr2.accept(this).asInstanceOf[ExpList]).concat(new ExpList(List[CAST](mult)))
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType={
    new ExpList(List[CAST](intConst))
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType ={
    new ExpList(List[CAST](intVar))
  }
}
