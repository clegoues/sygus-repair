package extsolvers.utils

import extsolvers.common.Solution
import engines.coreast.{Implies, LTE, _}
import synthlib2parser.FunGTerm

import scala.util.parsing.input.Position

/**
  * Created by dxble on 7/17/16.
  */
object ExtUtils {
  def crossProduct[T](inputList: List[List[T]]): List[List[T]] = {
    def fun(xs: List[T], zss: List[List[T]]): List[List[T]] = {
      for {
        x <- xs
        zs <- zss
      } yield {
        x :: zs
      }
    }
    val crossProd: List[List[T]] = inputList.foldRight(List(List[T]()))(fun _)
    crossProd
  }

  /**
    * Note that in this case exp and e must be of the same kind, e.g., binary operators
    *
    * @param e
    * @param exp
    * @return
    */
  def transformTheoryFun2CoreAST(funName: String, exp: CAST, position: Position): CAST ={
    funName match {
      case "and" => new And(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[BoolOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[BoolOperatorsCAST], position)
      case "or" => new Or(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[BoolOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[BoolOperatorsCAST], position)
      case "not" => new Not(exp.asInstanceOf[BoolOperatorsCAST], exp.position)
      case "+" => new Plus(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "-" => new Minus(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "*" => new Mult(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "/" => new Div(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "=" => new Equal(exp.asInstanceOf[BinOp].getLeft(),exp.asInstanceOf[BinOp].getRight())
      case ">" => new Greater(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case ">=" => new GTE(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "<" => new LessThan(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "<=" => new LTE(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[ArithOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[ArithOperatorsCAST], position)
      case "=>" => new Implies(exp.asInstanceOf[BinOp].getLeft().asInstanceOf[BoolOperatorsCAST],exp.asInstanceOf[BinOp].getRight().asInstanceOf[BoolOperatorsCAST], position)
    }
  }

  def transformTheoryFun2CoreAST(funName: String, expLeft: CAST, expRight: CAST, position: Position): CAST ={
    try {
      funName match {
        case "and" => new And(expLeft.asInstanceOf[BoolOperatorsCAST], expRight.asInstanceOf[BoolOperatorsCAST], position)
        case "or" => new Or(expLeft.asInstanceOf[BoolOperatorsCAST], expRight.asInstanceOf[BoolOperatorsCAST], position)
        //case "not" => new Not(expLeft.asInstanceOf[BoolOperatorsCAST])
        case "+" => new Plus(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "-" => new Minus(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "*" => new Mult(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "/" => new Div(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "=" => new Equal(expLeft, expRight, position)
        case ">" => new Greater(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case ">=" => new GTE(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "<" => new LessThan(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "<=" => new LTE(expLeft.asInstanceOf[ArithOperatorsCAST], expRight.asInstanceOf[ArithOperatorsCAST], position)
        case "=>" => new Implies(expLeft.asInstanceOf[BoolOperatorsCAST], expRight.asInstanceOf[BoolOperatorsCAST], position)
      }
    }catch {
      case e: ClassCastException => null
    }
  }

}
