package extsolvers.metasearch.searcher

import extsolvers.common.UnrolledGrammar
import engines.coreast._
import extsolvers.utils.ExtUtils
import synthlib2parser._
import synthlib2parser.scopemanager.SymbolTable
import org.apache.log4j.Logger

import scala.collection.mutable
import scala.util.Random

/**
  * Created by dxble on 7/15/16.
  */
abstract class Searcher{
  def search(): mutable.HashSet[CAST]
}
case class MutationSeacher(exp: CAST, rand: Random, niter: Int, unrolledGrammar: UnrolledGrammar, synthFunCmd: SynthFunCmd, theSymbolTable: SymbolTable) extends Searcher{
  val logger = Logger.getLogger(this.getClass)


  override def search(): mutable.HashSet[CAST] = {
    val results = new mutable.HashSet[CAST]()
    //A scope is an NTDef
    val scopeList = unrolledGrammar.findScopesOfExp(exp)
    var count = 0

    while (results.size < niter && count < niter) {
      scopeList.scopesList.foreach {
        scopeName => {
          val expansions = unrolledGrammar.scopeRules.get(scopeName.scope).getOrElse(null)
          val expRule = expansions.find(_ match {
            case e: FunGTerm => e.funName.equals(exp.topLevelString())
            case e: LiteralGTerm => e.litGTerm.litStr.equals(exp.topLevelString())
            case e: SymbolGTerm => e.symbol.equals(exp.topLevelString())
          }).getOrElse(throw new RuntimeException("Exp with "+exp.topLevelString()+" is not defined in the grammar!"))

          /**
            * For now, we filter expansions of the same size and same type with exp. Note: we allow not(not)
            */
          val filteredExps = expansions.filter(_ match {
            case e: FunGTerm => {
              //println(e)
              if (expRule.isInstanceOf[FunGTerm])
                if(!e.funName.equals("not"))
                  !e.funName.equals(exp.topLevelString()) && e.args.equals(expRule.asInstanceOf[FunGTerm].args)
                else //Allow Not opr on any bool operator
                  exp.isInstanceOf[BoolOperatorsCAST]
              //e.args.equals(expRule.asInstanceOf[FunGTerm].args)
              else false
            }
            case e: LiteralGTerm => !e.litGTerm.litStr.equals(exp.topLevelString()) && exp.size() == 1
            case e: SymbolGTerm => !e.symbol.equals(exp.topLevelString()) && exp.size() == 1
          })

          logger.debug("FilteredExp: " + filteredExps)
          while (results.size < niter / scopeList.scopesList.size && count < niter) {
            val chosenIndex = rand.nextInt(filteredExps.size)
            val chosenRule = filteredExps(chosenIndex)
            logger.debug("Chosen Exp Rule: " + chosenRule)

            val mutated = chosenRule match {
              case e: FunGTerm => ExtUtils.transformTheoryFun2CoreAST(e.funName, exp, exp.position)
              case e: LiteralGTerm => {
                e.litGTerm.sort match {
                  case BoolSortExp() => new BoolConst(e.litGTerm.litStr, exp.position)
                  case IntSortExp() => new IntConst(e.litGTerm.litStr, exp.position)
                }
              }
              case e: SymbolGTerm => {
                /*val entry = synthFunCmd.scope.lookup(e.symbol)
                if(entry != null){
                  entry.getSort() match {
                    case BoolSortExp() => BoolVar(e.symbol)
                    case IntSortExp() => IntVar(e.symbol)
                  }
                }else{
                  null
                }*/
                unrolledGrammar.scope2Type.get(scopeName.scope).getOrElse(null) match {
                  case BoolSortExp() => new BoolVar(e.symbol, exp.position)
                  case IntSortExp() => new IntVar(e.symbol, exp.position)
                }
              }
            }
            count += 1
            if(mutated.getSort() == exp.getSort()) {
              results.add(mutated)
            }else{
              logger.debug("If happen, why? We need to debug enumerator.")
            }
          }
        }
      }
    }
    logger.debug("Mutants: "+results + "assert same pos: "+ results.groupBy(_.position))
    results
  }
}

case class EnumerationSeacher(exp: CAST, rand: Random, maxSize: Int, minSize: Int,unrolledGrammar: UnrolledGrammar, synthFunCmd: SynthFunCmd) extends Searcher{
  val logger = Logger.getLogger(this.getClass)

  // expStack is map from: Scope -> Map[Exp Size -> Expression Set]
  // Note that created exps are NoPosition exp by default
  val expStack = new mutable.HashMap[String, mutable.HashMap[Int, mutable.HashSet[CAST]]]()

  def getExpStackSize(): Int ={
    expStack.foldLeft(0){
      (res, stack) => {
        res + stack._2.foldLeft(0){
          (innerRes, exps) => {
            innerRes + exps._2.size
          }
        }
      }
    }
  }

  // This is only called on a fresh instantiation of EnumerationSearcher
  override def search(): mutable.HashSet[CAST] = {
    assert(expStack.size == 0)
    val expEnumerator = new ExpEnumerator2(expStack, unrolledGrammar, maxSize, true)
    synthFunCmd.accept(expEnumerator)
    harvestExp(exp, maxSize, minSize)
  }

  def searchWithExp(anExp: CAST, size: Int, minExpSize: Int): mutable.HashSet[CAST] = {
    //A scope is an NTDef
    if(size <= maxSize) {
      harvestExp(anExp, size, minExpSize)
    }else{
      val expEnumerator = new ExpEnumerator2(expStack, unrolledGrammar, size, true)
      synthFunCmd.accept(expEnumerator)
      harvestExp(anExp, size, minExpSize)
    }
  }

  def harvestExp(sketchExp: CAST, maxHarvestSize: Int, minHarvestSize: Int): mutable.HashSet[CAST] ={
    val scopeList = unrolledGrammar.findScopesOfExp(sketchExp)
    val results = scopeList.scopesList.foldLeft(new mutable.HashSet[CAST]()) {
      (res, scopeName) => {
        val expWithSize = expStack.get(scopeName.scope) match {
          case None => {
            //val expEnumerator = new ExpEnumerator2(expStack, unrolledGrammar, size, true, anExp.position)
            //synthFunCmd.accept(expEnumerator)
            //expStack.get(scopeName.scope).getOrElse(null)
            throw new RuntimeException("Should not happen! Do not find exp of scope: " + scopeName)
          }
          case Some(x) => x
        }
        expWithSize.values.foreach {
          exps => {
            exps.foreach(e => {
              //TODO: to be safe for now. Later need to check why generate type different from exp within the same scope
              if (e.getSort() == sketchExp.getSort()) {
                val eSize = e.size()
                if(e.toString.contains("enabled") && e.toString.contains("this.acknowledgeId == NOACK"))
                  logger.info("Debug here")
                if(eSize >= minHarvestSize && eSize <= maxHarvestSize && !e.equals(sketchExp)) {
                  // Note that we need a clone of position. We transform (replace) old exp by using position as an identifier
                  val ce = e.myClone()
                  ce.setPosition(sketchExp.clonePosition())
                  res.add(ce)
                }
              }
              else {
                logger.debug("If happen, why? We need to debug enumerator.")
              }
            })
          }
        }
        res
      }
    }
    //results.foreach(r => logger.info(r+" "+r.position+" for "+sketchExp+" "+sketchExp.position))
    results
  }

  /*
   //A scope is an NTDef
   val scopeList = unrolledGrammar.findScopesOfExp(exp)
   //println("Scopelist of "+exp+" is"+scopeList)
   scopeList.scopesList.foldLeft(new mutable.HashSet[CAST]()) {
     (res,scopeName) => {
       val expWithSize = expStack.get(scopeName.scope) match {
         case None => {
           //val expEnumeratorTest = new ExpEnumerator2(unrolledGrammar, maxSize, true, exp.position)
           //synthFunCmd.accept(expEnumeratorTest)
           throw new RuntimeException("Should not happen! Do not find exp of scope: "+scopeName)
         }
         case Some(x) => x
       }
       expWithSize.values.foreach{
         exps => {
           exps.foreach(e => {
             //TODO: to be safe for now. Later need to check why generate type different from exp within the same scope
             if(e.getSort() == exp.getSort()){
               // Note that we need a clone of position. We transform (replace) old exp by using position as an identifier
               val ce = e.myClone()
               ce.setPosition(exp.clonePosition())
               res.add(ce)
             }
             else{
               logger.debug("If happen, why? We need to debug enumerator.")
             }
           })
         }
       }
       res
     }
   }*/
}
