package extsolvers.metasearch.searcher

/**
  * Created by dxble on 7/18/16.
  */

import extsolvers.common.UnrolledGrammar
import engines.coreast._
import extsolvers.utils.ExtUtils
import synthlib2parser._

import scala.collection.{mutable, _}
import scala.util.parsing.input.{NoPosition, Position}
/**
  * Created by dxble on 7/17/16.
  */
/**
  * This should be called on each synth-fun cmd
  *
  * @param sort
  * @param unrolledGrammar
  */
class ExpEnumerator2(expStack: mutable.HashMap[String, mutable.HashMap[Int, mutable.HashSet[CAST]]], unrolledGrammar: UnrolledGrammar, var limitSize: Int, var keepPushingScope: Boolean) extends ASTVisitorBase("ExpressionEnumerator"){
  var sort: SortExp = null
  var currentMaxSize = limitSize
  var currentScope: String = null
  //ScopeName -> [size -> exps of this size]
  //val expStack = new mutable.HashMap[String, mutable.HashMap[Int, mutable.HashSet[CAST]]]()
  val scopeStack = new mutable.Stack[NTDef]()
  val scopeMaxsize = new mutable.HashMap[String, Int]()

  private def isScopeSymbol(symbol: String): Boolean ={
    unrolledGrammar.scope2Type.get(symbol) match {
      case None => false
      case _ => true
    }
  }

  override def visitSynthFunCmd(synthFunCmd: SynthFunCmd) = {
    // Building scope stack
    synthFunCmd.grammarRules.foreach(rule => {
      rule.accept(this)
    })
    // Popping scope stack
    while (scopeStack.size > 0) {
      val ntdef = scopeStack.pop()
      val maxSize = scopeMaxsize.get(ntdef.symbol).getOrElse(throw new RuntimeException("Cannot get max size for scope: "+ntdef.symbol))
      limitSize = maxSize
      keepPushingScope = false
      ntdef.accept(this)
    }
  }

  override def visitNTDef(nTDef: NTDef) = {
    currentScope = nTDef.symbol
    sort = nTDef.sort
    if(keepPushingScope) {
      scopeStack.push(nTDef)
      if(!scopeMaxsize.contains(nTDef.symbol))
        scopeMaxsize += nTDef.symbol -> currentMaxSize
      nTDef.expansions.foreach(e => {
        //Visit FunGTerm to push scope
        if(!e.isInstanceOf[LiteralGTerm] && !e.isInstanceOf[SymbolGTerm])
          e.accept(this)
      })
    }else{
      // Now we are popping the scope stack to build expressions
      // Build Exp of size 1
      nTDef.expansions.filter(p => p.isInstanceOf[LiteralGTerm] || p.isInstanceOf[SymbolGTerm]).foreach(litOrSym => litOrSym.accept(this))
      val funGTermList =  nTDef.expansions.filter(p => p.isInstanceOf[FunGTerm])
      currentMaxSize = scopeMaxsize.get(nTDef.symbol).getOrElse(throw new RuntimeException("Found no currentmaxsize of this: "+nTDef))
      val r = 2 to currentMaxSize
      r.foreach(size =>
        funGTermList.foreach(e => {
          //val permittedSizeOfThisFunc = currentMaxSize - e.asInstanceOf[FunGTerm].args.size + 1
          val minSize = e.asInstanceOf[FunGTerm].args.size + 1
          //Visit FunGTerm to build larger expressions
          //if(size == 6 && currentScope == "Start")
          //  println("Debug")
          if(minSize <= size /*&& size <= permittedSizeOfThisFunc*/)
            createExps(e.asInstanceOf[FunGTerm], size, currentScope)
        })
      )
    }
  }

  def buildExpsOfSize(scope: String, expSize: Int): Unit = {
    unrolledGrammar.scopeRules.get(scope).getOrElse(null).filter(p => p.isInstanceOf[FunGTerm]).foreach( f =>{
      createExps(f.asInstanceOf[FunGTerm], expSize, scope)
    })
  }

  private def createExps(term: FunGTerm, size: Int, scopeOfExp: String): Unit = {
    //Do this while popping the scope stack
    if(size < 2)
      return
    if(term.funName.equals("not")) {
      //Unary operator
      if (term.args(0).isInstanceOf[SymbolGTerm]) {
        val scope = term.args(0).asInstanceOf[SymbolGTerm].symbol
        val iterSize = 1 to size - 1
        iterSize.foreach(s => {
          buildExpsOfSize(scope, s)
          expStack.get(scope) match {
            case None => {/*throw new RuntimeException("There should be some of size: "+s+" for "+scope)*/}
            case Some(expsOfManySizes) =>{
              expsOfManySizes.get(s) match {
                case None => {/*throw new RuntimeException("There should be some of size "+s+" of "+scope)*/}
                case Some(expsOfSizeS) => {
                  expsOfSizeS.foreach(exp =>{
                    if(exp.isInstanceOf[BoolOperatorsCAST]){
                      val newExp = new Not(exp.asInstanceOf[BoolOperatorsCAST], NoPosition)
                      val expSizeSPlusOne = expsOfManySizes.get(s+1).getOrElse(new mutable.HashSet[CAST]())
                      expSizeSPlusOne.add(newExp)
                      expsOfManySizes.update(s+1, expSizeSPlusOne)
                      expStack.update(scope, expsOfManySizes)
                    }
                  })
                }
              }
            }
          }
        })
      } else {
        //TODO: handle function application on other terms later
      }
    }else{//Binary operator
      if(size < 3)
        return
      if (term.args(0).isInstanceOf[SymbolGTerm] && term.args(1).isInstanceOf[SymbolGTerm]) {
        val scopeLeft = term.args(0).asInstanceOf[SymbolGTerm].symbol
        val scopeRight = term.args(1).asInstanceOf[SymbolGTerm].symbol
        val iterSize = 1 to size - 1
        iterSize.foreach(s => {
          buildExpsOfSize(scopeLeft, s)
          val remain = (size - 1) - s
          buildExpsOfSize(scopeRight, remain)
          val leftSizeS = {
            expStack.get(scopeLeft) match {
              case None =>{
                //println("xxx")
                null
              }
              case Some(expOfManySizes) => expOfManySizes.get(s).getOrElse(null)
            }
          }
          if(leftSizeS != null){
            val rightSizeRemain = expStack.get(scopeRight).getOrElse(null).get(remain).getOrElse(null)
            if(rightSizeRemain != null)
              builExpsFromCrossProduct(leftSizeS, rightSizeRemain, term.funName, scopeOfExp)
          }
        })
      } else {
        //TODO: handle function application on other terms later
      }
    }
  }

  // This is meant to push scope to stack. A FunGTerm may contain SymbolGTerm that is a scope, e.g., IntExp
  // We take a lazy approach by pushing the scope we encounter to a stack. Later we will unroll all scopes in
  // that stack, meaning we generate expressions from the leave nodes (scopes)
  override def visitFunGTerm(term: FunGTerm) ={
    if(term.args.size <= currentMaxSize - 1) {
      if(keepPushingScope) {
        term.args.foreach(arg => {
          if (arg.isInstanceOf[SymbolGTerm]) {
            val argSymbol = arg.asInstanceOf[SymbolGTerm].symbol
            if (isScopeSymbol(argSymbol)) {
              scopeMaxsize.get(argSymbol) match {
                case None => scopeMaxsize += (argSymbol -> (currentMaxSize - term.args.size))
                case Some(s) => {
                  if (s < currentMaxSize - term.args.size)
                    scopeMaxsize.update(argSymbol, currentMaxSize - term.args.size)
                }
              }
            }
          }
        })
      }
    }
  }

  private def builExpsFromCrossProduct(e1: mutable.HashSet[CAST], e2: mutable.HashSet[CAST], funName: String, scope: String) ={
    val crossP = ExtUtils.crossProduct(List(e1.toList, e2.toList))
    crossP.foreach(cr => {
      if(cr(0).getSort() == cr(1).getSort()) {
        val newExp = ExtUtils.transformTheoryFun2CoreAST(funName, cr(0), cr(1), NoPosition)
        if(newExp != null) {
          expStack.get(scope) match {
            case None => {
              val mapCurrentScopeExps = new mutable.HashMap[Int, mutable.HashSet[CAST]]()
              val expsOfCurrentScope = new mutable.HashSet[CAST]()
              expsOfCurrentScope.add(newExp)
              mapCurrentScopeExps += newExp.size() -> expsOfCurrentScope
              expStack.update(scope, mapCurrentScopeExps)
            }
            case Some(e) => {
              e.get(newExp.size()) match {
                case None => {
                  val tempSet = new mutable.HashSet[CAST]()
                  tempSet.add(newExp)
                  e += newExp.size() -> tempSet
                }
                case Some(existingExps) => {
                  existingExps.add(newExp)
                  e.update(newExp.size(), existingExps)
                }
              }
              expStack.update(scope, e)
            }
          }
        }
      }
    })
  }

  override def visitSymbolGTerm(term: SymbolGTerm) ={
    if(currentMaxSize >= 1 && !keepPushingScope) {
      val expSet = expStack.get(currentScope).getOrElse(new mutable.HashMap[Int, mutable.HashSet[CAST]]())
      sort match {
        case BoolSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(BoolVar(term.symbol))
          expSet += (1 -> s)
        }
        case IntSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(IntVar(term.symbol))
          expSet += (1 -> s)
        }
      }
      expStack.update(currentScope, expSet)
    }
  }

  override def visitLiteralGTerm(term: LiteralGTerm) ={
    if(currentMaxSize >= 1 && !keepPushingScope) {
      val expSet = expStack.get(currentScope).getOrElse(new mutable.HashMap[Int, mutable.HashSet[CAST]]())
      sort match {
        case BoolSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(BoolConst(term.litGTerm.litStr))
          expSet += (1 -> s)
        }
        case IntSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(IntConst(term.litGTerm.litStr))
          expSet += (1 -> s)
        }
      }
      expStack.update(currentScope, expSet)
    }
  }
}

