package extsolvers.metasearch.solutionclassifier.clustering.mcl;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 * Created by dxble on 7/20/16.
 */
public class Test {
    public static void main(String[] args){
        UndirectedGraph<Integer, DefaultEdge> g =
                new SimpleGraph<>(DefaultEdge.class);

        Integer v1 = 1;
        Integer v2 = 2;
        Integer v3 = 3;
        Integer v4 = 4;

        Integer v5 = 5;
        Integer v6 = 6;
        Integer v7 = 7;
        Integer v8 = 8;
        // add the vertices
        g.addVertex(v1);
        g.addVertex(v2);
        g.addVertex(v3);
        g.addVertex(v4);

        g.addVertex(v5);
        g.addVertex(v6);
        g.addVertex(v7);
        g.addVertex(v8);
        // add edges to create a circuit
        g.addEdge(v1, v2);
        g.addEdge(v1, v3);
        g.addEdge(v2, v4);
        g.addEdge(v2, v3);
        g.addEdge(v3, v4);
        g.addEdge(v4, v1);

        g.addEdge(v5, v6);
        g.addEdge(v5, v7);
        g.addEdge(v5, v8);
        g.addEdge(v6, v7);
        g.addEdge(v6, v8);
        g.addEdge(v7, v8);

        MCClusterer cl = new MCClusterer();
        cl.execute(g);


    }
}
