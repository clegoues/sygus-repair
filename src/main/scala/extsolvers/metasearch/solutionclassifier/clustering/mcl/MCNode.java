package extsolvers.metasearch.solutionclassifier.clustering.mcl;

/**
 * Created by dxble on 7/20/16.
 */

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jiri Krizek
 */
public class MCNode {

    private final Integer node;
    private MCClusterImpl cluster;
    private List<MCClusterImpl> clusterList;
    private final String COLUMN = "MarkovClusters";

    MCNode(Integer node) {
        this.node = node;
        this.clusterList = new ArrayList<MCClusterImpl>();
    }

    @Override
    public int hashCode(){
        return node.intValue();
    }

    @Override
    public boolean equals(Object that){
        if(that instanceof MCNode){
            if(((MCNode)that).getNode().intValue() == node.intValue())
                return true;
            else
                return false;
        }else{
            return false;
        }
    }

    public MCClusterImpl addToCluster(MCClusterImpl cluster) {
        this.cluster = cluster;
        this.cluster.addNode(this);
        clusterList.add(cluster);
        //this.getNode().getAttributes().setValue(COLUMN, this.getClustersIntList());
        return cluster;
    }

    public boolean isMultiCluster() {
        return getNumberOfClusters() > 1;
    }

    public Integer getNode() {
        return node;
    }

    private int getNumberOfClusters() {
        return clusterList.size();
    }

    public List<MCClusterImpl> getClusters() {
        return clusterList;
    }

    /*private IntegerList getClustersIntList() {
        int[] res = new int[clusterList.size()];

        for (int i=0; i<clusterList.size(); i++) {
            res[i] = clusterList.get(i).getId();
        }

        return new IntegerList(res);
    }*/
}