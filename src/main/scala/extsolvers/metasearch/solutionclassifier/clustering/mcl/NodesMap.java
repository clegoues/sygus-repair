package extsolvers.metasearch.solutionclassifier.clustering.mcl;

/**
 * Created by dxble on 7/20/16.
 */

import org.jgrapht.Graph;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
/**
 *
 * @author Jiri Krizek
 */
class NodesMap {
    // Mapping of nodeId to sequential id
    Map<Integer, Integer> nodeTable = new HashMap<Integer, Integer>();
    // Mapping of sequential id to nodeId
    Map<Integer, Integer> idsTable = new HashMap<Integer, Integer>();
    private final Graph graph;

    public NodesMap(Graph graph) {
        this.graph = graph;
        int i = 0;
        Iterator iter = graph.vertexSet().iterator();
        while (iter.hasNext()){
            Integer node = (Integer) iter.next();
            nodeTable.put(node, new Integer(i));
            idsTable.put(i, node);
            i++;
        }
    }

    public int getSequentialIdFor(Integer node) {
        return nodeTable.get(node).intValue();
    }

    public Integer getNodeForId(int id) {
        return idsTable.get(id);
    }


}
