package extsolvers.metasearch.solutionclassifier
import engines.coreast._
import extsolvers.common._

import scala.collection._
/**
  * Created by dxble on 7/19/16.
  */
class ConstraintEvalOnInput(funcSubs: mutable.HashMap[FunctionSignature, Solution], solver: AbstractSolver, input: mutable.HashMap[InternalTerm, DataType]) extends ConcreteEvaluation(null, funcSubs){
  @throws(classOf[DivisionByZeroException])
  override def visitImplies(implies: Implies): VisitorReturnType ={
    //Each cstr is of the form: (=> X Y)
    //Collect all substitutions of variables in each constraint cstr
    val subsCollector = new SubsCollectorOnInput(solver, input)
    implies.accept(subsCollector)

    //Concrete evaluation of solution expressions on implies constraints.
    val concreteEvaluation = new EvaluatorOnRandomInput(subsCollector, funcSubs)
    implies.accept(concreteEvaluation)
  }
}
