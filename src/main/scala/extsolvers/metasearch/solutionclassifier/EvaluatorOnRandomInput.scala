package extsolvers.metasearch.solutionclassifier

import extsolvers.common.{DefaultSubsCollector, Solution}
import engines.coreast._

import scala.collection.mutable

/**
  * Created by dxble on 7/19/16.
  */
class EvaluatorOnRandomInput(collectedSubst: DefaultSubsCollector, funcSubs: mutable.HashMap[FunctionSignature, Solution]) extends CASTVisitorBase{
  // We only evaluate on function: e.g., x = 2 => f x = 10
  // Then we only evaluate f x with given random input in the collectedSubst, e.g., x = 5
  @throws(classOf[DivisionByZeroException])
  override def visitImplies(implies: Implies) ={
    implies.opr2.accept(this)
  }

  @throws(classOf[DivisionByZeroException])
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    //val concrete = concreteSubs.get(functionDesign).getOrElse(null) //throw new RuntimeException("Found No Substitution of: "+functionDesign
    //if(concrete == null) {
    val subst = funcSubs.get(functionDesign).getOrElse(throw new RuntimeException("Found No Substitution of: " + functionDesign))
    // TODO: what if substitution function (solution) is of the form X => Y?
    return subst.accept(this)
    //}else
    //return concrete
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    collectedSubst.findSubstitution(boolVar) match {
      case None => throw new RuntimeException("Found No Substitution of: "+boolVar)
      case Some(sub) => sub
    }
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    collectedSubst.findSubstitution(intVar) match {
      case None => {
        //collectedSubst.findSubstitution(intVar)
        throw new RuntimeException("Found No Substitution of: "+intVar)
      }
      case Some(sub) => sub
    }
  }
}

