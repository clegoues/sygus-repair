package extsolvers.metasearch.solutionclassifier

/*import java.io.FileWriter
import java.util

import extsolvers.common._
import engines.coreast._
import extsolvers.metasearch.selection.SelectionScheme
import extsolvers.metasearch.solutionclassifier.clustering.mcl.{MCClusterImpl, MCClusterer}
import org.apache.log4j.Logger
import org.jgrapht.alg.GabowStrongConnectivityInspector
import org.jgrapht.ext
import org.jgrapht.ext.{ComponentAttributeProvider, IntegerNameProvider, StringNameProvider}
import org.jgrapht.graph._
import z3.scala.Z3Context

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
/**
  * Created by dxble on 7/19/16.
  */
class RandomInputRunner(solver: AbstractSolver, random: Random, realSolutions: mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]]) {
  val logger = Logger.getLogger(this.getClass)
  val clustersNonDiv = new mutable.HashSet[MCClusterImpl]()
  val clusterDiv = new mutable.HashSet[MCClusterImpl]()

  def runner(useGraphCLustering: Boolean): mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]] = {
    var correspondingIndex = 0
    val indexedSolutions = realSolutions.foldLeft(new mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]]()){
      (res, eachSol) => {
        res += (correspondingIndex -> eachSol)
        correspondingIndex += 1
        res
      }
    }

    logger.debug(indexedSolutions)
    // Collect all variable names in constraints
    val varsCollector = new ConstraintVarsCollector(solver)
    solver.allConstraints.foreach(constr => {constr.accept(varsCollector)})

    val inputSet = RandomInputGenerator.generate(50, random, varsCollector.allVariableNames)

    val classifiedSolutions = inputRunner(inputSet, indexedSolutions, indexedSolutions.keySet)

    val filteredClassifier = classifiedSolutions.filter(eachInputRes => eachInputRes.size > 1)
    if(filteredClassifier.size == 0){
      val chosenIDSet = classifiedSolutions.head.head._2
      val chosenSol = indexedSolutions.get(chosenIDSet.head).getOrElse(null)
      val res = new mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]]()
      res.add(chosenSol)
      return res
    }

    if(useGraphCLustering) {
      clusterGraph(indexedSolutions, indexedSolutions.keySet, filteredClassifier, varsCollector)
      chooseSolutionFromClusters(clustersNonDiv, indexedSolutions)
    }
    else{
      // Take out List[(index, group size)]
      val chosenIDs = filteredClassifier.foldLeft(List[(Int, Int)]()){
        (res, eachInputRes) =>{
          val tempRes = eachInputRes.foldLeft(List[(Int, Int)]())(
            (resInner, out2GroupOfSol) =>{
              val (_,solutions) = out2GroupOfSol
              val randIndex = random.nextInt(solutions.size)
              val chosenID = solutions.toList(randIndex)
              resInner :+ (chosenID, solutions.size)
            })
          res ::: tempRes
        }
      }

      val chosenID = SelectionScheme.tournamentSelection(chosenIDs, 2, random, true, compareFitness)
      val correspondingSolution = indexedSolutions.get(chosenID._1).getOrElse(null)
      logger.debug("Chosen: "+correspondingSolution+" id:"+chosenID._1)
      val res = new mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]]()
      res.add(correspondingSolution)
      return res
    }
  }

  def inputRunner(inputSet:mutable.HashSet[mutable.HashMap[InternalTerm, DataType]],
                  indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]],
                  runOnSolutions: scala.collection.Set[Int])
  : ArrayBuffer[mutable.HashMap[ArrayBuffer[Boolean], mutable.HashSet[Int]]] ={
    // Map from constraints output -> index of Solution

    val classifiedSolutions = new ArrayBuffer[mutable.HashMap[ArrayBuffer[Boolean], mutable.HashSet[Int]]]() //mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]]
    inputSet.foreach(input => {
      val outputOfSol = new mutable.HashMap[ArrayBuffer[Boolean], mutable.HashSet[Int]]()
      runOnSolutions.foreach(solID => {
        // out should be boolean type because we are evaluating constraints
        val output = new ArrayBuffer[Boolean]()
        solver.allConstraints.foreach{
          constr => {
            val solution = indexedSolutions.getOrElse(solID, null)
            val eval = new ConstraintEvalOnInput(solution, solver, input)
            val out = constr.accept(eval).asInstanceOf[BoolType]
            output.append(out.value)
          }
        }
        outputOfSol.get(output) match {
          case None => {
            val newSet = new mutable.HashSet[Int]()
            newSet.add(solID)
            outputOfSol += output -> newSet
          }
          case Some(existingSet) => {
            existingSet.add(solID)
            outputOfSol.update(output, existingSet)
          }
        }
      })
      classifiedSolutions.append(outputOfSol)
    })
    classifiedSolutions
  }

  def clusterGraph(indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]],
                   clusterOnIDs: scala.collection.Set[Int],
                   filteredClassifier: ArrayBuffer[mutable.HashMap[ArrayBuffer[Boolean], mutable.HashSet[Int]]],
                   varCollector: ConstraintVarsCollector)
  :  Unit /*mutable.HashSet[MCClusterImpl]*/ ={

    //if(clusterDiv.size == 0 && clustersNonDiv.size > 0)
    //  return

    // Graph is for graph clustering
    val graph = new SimpleGraph[Integer, DefaultWeightedEdge](classOf[DefaultWeightedEdge])
    clusterOnIDs.foreach(k => graph.addVertex(k))

    filteredClassifier.foreach(eachInputRes =>{
      eachInputRes.foreach(out2GroupOfSol =>{
        val (_,solutions) = out2GroupOfSol
        solutions.foreach(sol1 =>{
          solutions.foreach(sol2 =>{
            if(sol1 != sol2) {
              graph.addEdge(sol1, sol2)
            }
          })
        })
      })
    })

    val cl: MCClusterer = new MCClusterer
    cl.execute(graph)
    cl.printResult()
    logger.debug("NGroups: "+cl.getResult.size())
    val clusters = cl.getResult()

    val clusterTests = generateTestForEachCluster(clusters, indexedSolutions, varCollector)
    import scala.collection.JavaConversions._
    clusters.foreach(cl => {
      val tests = clusterTests.getOrElse(cl, null)
      if(tests != null){
        val runOnSolutions = cl.getNodes.foldLeft(new mutable.HashSet[Int]()){(res, node) => {
          res.add(node.getNode)
          res
        }}
        val classifiedSol = inputRunner(tests, indexedSolutions, runOnSolutions)
        val filteredSol = classifiedSol.filter(f => f.size > 1)
        if(filteredSol.size == 0){
          cl.setDividable(false)
          clustersNonDiv.add(cl)
          clusterDiv.remove(cl)
        }else{
          // Dividable clusters
          if(!clusterDiv.contains(cl)) {
            cl.setDividable(true)
            clusterDiv.add(cl)
            val ids = cl.getNodes.foldLeft(new mutable.HashSet[Int]()) {
              (res, node) => {
                res.add(node.getNode)
                res
              }
            }
            clusterGraph(indexedSolutions, ids, filteredSol, varCollector)
          }else{
            clustersNonDiv.add(cl)
          }
          clusterDiv.remove(cl)
        }
        //println("CL: "+classifiedSol)
      }else{
        clustersNonDiv.add(cl)
        clusterDiv.remove(cl)
      }
    })
  }

  def chooseSolutionFromClusters(clusters: mutable.HashSet[MCClusterImpl],
                                 indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]]) = {
    import scala.collection.JavaConversions._
    val chosenIDs = clusters.par.foldLeft(List[(Int, Int)]())(
      (res, cluster) => {
        val size = cluster.getNodes.size()
        val randIndex = random.nextInt(size)
        // Randomly choose one from each cluster
        val chosen = cluster.getNodes().toList(randIndex)
        // We wrap up each id with its group's size
        res :+ (chosen.getNode.intValue(), size)
      })

    /*val sortedByGroupSize = chosenIDs.sortBy(_._2)
    logger.debug(sortedByGroupSize)
    val maxSize = sortedByGroupSize.last._2
    //println(List[(Int, Int)]((6,3),(7,2),(8,1)).sortBy(_._2))
    def getFitness(indiv: (Int, Int)): Double ={
      1 - (indiv._2.toDouble/maxSize.toDouble)
    }

    def compilableIndiv(indiv: (Int, Int)): Boolean = true*/

    //val filteredIDs = if(chosenIDs.size > 3) chosenIDs.sortBy(_._2).take(3) else chosenIDs
    //val filteredIDs = SelectionScheme.stochasticUniversalSampling(chosenIDs, 1, rand,getFitness, compilableIndiv)
    val chosenID = SelectionScheme.tournamentSelection(chosenIDs, 2, random, true, compareFitness)
    val filteredIDs = new ArrayBuffer[(Int, Int)]()
    filteredIDs.append(chosenID)
    val chosenSolutions = filteredIDs.foldLeft(new mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]]()){
      (res, eachID) =>{
        val correspondingSolution = indexedSolutions.get(eachID._1).getOrElse(null)
        logger.debug("Chosen: "+correspondingSolution+" id:"+eachID._1)
        res.add(correspondingSolution)
        res
      }
    }

    logger.debug(chosenSolutions)
    chosenSolutions
  }

  def generateTestForEachCluster(clusters: util.HashSet[MCClusterImpl],
                                 indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]],
                                 varCollector: ConstraintVarsCollector)
  : mutable.HashMap[MCClusterImpl, mutable.HashSet[mutable.HashMap[InternalTerm, DataType]]] ={
    import scala.collection.JavaConversions._
    //println("Generating tests by z3")
    // TODO: remove this setSeed later
    //random.setSeed(0)
    // each cluster, we generate set of test input
    clusters.foldLeft(new mutable.HashMap[MCClusterImpl, mutable.HashSet[mutable.HashMap[InternalTerm, DataType]]]())(
      (res, cl) => {
        val clSize = cl.getNodes.size()
        if(clSize > 1) {
          val clList = cl.getNodes.toList
          var count = 0
          while (count < 100){
            val i1 = random.nextInt(clSize)
            val i2 = random.nextInt(clSize)
            val cl1 = clList(i1)
            val cl2 = clList(i2)
            //println("IDs: " + cl1.getNode + " " + cl2.getNode)
            val sol1 = indexedSolutions.get(cl1.getNode).getOrElse(null)
            val sol2 = indexedSolutions.getOrElse(cl2.getNode, null)
            sol1.foreach(f =>{
              val (funSig, sol1AST) = f
              val sol2AST = sol2.get(funSig).getOrElse(null)
              if(!sol1AST.isInstanceOf[OriginalSolution] || !sol2AST.isInstanceOf[OriginalSolution]){
                val test = generateTestsWithZ3(sol1AST, sol2AST, varCollector)
                if(test != null){
                  cl.setDividable(true)
                  val testSet = res.getOrElse(cl, new mutable.HashSet[mutable.HashMap[InternalTerm, DataType]]())
                  testSet.add(test)
                  res.update(cl, testSet)
                }
                count += 1
              }
            })
          }
        }
        res
    })
  }

  def generateTestsWithZ3(sol1: Solution, sol2: Solution, varCollector: ConstraintVarsCollector)
  : mutable.HashMap[InternalTerm, DataType]= {
    //println("sol1: "+sol1+" sol2: "+sol2)
    val z3 = new Z3Context("MODEL" -> true)
    val z3ASTConverter = new ToZ3ASTConverter(z3)
    val sol1Z3 = sol1.getTransformedCAST().accept(z3ASTConverter).asInstanceOf[Z3ASTReturnType].getAST()
    val sol2Z3 = sol2.getTransformedCAST().accept(z3ASTConverter).asInstanceOf[Z3ASTReturnType].getAST()
    val cs1 = z3.mkAnd(sol1Z3, z3.mkNot(sol2Z3))
    val cs2 = z3.mkAnd(sol2Z3, z3.mkNot(sol1Z3))
    val solver = z3.mkSolver
    solver.assertCnstr(z3.mkOr(cs1, cs2))
    //println(cs)
    // Attempting to solve the constraints
    val sol = solver.check()
    //println(sol)
    sol match {
      case None => {
        z3.delete()
        null
      }// Note: Need reset before using z3 to check and getModel again
      case Some(true) => {
        val model = solver.getModel()
        //println(model.toString())
        val result = model.getConstInterpretations.foldLeft(new mutable.HashMap[InternalTerm, DataType]())(
          (res, c) => {
            if(c._2.getSort.isBoolSort){
              val bvar = varCollector.variableNameMap.getOrElse(BoolVar(c._1.getName.toString()), BoolVar(c._1.getName.toString()))
              val value = model.evalAs[Boolean](c._2).getOrElse(throw new RuntimeException("Expect bool value, but found: "+c._2))
              res += bvar -> BoolType(bvar, value)
            }else{
              val ivar = varCollector.variableNameMap.getOrElse(IntVar(c._1.getName.toString()), IntVar(c._1.getName.toString()))
              val value = model.evalAs[Int](c._2).getOrElse(throw new RuntimeException("Expect int value, but found: "+c._2))
              res += ivar -> IntType(ivar, value)
            }
            res
          }
        )
        z3.delete()
        val toGenerateInput = varCollector.allVariableNames.foldLeft(new mutable.HashSet[InternalTerm]()){
          (res, v) => {
            if(!result.contains(v)){
              res.add(v)
            }
            res
          }
        }
        val inputSet = RandomInputGenerator.generate(1, random, toGenerateInput)
        inputSet.head.foldLeft(result){
          (res, ip) => {
            res += ip
            res
          }
        }
      }
      case Some(false) => {
        z3.delete()
        null
      }
    }
  }

  //Prefer smaller size
  def compareFitness(indiv1: (Int, Int), indiv2: (Int, Int)): Int ={
    if(indiv1._2 > indiv2._2)
      return -1
    else
      return 1
  }

  private def addVertexAndEdge(graph: SimpleDirectedGraph[Integer, DefaultWeightedEdge], s: Int, t: Int): Unit ={
    if(!graph.containsVertex(s)) {
      graph.addVertex(s)
    }
    if(!graph.containsVertex(t)){
      graph.addVertex(t)
    }
    if(!graph.containsEdge(s, t)){
      val edgeS2T = graph.getEdgeFactory.createEdge(s, t)
      graph.setEdgeWeight(edgeS2T, 1)
      graph.addEdge(s, t, edgeS2T)
    }else{
      val existingEdge = graph.getEdge(s, t)
      graph.setEdgeWeight(existingEdge, graph.getEdgeWeight(existingEdge)+1)
    }
  }

  def clusterGraph2(graph: SimpleGraph[Integer, DefaultWeightedEdge],
                   indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]],
                   filteredClassifier: ArrayBuffer[mutable.HashMap[ArrayBuffer[Boolean], mutable.HashSet[Int]]],
                   rand: Random):  mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]] = {

    val directedG = new SimpleDirectedGraph[Integer, DefaultWeightedEdge](classOf[DefaultWeightedEdge])
    filteredClassifier.foreach(eachInputRes => {
      eachInputRes.foreach(out2GroupOfSol => {
        val (_, solutions) = out2GroupOfSol
        solutions.foreach(sol1 => {
          solutions.foreach(sol2 => {
            if (sol1 != sol2) {
              addVertexAndEdge(directedG, sol1, sol2)
              if (graph.containsEdge(sol1, sol2)) {
                val edge = graph.getEdge(sol1, sol2)
                val currentWeight = graph.getEdgeWeight(edge)
                graph.setEdgeWeight(edge, currentWeight + 1)
              } else {
                val edge = graph.addEdge(sol1, sol2)
                graph.setEdgeWeight(edge, 1)
              }
            }
          })
        })
      })
    })

    val graph2 = new SimpleGraph[Integer, DefaultWeightedEdge](classOf[DefaultWeightedEdge])

    //printGraph(graph,indexedSolutions)


    val stronglyCon = new GabowStrongConnectivityInspector(directedG)
    val strongSet = stronglyCon.stronglyConnectedSubgraphs()
    println("Discovered components: "+strongSet.size())
    val iterSet = strongSet.iterator()
    var index = 0
    val p1=new IntegerNameProvider[Integer]()
    val p2=new StringNameProvider[Integer]()

    while (iterSet.hasNext){
      val s = iterSet.next()
      println("****Strongly Connected Graph:")
      printGraph(s, indexedSolutions)
      val p4 =
        new ComponentAttributeProvider[DefaultWeightedEdge]() {
          override def getComponentAttributes(e: DefaultWeightedEdge): util.Map[String, String] = {
            val map =new util.LinkedHashMap[String, String]()
            map.put("weight", s.getEdgeWeight(e).toString)
            return map
          }
        }
      val exporter = new ext.DOTExporter(p1,p2,null,null,p4)
      val writer = new FileWriter("./GraphFile_"+index+".dot")
      exporter.export(writer, s)
      index += 1
    }

    val set = graph.edgeSet()
    val iter = set.iterator()
    val torem = new mutable.HashSet[Int]()
    val toadd = new mutable.HashSet[DefaultWeightedEdge]()
    while (iter.hasNext){
      val x = iter.next()
      if (graph.getEdgeWeight(x) >= 4) {
        //println(x.toString + " w: " + graph.getEdgeWeight(x))
        torem.add(graph.getEdgeSource(x))
        torem.add(graph.getEdgeTarget(x))
        toadd.add(x)
      }
      else {
        //print(x.toString + " **w: " + graph.getEdgeWeight(x))
        /*print(" ")
        val x1 = graph.getEdgeSource(x)
        val x2 = graph.getEdgeTarget(x)
        val sol1 = indexedSolutions.getOrElse(x1, null)
        val sol2 = indexedSolutions.getOrElse(x2, null)
        println(" sol1: " + sol1.head._2.getSolution() + " sol2: " + sol2.head._2.getSolution())
        */
      }
    }
    println(torem.size+" "+graph.vertexSet().size())
    //torem.foreach(rem => {
    //  graph.removeVertex(rem)
    //})

    println(torem)

    /*val set1 = graph.edgeSet()
    val iter1 = set1.iterator()
    while (iter1.hasNext) {
      val x = iter1.next()
      print(x.toString + " w: " + graph.getEdgeWeight(x))
      print(" ")
      val x1 = graph.getEdgeSource(x)
      val x2 = graph.getEdgeTarget(x)
      val sol1 = indexedSolutions.getOrElse(x1, null)
      val sol2 = indexedSolutions.getOrElse(x2, null)
      println(" sol1: " + sol1.head._2.getSolution() + " sol2: " + sol2.head._2.getSolution())
    }*/
    toadd.foreach(e => {
      val s = graph.getEdgeSource(e)
      val t = graph.getEdgeTarget(e)
      graph2.addVertex(s)
      graph2.addVertex(t)
      val e2Add = graph2.getEdgeFactory.createEdge(s, t)
      graph2.addEdge(s, t, e2Add)
      graph2.setEdgeWeight(e2Add, graph.getEdgeWeight(e))
    })

    //printGraph(graph2, indexedSolutions)
    exit(0)
    null
  }

  def printGraph(graph: AbstractGraph[Integer, DefaultWeightedEdge],
                 indexedSolutions: mutable.HashMap[Int, mutable.HashMap[FunctionSignature, Solution]]): Unit = {
    val set1 = graph.edgeSet()
    val iter1 = set1.iterator()
    while (iter1.hasNext) {
      val x = iter1.next()
      print(x.toString + " w: " + graph.getEdgeWeight(x))
      print(" ")
      val x1 = graph.getEdgeSource(x)
      val x2 = graph.getEdgeTarget(x)
      val sol1 = indexedSolutions.getOrElse(x1, null)
      val sol2 = indexedSolutions.getOrElse(x2, null)
      println(" sol1: " + sol1.head._2.getSolution() + " sol2: " + sol2.head._2.getSolution())
    }
  }
}
*/