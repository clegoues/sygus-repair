package extsolvers.metasearch.solutionclassifier

import engines.coreast.{BoolVar, _}

import scala.util.Random
import scala.collection._
/**
  * Created by dxble on 7/19/16.
  */
object RandomInputGenerator {
  // generate input variable -> its value
  def generate(niter: Int, rand: Random, allVariables: mutable.HashSet[InternalTerm])
  : mutable.HashSet[mutable.HashMap[InternalTerm, DataType]] = {
    val range = niter * 10
    val iterRange = 1 to niter
    val all = new mutable.HashSet[mutable.HashMap[InternalTerm, DataType]]
    /*val test1 = new mutable.HashMap[InternalTerm, VisitorReturnType]()
    test1 += (IntVar("td->td_nstrips_589-13-590-31") -> IntType(IntConst("2"), 2))
    all.add(test1)
    val test2 = new mutable.HashMap[InternalTerm, VisitorReturnType]()
    test2 += (IntVar("td->td_nstrips_589-13-590-31") -> IntType(IntConst("3"), 3))
    all.add(test2)
    val test3 = new mutable.HashMap[InternalTerm, VisitorReturnType]()
    test3 += (IntVar("td->td_nstrips_589-13-590-31") -> IntType(IntConst("6"), 6))
    all.add(test3)
    val test4 = new mutable.HashMap[InternalTerm, VisitorReturnType]()
    test4 += (IntVar("td->td_nstrips_589-13-590-31") -> IntType(IntConst("9"), 9))
    all.add(test4)*/
    //return all
    val generated = iterRange.foldLeft(new mutable.HashSet[mutable.HashMap[InternalTerm, DataType]]()){
      (allRes, iter) => {
        val eachIterResult = allVariables.foldLeft(new mutable.HashMap[InternalTerm, DataType]()) {
          (res, varName) => {
            varName match {
              case e: BoolVar => {
                val randVal = rand.nextBoolean()
                res += (e -> BoolType(BoolConst(randVal.toString), randVal))
              }
              case e: IntVar => {
                var randVal = rand.nextInt(range)
                if(rand.nextBoolean())
                  randVal = 0 - randVal
                res += (e -> IntType(IntConst(randVal.toString), randVal))
              }
            }
            res
          }
        }

        allRes.add(eachIterResult)
        allRes
      }
    }

    generated
   //generated.foreach(f => all.add(f))
   //all
  }
}
