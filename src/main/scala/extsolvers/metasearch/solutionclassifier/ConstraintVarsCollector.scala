package extsolvers.metasearch.solutionclassifier

import extsolvers.common.{AbstractSolver, DefaultSubsCollector}
import engines.coreast._

import scala.collection._

/**
  * Created by dxble on 7/19/16.
  */
/**
  * This should be called at each constraint from allConstraints in the AbstractSolver
  */
class ConstraintVarsCollector(solver: AbstractSolver) extends DefaultSubsCollector(solver){
  val allVariableNames = new mutable.HashSet[InternalTerm]()
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    val cmd = solver.synthFuns.get(functionDesign).getOrElse(throw new RuntimeException("Function has not been defined/declared! "+functionDesign))

    /** Build variableNameMap
      * Iterate over synth-fun arguments and arguments of function in the constraint to make sure
      * that the arg names are the same. If not same, create a map from synth-fun arg name to arg name
      * of the function in the constraint
      */
    cmd.argList.args.foldLeft(0)((index,arg) =>{
      val thatArg = functionDesign.args(index)
      thatArg match {
        case BoolVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (BoolVar(arg.name) -> thatArg)
        case IntVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (IntVar(arg.name) -> thatArg)
      }
      index + 1
    })
    EmptyType()
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    allVariableNames.add(boolVar)
    EmptyType()
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    allVariableNames.add(intVar)
    EmptyType()
  }
}
