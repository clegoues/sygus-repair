package extsolvers.metasearch.solutionclassifier

import extsolvers.common.{AbstractSolver, DefaultSubsCollector}
import engines.coreast._
import scala.collection._
/**
  * Created by dxble on 7/19/16.
  */
class SubsCollectorOnInput(solver: AbstractSolver, input: mutable.HashMap[InternalTerm, DataType]) extends DefaultSubsCollector(solver){

  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    val cmd = solver.synthFuns.get(functionDesign).getOrElse(throw new RuntimeException("Function has not been defined/declared! "+functionDesign))

    /** Build variableNameMap
      * Iterate over synth-fun arguments and arguments of function in the constraint to make sure
      * that the arg names are the same. If not same, create a map from synth-fun arg name to arg name
      * of the function in the constraint
      */
    cmd.argList.args.foldLeft(0)((index,arg) =>{
      val thatArg = functionDesign.args(index)
      thatArg match {
        case BoolVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (BoolVar(arg.name) -> thatArg)
        case IntVar(thatName) => if(!arg.name.equals(thatName)) variableNameMap += (IntVar(arg.name) -> thatArg)
      }
      index + 1
    })
    EmptyType()//Return whatever type, which is not null
  }


  //Return whatever
  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = EmptyType()

  //Return whatever
  override def visitIntVar(intVar: IntVar): VisitorReturnType = EmptyType()

  //Visit LHS to collect substitutions for variables
  //Visit RHS to collect functions
  override def visitImplies(implies: Implies): VisitorReturnType ={
    implies.opr1.accept(this)
    implies.opr2.accept(this)
  }

  override def visitEqual(equal: Equal): VisitorReturnType ={
    val variableName = equal.opr1.asInstanceOf[InternalTerm]
    if(!equal.opr1.isInstanceOf[FunctionSignature])
      substitutions += ( variableName -> input.get(variableName).getOrElse(throw new RuntimeException("Found no concrete value for: "+variableName)))
    else{
      equal.opr1.accept(this)
    }
    //super.visitEqual(equal)
    //EmptyType()
    BoolType(null, false)
  }
}
