package extsolvers.metasearch.ranking

import engines.coreast.{BoolVar, _}

import scala.collection._
import extsolvers.common._
import synthlib2parser.{FunGTerm, _}

import scala.collection.mutable.{ArrayBuffer, HashMap}
/**
  * Created by dxble on 7/19/16.
  */
class Score{
  private val scoreBuffer = new ArrayBuffer[Double]()

  def overallScore(): Double = (scoreBuffer.foldLeft(0.0){(res, s) => res + s})/scoreBuffer.size
  def setScoreAtIndex(index: Int, score: Double) = scoreBuffer(index) = score
  def addScore(score: Double) = scoreBuffer.append(score)
  def getScoreAtIndex(index: Int) = scoreBuffer(index)
  override def toString(): String = overallScore().toString
}
class Exp2VectorConverter extends CASTVisitorBase{
  val vector = Array[Int](0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
  val weight = Array[Int](1,2,3,4,5,6,7,19,9,10,11,12,13,14,15,16,17,18)

  var numberOfConsts = 0
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    vector(0) += weight(0)
    EmptyType()
  }

  override def visitGreater(greater: Greater): VisitorReturnType = {
    greater.opr1.accept(this)
    greater.opr2.accept(this)
    vector(1) += weight(1)
    EmptyType()
  }

  override def visitLessThan(lessThan: LessThan): VisitorReturnType ={
    lessThan.opr1.accept(this)
    lessThan.opr2.accept(this)
    vector(2) += weight(2)
    EmptyType()
  }

  override def visitLTE(lTE: LTE): VisitorReturnType ={
    lTE.opr1.accept(this)
    lTE.opr2.accept(this)
    vector(3) += weight(3)
    EmptyType()
  }

  override def visitGTE(gTE: GTE): VisitorReturnType ={
    gTE.opr1.accept(this)
    gTE.opr2.accept(this)
    vector(4) += weight(4)
    EmptyType()
  }

  override def visitEqual(equal: Equal): VisitorReturnType={
    equal.opr1.accept(this)
    equal.opr2.accept(this)
    vector(5) += weight(5)
    EmptyType()
  }

  override def visitAnd(and: And): VisitorReturnType={
    and.opr1.accept(this)
    and.opr2.accept(this)
    vector(6) += weight(6)
    EmptyType()
  }

  override def visitOr(or: Or): VisitorReturnType={
    or.opr1.accept(this)
    or.opr2.accept(this)
    vector(7) += weight(7)
    EmptyType()
  }

  override def visitNot(not: Not): VisitorReturnType={
    not.opr.accept(this)
    vector(8) += weight(8)
    EmptyType()
  }

  override def visitImplies(implies: Implies): VisitorReturnType={
    implies.opr1.accept(this)
    implies.opr2.accept(this)
    vector(9) += weight(9)
    EmptyType()
  }

  override def visitPlus(plus: Plus): VisitorReturnType={
    plus.opr1.accept(this)
    plus.opr2.accept(this)
    vector(10) += weight(10)
    EmptyType()
  }

  override def visitMinus(minus: Minus): VisitorReturnType={
    minus.opr1.accept(this)
    minus.opr2.accept(this)
    vector(11) += weight(11)
    EmptyType()
  }

  override def visitDiv(div: Div): VisitorReturnType={
    div.opr1.accept(this)
    div.opr2.accept(this)
    vector(12) += weight(12)
    EmptyType()
  }

  override def visitMult(mult: Mult): VisitorReturnType={
    mult.opr1.accept(this)
    mult.opr2.accept(this)
    vector(13) += weight(13)
    EmptyType()
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    vector(14) += weight(14)
    EmptyType()
  }

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType = {
    vector(15) += weight(15)
    numberOfConsts += 1
    EmptyType()
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    vector(16) += weight(16)
    EmptyType()
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType = {
    vector(17) += weight(17)
    numberOfConsts += 1
    EmptyType()
  }
}

class SolutionRanker(funSubs: mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]], originalExprs: mutable.HashMap[FunctionSignature,CAST]) {
  var solutionList: ArrayBuffer[(mutable.HashMap[FunctionSignature, Solution], Score)] = null
  def this(funSubs: mutable.HashSet[mutable.HashMap[FunctionSignature, Solution]], originalExprs: HashMap[FunctionSignature,CAST], rankingScheme: Boolean) ={
    this(funSubs, originalExprs)
    solutionList = new ArrayBuffer[(mutable.HashMap[FunctionSignature, Solution], Score)]()
    funSubs.foreach(f =>{
      solutionList.append((f, new Score()))
    })
  }

  def refineScore() = {
    assert(solutionList != null)
    solutionList.foreach(f =>{
      val (funSigSolPairs, score) = f
      funSigSolPairs.foreach(fs => {
        val fsSol = fs._2
        val origExp = originalExprs.get(fs._1).getOrElse(throw new RuntimeException("Currently we only support ranking if original expression is provided!"))
        val vectorSol = new Exp2VectorConverter
        fsSol.accept(vectorSol)
        val vectorOrig = new Exp2VectorConverter
        origExp.accept(vectorOrig)
        val cosineScore = CosineSimilarity.cosineSimilarity(vectorSol.vector, vectorOrig.vector)
        score.addScore(cosineScore)
        //val constDiff = vectorSol.numberOfConsts - vectorOrig.numberOfConsts
        //score.addScore(constDiff)
        //fsSol.setScore(score.overallScore())
      })
    })
  }
}
