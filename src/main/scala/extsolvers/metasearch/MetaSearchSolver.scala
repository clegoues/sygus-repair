package extsolvers.metasearch

import config.ConfigOptions
import engines.SynthesisConfig
import engines.{Linear, NonLinear}
import engines.features._
import extsolvers.common.FromSearcher.FromSearcher
import extsolvers.common._
import engines.coreast._
import extsolvers.metasearch.ranking.{CosineSimilarity, Exp2VectorConverter, SolutionRanker}
import extsolvers.metasearch.selection.StochasticUniversalSampling
import extsolvers.metasearch.searcher.{EnumerationSeacher, MutationSeacher}
//import extsolvers.metasearch.solutionclassifier.RandomInputRunner
import synthlib2parser.scopemanager.SymbolTable
import synthlib2parser._
import org.apache.log4j.{BasicConfigurator, Logger, PropertyConfigurator}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.collection.parallel.mutable.ParArray
import scala.util.Random

/**
  * Created by dxble on 7/12/16.
  */
class MetaSearchSolver(theSymbolTable: SymbolTable, rand: Random, features: ArrayBuffer[RankingFeature], synthConfig: SynthesisConfig) extends AbstractSolver(theSymbolTable){
  val EXP_MAX_SIZE = 14
  type SynthesizedSolution = mutable.HashMap[FunctionSignature, Solution]
  val realSolutions = new mutable.HashSet[SynthesizedSolution]()

  var top3Solutions: mutable.HashSet[SynthesizedSolution] = null
  var chosenSol: SynthesizedSolution = null
  val enumSearchers: mutable.HashMap[FunctionSignature, EnumerationSeacher] = new mutable.HashMap[FunctionSignature, EnumerationSeacher]()

  def addSolution2Pool(funSignature: FunctionSignature,sol: CAST, origExp: CAST, pool: mutable.HashSet[Solution], searcherType: FromSearcher): Unit ={
    //val vectorSol = new Exp2VectorConverter
    //sol.accept(vectorSol)
    // Solution must be at least 50% similar to the original buggy expression
    // TODO: the threshold value here is somewhat important
    //logger.info("Threshold value: "+ threshold)
    val mut = new MutantSolution(funSignature, sol, -1, origExp, searcherType)
    // mutant must satisfy all features
    //if(sol.toString.compareTo("(enabled && (this.acknowledgeId == NOACK))") == 0)
    //  logger.info("Debug here")
    //if(sol.toString.contains("enabled") && sol.toString.contains("this.acknowledgeId == NOACK"))
    //  logger.info("Debug here")
    if(!features.exists(feature => if(!feature.satisfyFeature(mut)){
      true
    }else
      false
    )){
      pool.add(mut)
    }
  }

  def expandingSearch(funSignature: FunctionSignature, synthfunCmd: SynthFunCmd, startingSketch: CAST, niter: Int, upSize: Int = 0, downSize: Int = 1): mutable.HashSet[Solution] = {
    if(startingSketch != null){
      //Collect all sub-expressions in the sketch
      val expCol = new ExpressionsCollector
      val collectedExps = startingSketch.accept(expCol).asInstanceOf[ExpList]
      val filterexExps = collectedExps.exps.filter(_.size() < EXP_MAX_SIZE)
      //Unroll the grammar in this synthfunCmd
      val unrolledGrammar = new UnrolledGrammar
      synthfunCmd.accept(unrolledGrammar)

      val mutantPool = new mutable.HashSet[Solution]()
      def choosingExp(): CAST = {
        if(startingSketch.size() > EXP_MAX_SIZE){
          val selector = new StochasticUniversalSampling(filterexExps, rand)
          selector.select()
        }else startingSketch
      }

      def collectCandidates(collected: mutable.HashSet[Solution]): Boolean = {
        var toContinue = true
        val chosenExp = choosingExp()
        // For now, this is experimental. We only enumerate over the starting sketch, instead of
        // enumerating over leaf nodes of starting sketch as selector above.
        // val chosenExp = startingSketch

        logger.debug("~~~~Chosen Expr: " + chosenExp + " Size: " + chosenExp.size() +" Pos: "+chosenExp.position)
        if(chosenExp.size() == startingSketch.size())
          toContinue = false

        if(chosenExp.size() > EXP_MAX_SIZE /*rand.nextBoolean()*/) {
          // Filter those that are syntactically too different with original buggy expr
          val searcher = new MutationSeacher(chosenExp, rand, 10, unrolledGrammar, synthfunCmd, theSymbolTable)
          searcher.search().foreach(m => addSolution2Pool(funSignature, m, chosenExp, mutantPool, FromSearcher.FROM_MUTATION_SEARCHER))
        }else {
          //TODO: how to choose maxSize and minSize wisely?
          // We currently choose maxSize = chosenExp.size()
          val maxSize = chosenExp.size() + upSize
          val minSize = chosenExp.size() - downSize
          enumSearchers.get(funSignature) match {
            case None => {
              val searcher = new EnumerationSeacher(chosenExp, rand, maxSize, minSize, unrolledGrammar, synthfunCmd)
              val searchRes = searcher.search()
              searchRes.foreach(m => addSolution2Pool(funSignature, m, chosenExp, mutantPool, FromSearcher.FROM_ENUMERATION_SEARCHER))
              enumSearchers += funSignature -> searcher
            }
            case Some(enumSearch) => {
              val searchRes = enumSearch.searchWithExp(chosenExp, maxSize, minSize)
              searchRes.foreach(m => addSolution2Pool(funSignature, m, chosenExp, mutantPool, FromSearcher.FROM_ENUMERATION_SEARCHER))
            }
          }
        }
        return toContinue
      }

      var count = 0
      while (count < niter && collectCandidates(mutantPool)) {
       count += 1
      }

      /*val sols = mutantPool.foldLeft(mutable.HashSet[Solution]()){
        (res, mut) => {
          logger.debug("Mutant: "+mut+ " for orig expr: ")
          // IMPORTANT: we need to transform the mutant using the starting sketch!!!
          // DO NOT FORGET to do this transformation!!!
          // candidateSolution is the one we transformed from starting sketch and the mutation solution
          val candidateSolution = mut.transformMutant(startingSketch)
          //val evaluatedCand = partialEvalCandidateSol(new MutantSolution(mut.getFuncSignature(), candidateSolution, -1, mut.getOrigExp(), mut.getFromSearcher()))
          val evaluatedCand = new MutantSolution(mut.getFuncSignature(), candidateSolution, -1, mut.getOrigExp(), mut.getFromSearcher())

          // Copy feature scores from mut to the new one
          if(evaluatedCand != null) {
            if (mut.featureScores.size > 0)
              mut.featureScores.foreach(f => evaluatedCand.addFeatureScores(f._1, f._2))

            res.add(evaluatedCand)
            logger.debug("Evaluated: " + evaluatedCand)
          }
          res
        }
      }*/
      // Just make sure the mutants are transformed before they can be used.
      mutantPool.foreach{
         mut => {
          logger.debug("Mutant: "+mut+ " for orig expr: ")
          mut.transformMutant(startingSketch)
        }
      }
      return mutantPool

    }else{
      throw new RuntimeException("We only support finding solutions based on a given sketch for now!")
    }
  }

  /** Comments taken from the abstract solver:
    * Once we reached this function, all constraints and original expressions have already been collected
    * Note that check-synth is the last command in sygus input. This acts as an activation function.
    * Depending on the search behavior of each synthesis technique/solver, this function will be implemented
    * differently.
    *
    * @param cmd
    */

  override def visitCheckSynthCmd(cmd: CheckSynthCmd): Unit ={
    //Main function here
    super.visitCheckSynthCmd(cmd)
    var (doSynthLater, doSynthFirst) = synthFuns.foldLeft((List[(FunctionSignature, SynthFunCmd)](), List[(FunctionSignature, SynthFunCmd)]())){
      (res, func) =>{
        if(solutionIsOriginal(func._1)){
          (res._1 :+ func, res._2)
        }else{
          (res._1, res._2 :+ func)
        }
      }
    }
    logger.debug("Do synth first:"+doSynthFirst)
    logger.debug("Do synth later:"+doSynthLater)
    if(doSynthFirst.size == 0)
      doSynthFirst = doSynthLater

    var loopIndex = 0
    def Desc[T: Ordering] = implicitly[Ordering[T]].reverse
    val higherOrder = new mutable.HashMap[SynthesizedSolution, mutable.HashSet[Int]]()

    while(realSolutions.size == 0 && loopIndex <= synthConfig.solverBound) {
      doSynthFirst.foreach(func => {
        val (funSignature, synthfunCmd) = func
        val startingSketch = originalExprs.get(funSignature).getOrElse(null)
        features.foreach(f => f.setStartingSketch(startingSketch))
        val sols = expandingSearch(funSignature, synthfunCmd, startingSketch, 10, loopIndex, loopIndex+1)
        val solutionSet = solutions.get(funSignature).getOrElse(new mutable.HashSet[Solution]())
        sols.foreach(s => solutionSet.add(s))
        solutions.update(funSignature, solutionSet)
      })

      if (solutions.size == synthFuns.size) {
        // Each synthesized function, we take top k = 10, 20, etc to be cross product later
        /*val reducedSols = solutions.foldLeft(new mutable.HashMap[FunctionSignature, mutable.HashSet[Solution]]())
      {(res, s) => {
        val sorted = s._2.toList.sortBy(_.featureScores.foldLeft(0.0)(_+_._2))
        res += s._1 -> sorted.take(10).foldLeft(new mutable.HashSet[Solution]())((res, s) => {
          res += s
          res
        })
        res
      }}*/
        val reducedSols = solutions
        val solProduct = crossPSolutions(reducedSols)
        val funcSubsList = crossedSol2Map(solProduct)

        if (!synthConfig.synthesisConfig.level.isInstanceOf[NonLinear]) {
          funcSubsList.foreach(funcSubs => {
            val constraintEvaluator = new ConstraintEvaluation(funcSubs, this)
            val anyFalse = allConstraints.find(constr => {
              try {
                constr.accept(constraintEvaluator).asInstanceOf[BoolType].value == false
              } catch {
                case e: DivisionByZeroException => true
                case _ => true
              }
            })
            var satCov = true
            if(ConfigOptions.semanticFeature || ConfigOptions.onlyFeature == 8) {
              val cov = constraintEvaluator.funsAngelicValuesSat.foreach {
                fs => {
                  val allExpectedValues = funsAngelicValues.getOrElse(fs._1, new mutable.HashSet[InternalTerm]())
                  // Count all expected values that are covered
                  val coverage = allExpectedValues.intersect(fs._2).size.toDouble / allExpectedValues.size.toDouble
                  val sol = funcSubs.getOrElse(fs._1, throw new RuntimeException("Cannot happen"))
                  val cov = new AngelicValuesBalanceCoverage(coverage)
                  //cov.setWeight(1000)
                  if (!cov.satisfyFeature(sol))
                  // Coverage feature is not satisfied
                    satCov = false
                }
              }
            }
            anyFalse match {
              case None => if (satCov) realSolutions.add(funcSubs)
              case Some(_) => {}
            }
          })
        } else {
          // Allow higher order mutant here with nonlinear level
          funcSubsList.foreach(funcSubs => {
            val constraintEvaluator = new ConstraintEvaluation(funcSubs, this)
            if (funcSubs != null && funcSubs.size > 0) {
              var index = 0
              val passedConstraints = allConstraints.foldLeft(new mutable.HashSet[Int]()) { (res, c) => {
                index += 1
                // Collect passed constraints
                if (c.accept(constraintEvaluator).asInstanceOf[BoolType].value)
                  res += index
                else res
              }
              }
              if (passedConstraints.size == allConstraints.size) realSolutions.add(funcSubs)
              else higherOrder += funcSubs -> passedConstraints
            }
          })
        }
      }
      //TODO: to do something with the doSynthLater
      logger.debug("Real Solution: size " + realSolutions.size + " :" + realSolutions)
      loopIndex += 1
      // Bachle: new updates: reduce threshold
      features.foreach(f => {
        if(f.isInstanceOf[CosineSimilarityFeature]) {
          f.setThreshold(f.getThreshold()/2.0)
        }
        if(f.isInstanceOf[VarLocationChange]) {
          f.setThreshold(f.getThreshold() * 1.5)
        }
        if(f.isInstanceOf[GumtreeDiffFeature]) f.setThreshold(f.getThreshold() * 5.0)
      })
      //val ranker = new SolutionRanker(realSolutions, originalExprs, true)
      //ranker.refineScore()
      /*def Desc[T : Ordering] = implicitly[Ordering[T]].reverse
    ranker.solutionList.sortBy(_._2.overallScore())(Desc).foreach(f =>{
      logger.debug("Refined: "+f)
    })*/
    }

    logger.info("Real Solution Size: "+realSolutions.size)

    if(realSolutions.size > 1000) {
      //val inputRunner = new RandomInputRunner(this, rand, realSolutions)
      //top3Solutions = inputRunner.runner(true)
      //val randIndex = rand.nextInt(top3Solutions.size)
      //chosenSol = top3Solutions.toList(randIndex)
    }else{
      // Allow higher order mutant here, which combines enumerated mutant at different branches
      if(realSolutions.size == 0 && synthConfig.synthesisConfig.level.isInstanceOf[NonLinear]) {
        // Here we take higherOder, combine those that have different set of passed constraints
        // This will increase the likelihood of combining them would solve all constraints
        // SynthesizedSolution is of type mutable.HashMap[FunctionSignature, Solution]
        val combinedSols = computePairs(higherOrder.toList, true)
        // Now evaluate again on the constraints
        combinedSols.foreach(funcSubs => {
          val constraintEvaluator = new ConstraintEvaluation(funcSubs, this)
          val anyFalse = allConstraints.find(constr => {
            try {
              constr.accept(constraintEvaluator).asInstanceOf[BoolType].value == false
            } catch {
              case e: DivisionByZeroException => true
              case _ => true
            }
          })
          anyFalse match {
            case None => realSolutions.add(funcSubs)
            case Some(_) => {}
          }
        })
      }
      val sorted = realSolutions.toList.sortBy(_.values.foldLeft(0.0){_+_.sumFeatureScores()})(Desc)
      println(sorted)
      val top10Sol = sorted.take(50)
      //val sortedByFeature8Coverage = realSolutions.toList.sortBy(_.values.foldLeft(0.0){_+_.featureScores.getOrElse(8,0.0)})(Desc)
      //val sorted = sortedByFeature8Coverage.take(50)
      //val top10Sol = sorted.sortBy(_.values.foldLeft(0.0){_+_.sumFeatureScores()})(Desc)
      if(ConfigOptions.semanticFeature || ConfigOptions.onlyFeature == 6) {
        top10Sol.foreach(s => s.values.foreach(v => {
          val modelCounter = new ModelCounterCloserFeature("/mnt/hgfs/DDriveWin/GoogleDrive/SourceCode/sygus-repair/count", -100, 100) //"/home/dxble/Downloads/latte-integrale-1.7.3/latte-int-1.7.3/code/latte/count"
          val startingSketch = originalExprs.get(v.getFuncSignature()).getOrElse(null)
          modelCounter.setStartingSketch(startingSketch)
          modelCounter.satisfyFeature(v)
        }))
      }
      if(top10Sol.size>0) {
        val top1 = top10Sol.sortBy(_.values.foldLeft(0.0)(_ + _.sumFeatureScores()))(Desc)
        if (top1.size > 0)
          chosenSol = top1.head
      }
      //realSolutions.toList(rand.nextInt(realSolutions.size))
    }
  }

  def computePairs(list: List[(SynthesizedSolution, mutable.HashSet[Int])], tailOfWhole: Boolean): List[SynthesizedSolution] = {
    list match {
      case hd::tl => {
        val sols = tl match {
          case htl::tl2 => {
            // If the unition of the sets of passed constraints has size equal with allConstraints, we combine the mutant
            val hSol = if(hd._2.union(htl._2).size == allConstraints.size){
              //Get the higher order mutant here
              val sol1 = hd._1
              val sol2 = htl._1
              // now turn the Solution in SynthesizedSolution to higher order
              val higherOrderSol = sol1.foldLeft(new SynthesizedSolution){
                (res, s) => {
                  val combinedSolution = sol2.getOrElse(s._1, null).createHigherOrder(s._2)
                  val startingSketch = originalExprs.getOrElse(s._1, null)
                  res += s._1 -> combinedSolution.toMutantSolution(startingSketch)
                  res
                }
              }
              List[SynthesizedSolution](higherOrderSol)
            }else //Otherwise we do not combine them
              List[SynthesizedSolution]()

            // See TestHigherOrderSol for clearer example.
            // Basically, we compute every pair: (1,2,3) => (1,2), (1,3), (2,3) without repetition
            tl2 match {
              case Nil => {hSol}
              case _ => hSol ++ computePairs(hd::tl2, false)
            }
          }
          case Nil => {List[SynthesizedSolution]()}
        }
        val tailSols = if(tailOfWhole) computePairs(tl, true) else List[SynthesizedSolution]()
        sols ++ tailSols
      }
      case Nil => {List[SynthesizedSolution]()}
    }
  }

  def visitCheckSynthCmd_experimental(cmd: CheckSynthCmd): Unit ={
    //Main function here
    super.visitCheckSynthCmd(cmd)
    val (doSynthLater, doSynthFirst) = synthFuns.foldLeft((List[(FunctionSignature, SynthFunCmd)](), List[(FunctionSignature, SynthFunCmd)]())){
      (res, func) =>{
        if(solutionIsOriginal(func._1)){
          (res._1 :+ func, res._2)
        }else{
          (res._1, res._2 :+ func)
        }
      }
    }
    logger.debug("Do synth first:"+doSynthFirst)
    logger.debug("Do synth later:"+doSynthLater)
    def Desc[T : Ordering] = implicitly[Ordering[T]].reverse

    doSynthFirst.foreach(func => {
      val (funSignature, synthfunCmd) = func
      val startingSketch = originalExprs.get(funSignature).getOrElse(null)
      features.foreach(f => f.setStartingSketch(startingSketch))
      val sols = expandingSearch(funSignature,synthfunCmd, startingSketch, 5)
      val sortedByFeatures = sols.toList.sortBy(_.sumFeatureScores())(Desc).take(20)
      val solutionSet = solutions.get(funSignature).getOrElse(new mutable.HashSet[Solution]())
      sortedByFeatures.foreach(s => solutionSet.add(s))
      solutions.update(funSignature, solutionSet)
    })

    if(solutions.size == synthFuns.size){
      val reducedSols = solutions
      val solProduct = crossPSolutions(reducedSols)
      val funcSubsList = crossedSol2Map(solProduct)
      funcSubsList.foreach(funcSubs => {
        val constraintEvaluator = new ConstraintEvaluation(funcSubs, this)
        val anyFalse = allConstraints.find(constr => {
          try {
            constr.accept(constraintEvaluator).asInstanceOf[BoolType].value == false
          }catch {
            case e: DivisionByZeroException => true
            case _ => true
          }
        })

        anyFalse match {
          case None => realSolutions.add(funcSubs)
          case Some(_) => {}
        }
      })
    }
    //TODO: to do something with the doSynthLater
    logger.debug("Real Solution: size "+realSolutions.size+" :"+realSolutions)
    //val ranker = new SolutionRanker(realSolutions, originalExprs, true)
    //ranker.refineScore()
    /*def Desc[T : Ordering] = implicitly[Ordering[T]].reverse
    ranker.solutionList.sortBy(_._2.overallScore())(Desc).foreach(f =>{
      logger.debug("Refined: "+f)
    })*/
    logger.info("Real Solution Size: "+realSolutions.size)

    if(realSolutions.size > 1000) {
      //val inputRunner = new RandomInputRunner(this, rand, realSolutions)
      //top3Solutions = inputRunner.runner(true)
      //val randIndex = rand.nextInt(top3Solutions.size)
      //chosenSol = top3Solutions.toList(randIndex)
    }else{
      val sorted = realSolutions.toList.sortBy(_.values.foldLeft(0.0){_+_.sumFeatureScores()})(Desc)
      println(sorted)
      val top10Sol = sorted.take(50)
      top10Sol.foreach(s => s.values.foreach(v => {
        val modelCounter = new ModelCounterCloserFeature("count", -100, 100)
        val startingSketch = originalExprs.get(v.getFuncSignature()).getOrElse(null)
        modelCounter.setStartingSketch(startingSketch)
        modelCounter.satisfyFeature(v)
      }))
      if(top10Sol.size>0) {
        val top1 = top10Sol.sortBy(_.values.foldLeft(0.0)(_ + _.sumFeatureScores()))(Desc)
        if (top1.size > 0)
          chosenSol = top1.head
      }
      //realSolutions.toList(rand.nextInt(realSolutions.size))
    }
  }
}

object MetaSearchSolver{
  val log4jProp = "/home/dxble/workspace/sygus-repair/src/main/resources/Log4j.properties" //"/home/dxble/workspace/repairtools/angelix/src/af2sygus/src/main/resources/Log4j.properties"
  val fileName1 = "/home/dxble/workspace/synthesis/mytest/if-condition"
  val fileName2 =  "/home/dxble/workspace/synthesis/mytest/distance.sl"
  val fileName3 = "/home/dxble/Dropbox/testing/median-h45-012/.angelix/myout_median-h45-012.sygus"
  val fileName4 = "/home/dxble/workspace/repairtools/angelix/src/af2sygus/myout_median-h19-003.sygus"
  val fileName5 = "/home/dxble/workspace/repairtools/angelix/src/af2sygus/myout_reuse.sygus"

  val logger = Logger.getLogger(this.getClass)

  def main(args: Array[String]) {
    /*PropertyConfigurator.configure(log4jProp)
    val parsedResult = Synthlib2Compiler(scala.io.Source.fromFile(fileName5).mkString, true)
    logger.debug(parsedResult)
    val rand = new Random(0)
    parsedResult match {
      case Right(res) => {
        val (theProg, parser) = res
        val solver = new MetaSearchSolver(parser.theSymbolTable, rand)
        theProg.accept(solver)
      }
    }*/
  }

  def solveFromString(inputString: String, rand: Random, features: ArrayBuffer[RankingFeature]) ={
    //PropertyConfigurator.configure(log4jProp)
    val parsedResult = Synthlib2Compiler(inputString, true)
    logger.debug(parsedResult)
    parsedResult match {
      case Right(res) => {
        val (theProg, parser) = res
        val solver = new MetaSearchSolver(parser.theSymbolTable, rand, features, null)
        theProg.accept(solver)
        solver.chosenSol
      }
      case Left(_) => null
    }
  }

  def solveFromFile(inputFile: String, rand: Random, features: ArrayBuffer[RankingFeature], config: SynthesisConfig) ={
    //PropertyConfigurator.configure(log4jProp)
    BasicConfigurator.configure()
    val parsedResult = Synthlib2Compiler(scala.io.Source.fromFile(inputFile).mkString, true)
    logger.debug(parsedResult)
    parsedResult match {
      case Right(res) => {
        val (theProg, parser) = res
        val solver = new MetaSearchSolver(parser.theSymbolTable,rand, features, config)
        theProg.accept(solver)
        solver.chosenSol
      }
      case Left(_) => null
    }
  }
}
