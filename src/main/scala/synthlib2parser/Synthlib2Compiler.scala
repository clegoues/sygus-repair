package synthlib2parser

/**
  * Created by dxble on 7/12/16.
  */

object Synthlib2Compiler {
  def apply(code: String, typeScopeCheck: Boolean): Either[Synthlib2CompilationError, (ASTBase, Synthlib2Parser)] = {
    val parser: Synthlib2Parser = new Synthlib2Parser(typeScopeCheck)
    for {
      tokens <- Synthlib2Lexer(code).right
      ast <- parser.doParse(tokens).right
    } yield (ast, parser)
  }
}
