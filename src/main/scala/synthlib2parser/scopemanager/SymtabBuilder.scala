package synthlib2parser.scopemanager

import synthlib2parser._

import scala.collection.mutable

/**
  * Created by dxble on 7/11/16.
  */
class SymtabBuilder(theSymbolTable: SymbolTable) extends ASTVisitorBase("SymbolTableBuilder"){

  private val sortStack = new mutable.Stack[SortExp]()
  override def visitOriginalExpCmd(cmd: OriginalExpCmd): Unit = {
    // previsit. Push a new scope onto the symbol table
    theSymbolTable.push()
    // Check that the formals are okay.
    // And bind them in the new scope
    // also gather the argument sorts for later use

    val argSorts = cmd.args.args.foldLeft(List[SortExp]()) {
      (res, asPair) => {
        asPair.accept(this)
        theSymbolTable.bindFormal(asPair.name, asPair.sort.myClone().asInstanceOf[SortExp])
        res :+ asPair.sort
      }
    }

    // Check that the type of the term is the same as the one declared

    // Push the builder through the term to ensure that let-bound terms are
    // appropriately bound
    cmd.constraint.accept(this)

    //This try-catch is because we do not care if the original expression is well-formed in the grammar
    try {
      val termSort = cmd.constraint.getTermSort(theSymbolTable)
      val expTermSort = cmd.sort
      if (!theSymbolTable.resolveSort(termSort).equals(theSymbolTable.resolveSort(expTermSort))) {
        throw new RuntimeException("Definition of function symbol \"" + cmd.name + "\" does not match expected sort\n" + cmd.pos)
      }
    }catch {
      case e: Exception => {}
    }

    cmd.setScope(theSymbolTable.pop())

    // Finally, we check that a function of the same name
    // is not already defined

    if (theSymbolTable.lookupFun(cmd.name, argSorts) == null) {
      throw new RuntimeException("Function with name \"" + cmd.name + "\" has not been defined/declared.\n" + cmd.pos)
    }

  }

  override def visitFunDefCmd(cmd: FunDefCmd): Unit ={
    // previsit. Push a new scope onto the symbol table
    theSymbolTable.push()
    // Check that the formals are okay.
    // And bind them in the new scope
    // also gather the argument sorts for later use

    val argSorts = cmd.args.args.foldLeft(List[SortExp]()) {
      (res, asPair) => {
        asPair.accept(this)
        theSymbolTable.bindFormal(asPair.name, asPair.sort.myClone().asInstanceOf[SortExp])
        res :+ asPair.sort
      }
    }

    // Check that the type of the term is the same as the one declared

    // Push the builder through the term to ensure that let-bound terms are
    // appropriately bound
    cmd.funDef.accept(this)

    val termSort = cmd.funDef.getTermSort(theSymbolTable)
    val expTermSort = cmd.sort
    if(!theSymbolTable.resolveSort(termSort).equals(theSymbolTable.resolveSort(expTermSort))) {
      throw new RuntimeException("Definition of function symbol \"" + cmd.symbol + "\" does not match expected sort\n" + cmd.pos)
    }

    cmd.setScope(theSymbolTable.pop())

    // Finally, we check that a function of the same name
    // is not already defined

    if (theSymbolTable.lookupFun(cmd.symbol, argSorts) != null) {
      throw new RuntimeException("Function with name \"" + cmd.symbol + "\" has already been defined/declared.\n" + cmd.pos)
    }

    // All seems good.
    // Bind the def
    theSymbolTable.bindUserFun(cmd.myClone().asInstanceOf[FunDefCmd])
  }

  //override def visitFunDeclCmd(cmd: FuncDeclCmd)

  private def cloneVector(argList: List[SortExp]) ={
    argList.foldLeft(List[SortExp]()){(res, arg) => res :+ arg.myClone().asInstanceOf[SortExp]}
  }

  override def visitSynthFunCmd(cmd: SynthFunCmd): Unit ={
    // Push a new scope
    theSymbolTable.push()
    super.visitSynthFunCmd(cmd)
    // All is good
    cmd.setScope(theSymbolTable.pop())

    // Gather the arg sorts
    val argSorts = cmd.argList.args.foldLeft(List[SortExp]()){
      (res, arg) => {
        res :+ arg.sort
      }
    }
    // Check that no function exists of the same name
    if (theSymbolTable.lookupFun(cmd.name, argSorts) != null) {
      throw new RuntimeException("Function with name \"" + cmd.name + "\" has already been defined/declared.\n" + cmd.pos)
    }
    //Bachle: Check if the function defined in the grammar is supported by theory
    /*cmd.grammarRules.foreach(r => {
      r.expansions.foreach(exp =>{
        if(exp.isInstanceOf[FunGTerm]){
          val expFunGTerm = exp.asInstanceOf[FunGTerm]
          val definedTheoryFun = theSymbolTable.lookupFun(expFunGTerm.funName,
            expFunGTerm.args.foldLeft(List[SortExp]()){(res, expi) => {
                if(expi.isInstanceOf[SymbolGTerm]){
                  val symGterm = expi.asInstanceOf[SymbolGTerm]
                  if(symGterm.symbol == r.symbol)
                    res :+ r.sort
                  else
                    res :+ expi.getTermSort(theSymbolTable)
                }else{
                  res :+ expi.getTermSort(theSymbolTable)
                }
              }
            })
          if(definedTheoryFun == null)
            throw new RuntimeException("Error: not defined theory fun: "+expFunGTerm+ " at Line: "+expFunGTerm.pos.line+ " Column: "+expFunGTerm.pos.column)
        }
      })
    })*/

    // Bind
    theSymbolTable.bindSynthFun(cmd.name, cloneVector(argSorts), cmd.sort.myClone().asInstanceOf[SortExp])
  }

  override def visitVarDeclCmd(cmd: VarDeclCmd) = {
    // Ensure that the sort is okay
    super.visitVarDeclCmd(cmd)

    // Check for redeclarations

    if (theSymbolTable.lookupSort(cmd.name) != null ||
      theSymbolTable.lookupVariable(cmd.name) != null ||
      theSymbolTable.lookupFun(cmd.name, List[SortExp]()) != null) {

      throw new RuntimeException("Identifier \"" + cmd.name + "\" has " + "already been declared/defined as a sort/variable/constant.\n" + cmd.pos)
    }

    // Bind
    theSymbolTable.bindVariable(cmd.name, cmd.sort.myClone().asInstanceOf[SortExp])
  }

  override def visitConstraintCmd(cmd: ConstraintCmd) = {
    // Check that the type of the term is okay
    val sort = cmd.term.getTermSort(theSymbolTable)
    if (!theSymbolTable.resolveSort(sort).isInstanceOf[BoolSortExp]) {
      throw new RuntimeException("Constraint terms must be boolean typed.\n" + cmd.pos)
    }

    // Do nothing otherwise
  }

  override def visitNamedSortExp(sort: NamedSortExp) = {
    // Check that the named sort actually resolves to something
    val ste = theSymbolTable.lookupSort(sort.name);
    if(ste == null || ste.getKind() != SymtabEntryKind.STENTRY_SORT) {
      throw new RuntimeException("Sort name \"" + sort.name + "\" could not " + "be resolved to anything meaningful.\n" + sort.pos)
    }

    val sortE = ste.getSort()
    if (theSymbolTable.resolveSort(sortE) == null) {
      throw new RuntimeException("Sort name \"" + sort.name + "\" could not " +
        "be resolved to anything meaningful.\n" +  sort.pos)
    }
    // Do nothing otherwise
  }

  //Bachle: bind sorts defined in each synth-fun
  override def visitNTDef(ntDef: NTDef) ={
    theSymbolTable.bindSort(ntDef.symbol, ntDef.sort)
  }

  override def visitLiteralGTerm(theTerm: LiteralGTerm)
  {
    super.visitLiteralGTerm(theTerm)
  }
}

object SymtabBuilder{
  def doBuild(prog: Program, theSymbolTable: SymbolTable) = {
    val builder = new SymtabBuilder(theSymbolTable)
    prog.accept(builder)
  }
}
