package synthlib2parser
import synthlib2parser.scopemanager._
/**
  * Created by dxble on 7/8/16.
  */

sealed trait Synthlib2CompilationError

case class Synthlib2LexerError(location: Location, msg: String) extends Synthlib2CompilationError
case class Synthlib2ParserError(location: Location, msg: String) extends Synthlib2CompilationError

case class Location(line: Int, column: Int) {
  override def toString = s"$line:$column"
}

import synthlib2parser.scopemanager.{SymbolTableScope, SymtabBuilder}

import scala.util.parsing.combinator.Parsers
import scala.util.parsing.input.{NoPosition, Position, Reader}

class Synthlib2Parser(typeScopeCheck: Boolean) extends Parsers {
  override type Elem = Synthlib2Token
  val theSymbolTable = new SymbolTable

  class Synthlib2TokenReader(tokens: Seq[Synthlib2Token]) extends Reader[Synthlib2Token] {
    override def first: Synthlib2Token = tokens.head
    override def atEnd: Boolean = tokens.isEmpty
    override def pos: Position = tokens.headOption.map(_.pos).getOrElse(NoPosition)
    override def rest: Reader[Synthlib2Token] = new Synthlib2TokenReader(tokens.tail)
  }


  def doParse(tokens: Seq[Synthlib2Token]): Either[Synthlib2ParserError, ASTBase] = {
    val reader = new Synthlib2TokenReader(tokens)
    program(reader) match {
      case NoSuccess(msg, next) => Left(Synthlib2ParserError(Location(next.pos.line, next.pos.column), msg))
      case Success(result, next) => {
        if(typeScopeCheck) {
          val logicLoader = new LogicSymbolLoader
          logicLoader.loadAll(theSymbolTable)
          SymtabBuilder.doBuild(result.asInstanceOf[Program], theSymbolTable)
        }
        Right(result)
      }
    }
  }

  def program: Parser[ASTBase] = positioned {
    phrase(block)
  }

  def block: Parser[ASTBase] = positioned {
    val b1 = setLogicCmd ~ rep1(cmdPlus) ^^ { case setlg ~ stmtList => Program(setlg :: stmtList) }
    val b2 = rep1(cmdPlus) ^^ { case stmtList => Program(stmtList) }
    b1|b2
  }

  def setLogicCmd: Parser[ASTCmd]  = TK_LPARENT() ~ TK_SETLOGIC() ~ symbol ~ TK_RPARENT() ^^ {
    case _ ~ _ ~ TK_SYMBOL(iden) ~ _ => {SetLogicCmd(iden)}
  }

  //TODO other cmds
  def cmdPlus: Parser[ASTCmd] = positioned {

    //-------

    val funDefCmd = TK_LPARENT() ~TK_DEFINE_FUN() ~symbol ~ argList ~ sortExpr ~ term ~ TK_RPARENT() ^^{
      case _ ~ _ ~ TK_SYMBOL(symb) ~ args ~ retType ~ body ~_ => FunDefCmd(symb, args, retType, body, null)
    }

    //val funcDeclCmd = ""
    val synthFunCmd = TK_LPARENT() ~ TK_SYNTH_FUN() ~ symbol ~ argList ~ sortExpr ~ TK_LPARENT() ~ rep1(ntDef) ~ TK_RPARENT() ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ TK_SYMBOL(name) ~ args ~ sort ~ _ ~ grammarrules ~ _ ~ _ => SynthFunCmd(name, args, sort, grammarrules, new SymbolTableScope())
    }

    val checkSynthCmd =  TK_LPARENT() ~ TK_CHECK_SYNTH() ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ _ => CheckSynthCmd()
    }

    val constraintCmd = TK_LPARENT() ~ TK_CONSTRAINT() ~ term ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ constraint ~ _ => ConstraintCmd(constraint)
    }

    val assertCmd = TK_LPARENT() ~ TK_ASSERT() ~ term ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ constraint ~ _ => AssertCmd(constraint)
    }
    //val sortDefCmd = ""
    //val setOptsCmd = ""
    val varDeclCmd = TK_LPARENT() ~ TK_DECLARE_VAR() ~ symbol ~ sortExpr ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ TK_SYMBOL(name) ~ sort ~ _ => VarDeclCmd(name, sort)
    }
    val originalExpCmd = TK_LPARENT() ~ TK_ORIGINALEXPR() ~symbol ~argList ~sortExpr ~term ~TK_RPARENT() ^^{
      case _ ~ _ ~ TK_SYMBOL(name) ~ args ~ sort ~ constraint ~ _ => OriginalExpCmd(name, args, sort, constraint, null)
    }

    funDefCmd| synthFunCmd| checkSynthCmd| constraintCmd|assertCmd| varDeclCmd| originalExpCmd
  }

  def ntDef: Parser[NTDef] = positioned{
    TK_LPARENT() ~ symbol ~ sortExpr ~ TK_LPARENT() ~ rep1(gTerm) ~ TK_RPARENT() ~ TK_RPARENT() ^^ {
      case _ ~ TK_SYMBOL(symb) ~ sort ~ _ ~ body ~ _ ~ _ => NTDef(symb, sort, body)
    }
  }

  //TODO: other gterms
  def gTerm: Parser[GTerm] = positioned{
    val symbolgterm = symbol ^^ {
      case TK_SYMBOL(symb) => SymbolGTerm(symb)
    }
    val litteralgterm = literal ^^ {
      case lit => LiteralGTerm(lit)
    }
    val fungterm = TK_LPARENT() ~ symbol ~ rep(gTerm) ~ TK_RPARENT() ^^ {
      case _ ~ TK_SYMBOL(symb) ~ gtermStar ~ _ => FunGTerm(symb, gtermStar)
    }

    symbolgterm|litteralgterm| fungterm
  }

  //TODO other terms: LetTerm
  def term: Parser[Term] = positioned {
    val funTerm = TK_LPARENT() ~ symbol ~ rep(term) ~ TK_RPARENT() ^^ {
      case _ ~ TK_SYMBOL(symb) ~ termStars ~ _ => FunTerm(symb, termStars)
    }

    val litTerm = literal ^^ {
      case Literal(lit, sort) => LiteralTerm(Literal(lit, sort))
    }

    val symbTerm = symbol ^^ {
      case TK_SYMBOL(str) => SymbolTerm(str)
    }

    funTerm| litTerm| symbTerm
  }

  //TODO other literals
  def literal: Parser[Literal] = positioned{
    val intConst = intconst ^^ {
      case TK_INT_LITERAL(str) => Literal(str, IntSortExp())
    }

    val boolConst = boolconst ^^ {
      case TK_BOOL_LITERAL(str) => Literal(str, BoolSortExp())
    }

    intConst|boolConst
  }

  def argList = positioned {
    TK_LPARENT() ~ rep(symbolSortPair) ~ TK_RPARENT() ^^{
      case _ ~ pairs ~ _ => ArgList(pairs)
    }
  }

  def symbolSortPair: Parser[ArgSortPair] = positioned {
    TK_LPARENT() ~ symbol ~ sortExpr ~ TK_RPARENT() ^^ {
      case _ ~ TK_SYMBOL(symb) ~ e ~_ => ArgSortPair(symb, e)
    }
  }

  //TODO other sorts
  def sortExpr: Parser[SortExp] = positioned {
    /*val sortbv = TK_LPARENT() ~ TK_BV() ~ intconst ~ TK_RPARENT() ^^ {
      case _ ~ _ ~ TK_INT_LITERAL(con) ~ _ => SortExp(con)
    }*/
    val sortint = TK_INT() ^^ {
      case _ => IntSortExp()
    }
    val sortbool = TK_BOOL() ^^ {
      case _ => BoolSortExp()
    }
    val sortsymb = symbol ^^ {
      case TK_SYMBOL(str) => NamedSortExp(str)
    }

    sortint|sortbool|sortsymb
  }

  /*private def identifier: Parser[IDENTIFIER] = positioned {
    accept("identifier", { case id @ IDENTIFIER(name) => id })
  }*/

  private def intconst: Parser[TK_INT_LITERAL] = positioned {
    accept("int constant", { case id @ TK_INT_LITERAL(name) => id })
  }

  private def boolconst: Parser[TK_BOOL_LITERAL] = positioned {
    accept("bool constant", { case id @ TK_BOOL_LITERAL(name) => id })
  }

  private def symbol: Parser[TK_SYMBOL] = positioned {
    accept("symbol tk", { case id @ TK_SYMBOL(name) => id })
  }

}


