package smt

import org.smtlib.IExpr
import org.smtlib.IExpr._

import scala.collection.JavaConverters.collectionAsScalaIterableConverter

object SMTUtils {
  // do not traverse child symbols such as functions names, semantics for let is a bit wierd
  def fold[T](e: IExpr, f: IExpr => List[T] => T) : T = {
    e match {
      case exp: IAttributedExpr => f(e)(List[T](fold(exp.expr(), f)))
      case exp: IBinaryLiteral => ???
      case exp: IDecimal => ???
      case exp: IError => ???
      case exp: IExists => ???
      case exp: IFcnExpr => f(e)(exp.args().asScala.toList.map((arg: IExpr) => fold(arg, f)))
      case exp: IForall => f(e)(List[T](fold(exp.expr(), f)))
      case exp: IHexLiteral => ???
      case exp: ILet =>
        val bindings = exp.bindings().asScala.toList
        val bindingsResults = bindings.map((b: IBinding) => fold(b.expr(), f))
        f(e)(bindingsResults ++ List[T](fold(exp.expr(), f)))
      case exp: INumeral => f(e)(List[T]())
      case exp: IParameterizedIdentifier => ???
      case exp: IAsIdentifier => ???
      case exp: IStringLiteral => ???
      case exp: ISymbol => f(e)(List[T]())
    }
  }

  private def builtinSymbols =
    Set("+", "-", "*", "/", ">", "<", ">=", "<=", "=",
      "neq", "and", "or", "iff", "ite", "not", "=>", "xor",
      "select", "true", "false", "store")

  def collectVarNames(e: IExpr): List[String] = {
    fold[List[String]](e, {
      case s: ISymbol =>
        if (builtinSymbols.contains(s.value())) { case l => l.flatten }
        else { case l => s.value() :: l.flatten }
      case o => { case l => l.flatten }
    })
  }
}
