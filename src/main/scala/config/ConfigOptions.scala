package config

/**
  * Created by dxble on 2/23/17.
  */
object ConfigOptions {

  object SynthesisLevel extends Enumeration{
    type SynthLevel = Value
    val ALTENATIVES = Value("alternatives")
    val VARIABLES = Value("variables")
    val BASIC_ARITHMETIC = Value("basic-arithmetic") //
    val BASIC_INEQUALITIES= Value("basic-inequalities")
    val BASIC_LOGIC = Value("basic-logic")
    val INTEGER_CONSTANTS = Value("integer-constants")
    val BOOLEAN_CONSTANTS = Value("boolean-constants")
  }

  lazy val synthesisLevels: Array[SynthesisLevel.SynthLevel] = {
    val prop = ConfigurationPropertiesX.properties.getProperty("synthesisLevels")
    if(prop == null)
      Array(SynthesisLevel.ALTENATIVES)
    else prop.split(";").map(SynthesisLevel.withName(_))
  }

  lazy val syntaxFeature: Boolean = {
    val prop = ConfigurationPropertiesX.properties.getProperty("feature.syntax")
    if(prop == null)
       true
    else prop.toBoolean
  }

  lazy val semanticFeature: Boolean = {
    val prop = ConfigurationPropertiesX.properties.getProperty("feature.semantic")
    if(prop == null)
       true
    else prop.toBoolean
  }

  lazy val onlyFeature: Int = {
    val prop = ConfigurationPropertiesX.properties.getProperty("feature.only")
    if(prop == null)
      -1
    else prop.toInt
  }
}
