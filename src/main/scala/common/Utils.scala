package common

object Utils {
  var enableLogging = true
  def writeToFile(file: String, data: String) {
    val pw = new java.io.PrintWriter(new java.io.File(file))
    try {
      pw.write(data)
    } finally {
      pw.close()
    }
  }
}
